VERSION 5.00
Object = "{19A6DCA1-B1C0-11D5-9712-5254AB22AAF0}#2.0#0"; "lc.ocx"
Begin VB.Form frmCopyProcedure 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "复制产品工序"
   ClientHeight    =   1635
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3870
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1635
   ScaleWidth      =   3870
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCopy 
      Caption         =   "复制(&C)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1275
      TabIndex        =   2
      Top             =   1080
      Width           =   1335
   End
   Begin LabelComboBox.lc lcProducts 
      Height          =   330
      Left            =   240
      TabIndex        =   1
      Top             =   540
      Width           =   3435
      _ExtentX        =   6059
      _ExtentY        =   582
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "产品名称"
      ListIndex       =   -1
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "将产品全部工序复制到另一产品："
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   0
      Top             =   180
      Width           =   3150
   End
End
Attribute VB_Name = "frmCopyProcedure"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Cancel As Boolean

Private Sub cmdCopy_Click()

    Me.Hide
    
End Sub

' 初始化必要数据
Public Sub InitData()
    
    Set lcProducts.DataSource = g_cnADO.Execute("select 编号, 名称 from 产品 order by 名称", , adCmdText)
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyEscape Then
        Cancel = True
        Me.Hide
    End If

End Sub
