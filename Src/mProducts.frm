VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form mfrmProducts 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "维护产品"
   ClientHeight    =   6450
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8250
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6450
   ScaleWidth      =   8250
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCombine 
      Caption         =   "合并(&B)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6780
      TabIndex        =   2
      ToolTipText     =   "合并名称相同、编号不同的产品"
      Top             =   3540
      Width           =   1335
   End
   Begin VB.CheckBox chkHideInvalid 
      Caption         =   "隐藏停产"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6780
      TabIndex        =   8
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox txtFilter 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   6780
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   2820
      Width           =   1275
   End
   Begin VB.CheckBox chkInvalid 
      Caption         =   "停产"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6780
      TabIndex        =   1
      Top             =   180
      Width           =   915
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "取消(&C)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6780
      TabIndex        =   7
      Top             =   5940
      Width           =   1335
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "保存(&S)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6780
      TabIndex        =   6
      Top             =   5460
      Width           =   1335
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "删除(&D)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6780
      TabIndex        =   5
      Top             =   4980
      Width           =   1335
   End
   Begin VB.CommandButton cmdModify 
      Caption         =   "修改(&M)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6780
      TabIndex        =   4
      Top             =   4500
      Width           =   1335
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "添加(&A)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6780
      TabIndex        =   3
      Top             =   4020
      Width           =   1335
   End
   Begin MSComctlLib.ListView lvw 
      Height          =   6255
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   6555
      _ExtentX        =   11562
      _ExtentY        =   11033
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "产品名称"
         Object.Width           =   8467
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "停产"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "过滤显示"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   6780
      TabIndex        =   9
      Top             =   2460
      Width           =   960
   End
End
Attribute VB_Name = "mfrmProducts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim blnAdding As Boolean
Dim blnEditing As Boolean
Dim intEditingIndex As Integer

' 放弃保存
Private Sub CancelSave()
    
    If blnAdding Then
        lvw.ListItems.Remove lvw.SelectedItem.Index
    End If
    
    blnAdding = False
    blnEditing = False
    intEditingIndex = 0
    
    Call DoLock
    
End Sub

Private Sub chkHideInvalid_Click()

    DisplayList

End Sub

Private Sub cmdAdd_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    Set lvw.SelectedItem = lvw.ListItems.Add
    DoEvents
    
    lvw.SelectedItem.EnsureVisible
    intEditingIndex = lvw.SelectedItem.Index
    
    blnAdding = True
    
    Call DoLock
    
    DoEvents
    
    lvw.SetFocus
    lvw.StartLabelEdit
    
End Sub

Private Sub cmdCancel_Click()

    Call CancelSave
    
End Sub

Private Sub cmdCombine_Click()

    mfrmCombineProduct.ProductID = lvw.SelectedItem.Text
    mfrmCombineProduct.ProductName = lvw.SelectedItem.Text
    mfrmCombineProduct.Success = False
    mfrmCombineProduct.Show vbModal, Me
    
    If mfrmCombineProduct.Success Then
        lvw.ListItems.Remove lvw.SelectedItem.Index
    End If
    
End Sub

Private Sub cmdDelete_Click()

    Call DeleteRecord
    
End Sub

Private Sub cmdExit_Click()

    Unload Me
    
End Sub

Private Sub cmdModify_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    blnEditing = True
    intEditingIndex = lvw.SelectedItem.Index
    
    Call DoLock
    
    lvw.SetFocus
    lvw.StartLabelEdit
    
End Sub

Private Sub cmdSave_Click()

    If blnAdding Then
        Call SaveAdd
    ElseIf blnEditing Then
        Call SaveEdit
    End If
    
End Sub

' 删除记录
Private Sub DeleteRecord()
    
    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If MsgBox("确实要删除该记录吗？", vbInformation + vbYesNo, msgPrompt) = vbNo Then
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    g_cnADO.Execute "delete from 产品 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText
    
    Call RemoveSelectedRow(lvw)
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox "删除记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 显示列表
Private Sub DisplayList()
    
    Dim strSQL As String
    Dim itemTemp As ListItem
    
    lvw.ListItems.Clear
    
    If txtFilter.Text <> "" Then
        txtFilter.Tag = Replace(txtFilter.Text, "'", "''")
        strSQL = "select * from 产品 where 编号 like '%" & txtFilter.Tag & "%' or 名称 like '%" & txtFilter.Tag & "%' order by 名称"
    Else
        strSQL = "select * from 产品 where 名称 order by 名称"
    End If
    
    With g_cnADO.Execute(strSQL, , adCmdText)
        Do Until .EOF
            If Not !停产 Or chkHideInvalid.Value = vbUnchecked Then
                Set itemTemp = lvw.ListItems.Add(, !GUID, !名称 & "")
                itemTemp.SubItems(1) = IIf(!停产, "是", "否")
            End If
            .MoveNext
        Loop
    End With
    
    If lvw.ListItems.Count > 0 Then
        Set lvw.SelectedItem = lvw.ListItems.Item(1)
        Call DisplayRecord
    End If
    
End Sub

' 显示记录
Private Sub DisplayRecord()
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    chkInvalid.Value = IIf(lvw.SelectedItem.SubItems(1) = "是", vbChecked, vbUnchecked)
    
End Sub

' 锁定/解锁控件
Private Sub DoLock()
    
    If blnAdding Or blnEditing Then
        chkInvalid.Enabled = True
        cmdAdd.Enabled = False
        cmdModify.Enabled = False
        cmdDelete.Enabled = False
        cmdSave.Enabled = True
        cmdCancel.Enabled = True
    Else
        chkInvalid.Enabled = False
        cmdAdd.Enabled = True
        cmdModify.Enabled = True
        cmdDelete.Enabled = True
        cmdSave.Enabled = False
        cmdCancel.Enabled = False
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
    
        If TypeName(ActiveControl) = "TextBox" _
            Or TypeName(ActiveControl) = "ComboBox" _
            Or TypeName(ActiveControl) = "CheckBox" _
            Or TypeName(ActiveControl) = "DTPicker" _
            Or TypeName(ActiveControl) = "lc" Then
            
            On Error Resume Next
            
            SendKeys "{TAB}"
            
        End If
        
    ElseIf KeyCode = vbKeyEscape Then
    
        Unload Me
        
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
    Call DisplayList
    Call DoLock
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Call CancelSave
    
End Sub

' 初始化必要数据
Private Sub InitData()

    chkHideInvalid.Value = vbChecked
    
End Sub

' 保存添加的内容
Private Sub SaveAdd()
    
    If intEditingIndex = 0 Or intEditingIndex > lvw.ListItems.Count Then
        Exit Sub
    End If
    
    Dim strProductName As String
    
    strProductName = Trim(lvw.ListItems(intEditingIndex).Text)
    
    If strProductName = "" Or StrLen(strProductName) > 50 Then
        MsgBox "名称应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        Call OnEditFail
        GoTo PROC_EXIT
    End If
    
    With g_cnADO.Execute("select * from 产品 where 名称 = '" & Replace(strProductName, "'", "''") & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            MsgBox "该名称已经存在，请输入另一个名称。", vbInformation, msgPrompt
            Call OnEditFail
            GoTo PROC_EXIT
        End If
    End With
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "产品", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
        
        .AddNew
        
        !编号 = strProductName
        !名称 = strProductName
        
        Call FillNewRecordData(rsADO)
        
        .Update
        
    End With
    
    lvw.ListItems(intEditingIndex).SubItems(1) = IIf(chkInvalid.Value = vbChecked, "是", "否")
    
    blnAdding = False
    Call DoLock
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    MsgBox "添加记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 保存修改的内容
Private Sub SaveEdit()
    
    If intEditingIndex = 0 Or intEditingIndex > lvw.ListItems.Count Then
        Exit Sub
    End If
    
    Dim strProductName As String
    
    strProductName = Trim(lvw.ListItems(intEditingIndex).Text)
    
    If strProductName = "" Or StrLen(strProductName) > 50 Then
        MsgBox "名称应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        Call OnEditFail
        GoTo PROC_EXIT
    End If
    
    With g_cnADO.Execute("select * from 产品 where 名称 = '" & Replace(strProductName, "'", "''") & "' and GUID <> '" & lvw.ListItems(intEditingIndex).Key & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            MsgBox "该名称已经存在，请输入另一个名称。", vbInformation, msgPrompt
            Call OnEditFail
            GoTo PROC_EXIT
        End If
    End With
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 产品 where GUID = '" & lvw.ListItems(intEditingIndex).Key & "'", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If .BOF Or .EOF Then
            GoTo PROC_EXIT
        End If
        
        !编号 = strProductName
        !名称 = strProductName
        !停产 = (chkInvalid.Value = vbChecked)
        
        Call FillUpdateRecordData(rsADO)
        
        .Update
        
    End With
    
    lvw.ListItems(intEditingIndex).SubItems(1) = IIf(chkInvalid.Value = vbChecked, "是", "否")
    
    blnEditing = False
    Call DoLock
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    If Err.Number = 3022 Then
        rsADO.CancelUpdate
        MsgBox "编号或名称重复，无法修改。", vbCritical, msgErrRuntime
    Else
        MsgBox "修改记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    End If
    GoTo PROC_EXIT
    
End Sub

Private Sub lvw_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    lvw.SortKey = ColumnHeader.Index - 1
    
End Sub

Private Sub lvw_ItemClick(ByVal Item As MSComctlLib.ListItem)

    If blnAdding Then
    
        Call SaveAdd
        
    ElseIf blnEditing Then
    
        Call SaveEdit
        
    Else
        
        Call DisplayRecord
        
        cmdCombine.Enabled = False
        
        Dim i As Long
        
        For i = 1 To lvw.ListItems.Count
        
            If lvw.ListItems.Item(i) <> Item Then
                If lvw.ListItems.Item(i).Text = Item.Text Then
                    cmdCombine.Enabled = True
                    Exit For
                End If
            End If
            
        Next i
        
    End If
    
End Sub

Private Sub txtFilter_Change()

    If ActiveControl.Name = txtFilter.Name Then
        Call DisplayList
    End If
    
End Sub

Private Sub OnEditFail()

    Set lvw.SelectedItem = lvw.ListItems(intEditingIndex)
    DoEvents
    lvw.SetFocus
    lvw.StartLabelEdit

End Sub

