VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmProduceReport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "生产统计"
   ClientHeight    =   7515
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10770
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7515
   ScaleWidth      =   10770
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkHideInvalid 
      Caption         =   "隐藏停产"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7320
      TabIndex        =   8
      Top             =   7080
      Value           =   1  'Checked
      Width           =   1275
   End
   Begin VB.CommandButton cmdExport 
      Caption         =   "导出(&X)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   9360
      TabIndex        =   7
      Top             =   7020
      Width           =   1215
   End
   Begin VB.ComboBox cboDate 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "ProduceReport.frx":0000
      Left            =   1320
      List            =   "ProduceReport.frx":0028
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   7020
      Width           =   1575
   End
   Begin MSComctlLib.ListView lvw 
      Height          =   6855
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   10635
      _ExtentX        =   18759
      _ExtentY        =   12091
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "产品名称"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   1
         Text            =   "生产数量"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Text            =   "合格数量"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Text            =   "次品数量"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Text            =   "次品率"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "销量"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   5460
      TabIndex        =   5
      Top             =   7035
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   52887553
      CurrentDate     =   37302.9999884259
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      Top             =   7035
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   52887553
      CurrentDate     =   37302
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "时间区间"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   11
      Left            =   240
      TabIndex        =   6
      Top             =   7080
      Width           =   960
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "从"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   9
      Left            =   3000
      TabIndex        =   2
      Top             =   7080
      Width           =   240
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "到"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   10
      Left            =   5100
      TabIndex        =   4
      Top             =   7080
      Width           =   240
   End
End
Attribute VB_Name = "frmProduceReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboDate_Click()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        dtpFrom.Enabled = True
        dtpTo.Enabled = True
    Else
        dtpFrom.Enabled = False
        dtpTo.Enabled = False
    End If
    
    SetDateRange cboDate.List(cboDate.ListIndex), dtpFrom, dtpTo
    
    Call DisplayList
    
End Sub

Private Sub chkHideInvalid_Click()

    Call DisplayList
    
End Sub

Private Sub cmdExport_Click()

    Dim strFilename As String
    
    strFilename = CommonDialogFileSave(Me.hwnd, "产销统计" & Format(dtpFrom.Value, "yymmdd") & "-" & Format(dtpTo.Value, "yymmdd") & ".csv", , , "*.csv|*.csv|*.*|*.*|")
    
    If strFilename = "" Then
        Exit Sub
    End If
    
    Dim strContent As String
    
    strContent = "产品名称,生产数量,合格数量,次品数量,次品率,销量" & vbCrLf
    
    Dim i As Long
    
    For i = 1 To lvw.ListItems.Count
    
        strContent = strContent & lvw.ListItems.Item(i).Text & "," & lvw.ListItems.Item(i).SubItems(1) & "," _
            & lvw.ListItems.Item(i).SubItems(2) & "," & lvw.ListItems.Item(i).SubItems(3) & "," _
            & lvw.ListItems.Item(i).SubItems(4) & vbCrLf
        
    Next i
    
    If WriteFile(strFilename, strContent) Then
        MsgBox "数据已保存到文件 " & strFilename & " 中。", vbInformation
    End If
    
End Sub

' 显示指定时间段内所有产品产量和销量列表
Private Sub DisplayList()
    
    Dim dateFrom As Date, dateTo As Date
    Dim ccyTotal As Currency
    Dim rsADOProduce As New ADODB.Recordset
    Dim rsADOSale As New ADODB.Recordset
    Dim itemTemp As ListItem
    
    Screen.MousePointer = vbHourglass
    DoEvents
    
    ccyTotal = 0
    dateFrom = dtpFrom.Value
    dateTo = dtpTo.Value
    
    'dat.RecordSource = "SELECT 产品.名称, 产量表.生产数量, 销量表.销售数量 " & _
        "FROM (产品 LEFT OUTER JOIN " & _
        "(SELECT 产品编号, Sum(工序单主表.数量) AS 生产数量 " & _
        "From 工序单主表 " & _
        "where 工序单主表.日期 between #2002-4-4# and #2003-4-10 23:59:59# " & _
        "GROUP BY 产品编号) " & _
        "产量表  " & _
        "ON 产品.编号 = 产量表.产品编号) " & _
        "left outer join " & _
        "(SELECT 产品编号, Sum(发货明细记录.数量) AS 销售数量 " & _
        "from 发货明细记录 INNER JOIN 发货记录 ON 发货记录.记录号 = 发货明细记录.记录号 " & _
        "where 发货记录.日期 between #2002-4-4# and #2003-4-10 23:59:59# " & _
        "group by 产品编号) " & _
        "销量表 " & _
        "ON 产品.编号 = 销量表.产品编号 " & _
        "ORDER BY 产品.名称"
    rsADOProduce.Open "SELECT 产品编号, Sum(工序单主表.数量) AS 生产数量 , Sum(工序单主表.合格数) AS 合格数量 " & _
        "FROM 工序单主表 " & _
        "where 工序单主表.日期 between #" & Format(dateFrom, gc_DateFormat) & "# and #" & Format(dateTo, gc_DateFormat) & " 23:59:59# " & _
        "GROUP BY 产品编号", g_cnADO, adOpenKeyset, adLockReadOnly, adCmdText
    rsADOSale.Open "SELECT 产品编号, Sum(发货明细记录.数量) AS 销售数量 " & _
        "from 发货明细记录 INNER JOIN 发货记录 ON 发货记录.记录号 = 发货明细记录.记录号 " & _
        "where 发货记录.日期 between #" & Format(dateFrom, gc_DateFormat) & "# and #" & Format(dateTo, gc_DateFormat) & " 23:59:59# " & _
        "group by 产品编号", g_cnADO, adOpenKeyset, adLockReadOnly, adCmdText
    
    lvw.Visible = False
    DoEvents
    
    lvw.ListItems.Clear
    
    With g_cnADO.Execute("select * from 产品 " & IIf(chkHideInvalid.Value = vbChecked, "where not 停产", "") & " order by 名称", , adCmdText)
        
        Do Until .EOF
        
            Set itemTemp = lvw.ListItems.Add(, !GUID, !名称)
            
            If Not (rsADOProduce.BOF And rsADOProduce.EOF) Then
                rsADOProduce.MoveFirst
                rsADOProduce.Find "产品编号 = '" & !编号 & "'"
                If Not rsADOProduce.EOF Then
                    itemTemp.SubItems(1) = FormatNumber(rsADOProduce!生产数量, 0)
                    itemTemp.SubItems(2) = FormatNumber(rsADOProduce!合格数量, 0)
                    itemTemp.SubItems(3) = FormatNumber(rsADOProduce!生产数量 - rsADOProduce!合格数量, 0)
                    itemTemp.SubItems(4) = RegulateNumber(FormatNumber((rsADOProduce!生产数量 - rsADOProduce!合格数量) / rsADOProduce!生产数量 * 100, 2)) & "%"
                End If
            End If
            
            If Not (rsADOSale.BOF And rsADOSale.EOF) Then
                rsADOSale.MoveFirst
                rsADOSale.Find "产品编号 = '" & !编号 & "'"
                If Not rsADOSale.EOF Then
                    itemTemp.SubItems(5) = FormatNumber(rsADOSale!销售数量, 0)
                End If
            End If
            
            .MoveNext
        Loop
    End With
    
PROC_EXIT:
    lvw.Visible = True
    Screen.MousePointer = vbNormal
    DoEvents
        
End Sub

Private Sub dtpFrom_Change()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        Call DisplayList
    End If
    
End Sub

Private Sub dtpTo_Change()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        Call DisplayList
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
    'Call DisplayList
    cboDate.ListIndex = 5
    
End Sub

' 初始化必要数据
Private Sub InitData()
    
    ' 确定最早和最晚的记录的时间
    InitDateRange g_cnADO, "工序单主表,发货记录", dtpFrom, dtpTo
    
End Sub
