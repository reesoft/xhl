VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form mfrmMoveToHistory 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "归档数据"
   ClientHeight    =   3270
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6975
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   6975
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "..."
      Height          =   375
      Left            =   6360
      TabIndex        =   9
      Top             =   2040
      Width           =   375
   End
   Begin VB.TextBox txtDatabase 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1560
      TabIndex        =   8
      Top             =   2040
      Width           =   4695
   End
   Begin VB.CheckBox chkProcedureSheet 
      Caption         =   "生产单记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1860
      TabIndex        =   2
      Top             =   960
      Width           =   1515
   End
   Begin VB.CheckBox chkIncome 
      Caption         =   "收款记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4980
      TabIndex        =   4
      Top             =   960
      Width           =   1275
   End
   Begin VB.CheckBox chkSales 
      Caption         =   "发货记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3540
      TabIndex        =   3
      Top             =   960
      Width           =   1275
   End
   Begin VB.CommandButton cmdMoveToHistory 
      Caption         =   "归档(&A)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   2640
      TabIndex        =   10
      Top             =   2640
      Width           =   1695
   End
   Begin MSComCtl2.DTPicker dtp 
      Height          =   375
      Left            =   4020
      TabIndex        =   6
      Top             =   1440
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   54132737
      CurrentDate     =   37302
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "归档数据库"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   240
      TabIndex        =   7
      Top             =   2100
      Width           =   1200
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "要归档的数据"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Width           =   1440
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "归档该时间点（不含）之前的数据"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   11
      Left            =   240
      TabIndex        =   5
      Top             =   1500
      Width           =   3600
   End
   Begin VB.Label lblNotice 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "提示：被归档的数据将不可见。如需再次查阅，需从归档数据库导入已归档的数据！"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   240
      Left            =   240
      TabIndex        =   0
      Top             =   180
      Width           =   9435
   End
End
Attribute VB_Name = "mfrmMoveToHistory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdBrowse_Click()

    Dim strFilename As String
    
    strFilename = CommonDialogFileSave(Me.hwnd, txtDatabase.Text, , , "数据库|*.mdb|所有文件|*.*|")
    
    If strFilename = "" Then
        Exit Sub
    End If
    
    txtDatabase.Text = strFilename
    
End Sub

Private Sub cmdMoveToHistory_Click()

    If chkProcedureSheet.Value = vbUnchecked And chkSales.Value = vbUnchecked And chkIncome.Value = vbUnchecked Then
        MsgBox "请选择需要归档的数据。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    If Trim(txtDatabase.Text) = "" Then
        MsgBox "请输入归档数据库的路径。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    If MsgBox("确实要归档 " & Format(dtp.Value, gc_DateFormat) & " 之前的数据吗？", vbQuestion + vbYesNo, msgPrompt) = vbNo Then
        Exit Sub
    End If
    
    On Error Resume Next
    
    If Dir(txtDatabase.Text) = "" Then
    
        FileCopy g_DataPath & gc_NewDBName, txtDatabase.Text
        
        If Err.Number <> 0 Then
            MsgBox "创建归档数据库文件失败。", vbCritical, msgPrompt
            Exit Sub
        End If
        
    End If
    
    If Not CheckDatabase(txtDatabase.Text) Then
        MsgBox txtDatabase.Text & " 不是有效的归档数据库文件。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    DoEvents
    
    Call MoveToHistory(dtp.Value, txtDatabase.Text, chkProcedureSheet.Value = vbChecked, chkSales.Value = vbChecked, chkIncome.Value = vbChecked)
    
    Screen.MousePointer = vbNormal
    
    MsgBox "数据已成功归档！", vbInformation, msgOperationSuccess
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
    
End Sub

Private Sub Form_Load()

    lblNotice.Caption = "提示：被归档的数据将不可见。" & vbCrLf & "　　如需再次查阅，需从归档数据库导入已归档的数据！"
    
    Call InitData
    
End Sub

' 初始化必要数据
Private Sub InitData()
    
    dtp.Value = CDate(Year(Date) & "-01-01")
    txtDatabase.Text = g_DataPath & "小花篮归档数据库.mdb"
    
End Sub
