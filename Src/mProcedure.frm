VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{19A6DCA1-B1C0-11D5-9712-5254AB22AAF0}#2.0#0"; "lc.ocx"
Begin VB.Form mfrmProcedures 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "维护工序"
   ClientHeight    =   4500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6510
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   6510
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstFilteredProducts 
      Height          =   3840
      Left            =   1260
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   480
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.TextBox txtFilter 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1260
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   120
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.CommandButton cmdCopy 
      Caption         =   "复制(&C)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5100
      TabIndex        =   8
      Top             =   1500
      Width           =   1335
   End
   Begin VB.ComboBox cboName 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1260
      TabIndex        =   5
      Top             =   540
      Width           =   2565
   End
   Begin VB.TextBox txtPrice 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5040
      TabIndex        =   7
      Top             =   540
      Width           =   915
   End
   Begin LabelComboBox.lc lcProducts 
      Height          =   360
      Left            =   150
      TabIndex        =   1
      ToolTipText     =   "按空格键输入过滤条件"
      Top             =   120
      Width           =   3675
      _ExtentX        =   6482
      _ExtentY        =   635
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "产品名称"
      ListIndex       =   -1
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "取消(&C)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5100
      TabIndex        =   12
      Top             =   3900
      Width           =   1335
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "保存(&S)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5100
      TabIndex        =   11
      Top             =   3420
      Width           =   1335
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "删除(&D)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5100
      TabIndex        =   13
      Top             =   2940
      Width           =   1335
   End
   Begin VB.CommandButton cmdModify 
      Caption         =   "修改(&M)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5100
      TabIndex        =   10
      Top             =   2460
      Width           =   1335
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "添加(&A)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5100
      TabIndex        =   9
      Top             =   1980
      Width           =   1335
   End
   Begin VB.TextBox txtID 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5040
      TabIndex        =   3
      Top             =   120
      Width           =   915
   End
   Begin MSComctlLib.ListView lvw 
      Height          =   3435
      Left            =   60
      TabIndex        =   0
      Top             =   960
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   6059
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "编号"
         Object.Width           =   1323
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "工序名称"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "单价"
         Object.Width           =   1958
      EndProperty
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "工序单价          元"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   3960
      TabIndex        =   6
      Top             =   600
      Width           =   2400
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "工序名称"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   960
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "工序编号"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   3960
      TabIndex        =   2
      Top             =   180
      Width           =   960
   End
End
Attribute VB_Name = "mfrmProcedures"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_ProductIDs As Collection

Dim blnAdding As Boolean
Dim blnEditing As Boolean

' 放弃保存
Private Sub CancelSave()
    
    blnAdding = False
    blnEditing = False
    
    Call DoLock
    Call DisplayRecord
    
End Sub

' 清除控件内容以免对后来的显示造成影响
Private Sub ClearDisplay()
    
    txtID.Text = ""
    cboName.Text = ""
    txtPrice.Text = ""
    
End Sub

Private Sub cmdAdd_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    blnAdding = True
    Call DoLock
    Call ClearDisplay
    
    If lvw.ListItems.Count > 0 Then
        txtID.Text = IncreaseString(lvw.SelectedItem.Text)
    End If
    
    If txtID.Text = "" Then
        txtID.Text = 1
    End If
    
    cboName.SetFocus
    
End Sub

Private Sub cmdCancel_Click()

    Call CancelSave
    
End Sub

Private Sub cmdCopy_Click()

    Load frmCopyProcedure
    
    With frmCopyProcedure
    
        .Cancel = False
        .InitData
        
        .Show vbModal, Me
        
        If Not .Cancel Then
            Call CopyProcedure(lcProducts.Value, .lcProducts.Value)
        End If
        
    End With
    
    Unload frmCopyProcedure
    
End Sub

Private Sub cmdDelete_Click()

    Call DeleteRecord
    
End Sub

Private Sub cmdModify_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    blnEditing = True
    Call DoLock
    txtID.SetFocus
    
End Sub

Private Sub CopyProcedure(strProductSrc As String, strProductDest As String)
    
    If strProductSrc = strProductDest Then
        MsgBox "不能将产品工序复制到同一个产品。", vbInformation, msgPrompt
        GoTo PROC_EXIT
    End If
    
    On Error GoTo PROC_ERR
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 工序 where  产品编号 = '" & strProductDest & "'", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If Not (.BOF And .EOF) Then
            If MsgBox("目标产品的工序已经存在，是否删除该产品已有的工序？", vbQuestion + vbYesNo, msgPrompt) = vbNo Then
                GoTo PROC_EXIT
            End If
            
            Do Until .EOF
                .Delete
                .MoveNext
            Loop
            
        End If
        
    End With
    
    Dim rsADO2 As New ADODB.Recordset

    With g_cnADO.Execute("select * from 工序 where  产品编号 = '" & strProductSrc & "'", , adCmdText)
    
        Do Until .EOF
        
            rsADO.AddNew
            
            rsADO!产品编号 = strProductDest
            rsADO!工序编号 = !工序编号
            rsADO!工序名称 = !工序名称 & ""
            rsADO!单价 = !单价
            
            Call FillNewRecordData(rsADO)
            
            rsADO.Update
            
            .MoveNext
            
        Loop
        
    End With
    
    MsgBox "产品工序已经成功复制。", vbInformation, msgPrompt
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    MsgBox "复制产品工序的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbExclamation, msgPrompt
    rsADO.CancelUpdate
    GoTo PROC_EXIT
    
End Sub

' 删除记录
Private Sub DeleteRecord()
    
    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If MsgBox("确实要删除该记录吗？", vbInformation + vbYesNo, msgPrompt) = vbNo Then
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    g_cnADO.Execute "delete from 工序 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText
    
    Dim lngSN As Long
    Dim rsADO As New ADODB.Recordset
    
    lngSN = 1
    
    With rsADO
    
        .Open "select * from 工序 where  产品编号 = '" & lcProducts.Value & "' order by 工序编号", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        Do Until .EOF
        
            If !工序编号 <> lngSN Then
                !工序编号 = lngSN
                .Update
            End If
            
            lngSN = lngSN + 1
            .MoveNext
        
        Loop
        
    End With
    
    Call DisplayList
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox "删除记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 显示已过滤的产品列表
Private Sub DisplayFilteredProducts()

    Dim strSQL As String
    
    lstFilteredProducts.Clear
    Set m_ProductIDs = New Collection
    
    txtFilter.Tag = Replace(txtFilter.Text, "'", "''")
    
    strSQL = "select * from 产品 where 名称 like '%" & txtFilter.Tag & "%' and not 停产 order by 名称"
    
    With g_cnADO.Execute(strSQL, , adCmdText)
        Do Until .EOF
            lstFilteredProducts.AddItem !名称
            m_ProductIDs.Add !编号 & ""
            .MoveNext
        Loop
    End With
    
End Sub

' 显示列表
Private Sub DisplayList()
    
    Dim itemTemp As ListItem
    
    lvw.ListItems.Clear
    
    With g_cnADO.Execute("select * from 工序 where 产品编号 = '" & lcProducts.Value & "' order by 工序编号", , adCmdText)
        Do Until .EOF
            Set itemTemp = lvw.ListItems.Add(, !GUID, !工序编号)
            itemTemp.SubItems(1) = !工序名称 & ""
            itemTemp.SubItems(2) = FormatMoney(!单价)
            .MoveNext
        Loop
    End With
    
    If lvw.ListItems.Count > 0 Then
        Call DisplayRecord
    End If
    
End Sub

' 显示记录
Private Sub DisplayRecord()
    
    Call ClearDisplay
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    With g_cnADO.Execute("select * from 工序 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            txtID.Text = !工序编号
            cboName.Text = !工序名称
            txtPrice.Text = ToMoney(!单价)
        End If
    End With
    
End Sub

' 锁定/解锁控件
Private Sub DoLock()
    
    If blnAdding Or blnEditing Then
        lcProducts.Locked = True
        txtID.Locked = False
        cboName.Locked = False
        txtPrice.Locked = False
        cmdAdd.Enabled = False
        cmdModify.Enabled = False
        cmdDelete.Enabled = False
        cmdSave.Enabled = True
        cmdCancel.Enabled = True
    Else
        lcProducts.Locked = False
        txtID.Locked = True
        cboName.Locked = True
        txtPrice.Locked = True
        cmdAdd.Enabled = True
        cmdModify.Enabled = True
        cmdDelete.Enabled = True
        cmdSave.Enabled = False
        cmdCancel.Enabled = False
    End If
    
End Sub

Private Sub cmdSave_Click()

    If blnAdding Then
        Call SaveAdd
    ElseIf blnEditing Then
        Call SaveEdit
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
    
        If TypeName(ActiveControl) = "TextBox" _
            Or TypeName(ActiveControl) = "ComboBox" _
            Or TypeName(ActiveControl) = "CheckBox" _
            Or TypeName(ActiveControl) = "DTPicker" _
            Or TypeName(ActiveControl) = "lc" Then
            
            On Error Resume Next
            
            SendKeys "{TAB}"
            
        End If
        
    ElseIf KeyCode = vbKeyEscape Then
    
        Unload Me
        
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
'    Call Displaylist
    Call DoLock
    Call RefreshNameList
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Call CancelSave
    
End Sub

' 初始化必要数据
Private Sub InitData()
    
    Set lcProducts.DataSource = g_cnADO.Execute("select 编号, 名称 from 产品 where not 停产 order by 名称")
    
End Sub

' 刷新工序名称列表
Private Sub RefreshNameList()
    
    cboName.Clear
    
    With g_cnADO.Execute("select distinct 工序名称 from 工序 order by 工序名称", , adCmdText)
        Do Until .EOF
            cboName.AddItem !工序名称 & ""
            .MoveNext
        Loop
    End With
    
End Sub

' 保存添加的内容
Private Sub SaveAdd()
    
    Dim itemTemp As ListItem
    
    txtID.Text = Val(txtID.Text)
    If Val(txtID.Text) < 1 Then
        MsgBox "编号应该是大于 0 的数字，请重新输入。", vbInformation, msgPrompt
        txtID.SetFocus
        Exit Sub
    End If
    
    cboName.Text = Trim(cboName.Text)
    If cboName.Text = "" Or StrLen(cboName.Text) > 50 Then
        MsgBox "名称应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        cboName.SetFocus
        Exit Sub
    End If
    
    If Val(txtPrice.Text) <= 0 Then
        MsgBox "请输入工序单价。", vbInformation, msgPrompt
        txtPrice.SetFocus
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    Dim blnExistSameID As Boolean
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 工序 where  产品编号 = '" & lcProducts.Value & "' order by 工序编号", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        .MoveLast
        
        Do Until .BOF
        
            If !工序编号 > Val(txtID.Text) Then
                !工序编号 = !工序编号 + 1
                .Update
            ElseIf !工序编号 = Val(txtID.Text) Then
                !工序编号 = !工序编号 + 1
                .Update
                Exit Do
            End If
            
            .MovePrevious
            
        Loop
        
        .AddNew
        
        !产品编号 = lcProducts.Value
        !工序编号 = Val(txtID.Text)
        !工序名称 = cboName.Text
        !单价 = Val(txtPrice.Text)
        
        Call FillNewRecordData(rsADO)
        
        txtID.Tag = !GUID
        
        .Update
        
    End With
    
    blnAdding = False
    Call DoLock
    
    Call DisplayList
    Call RefreshNameList
    
    Set lvw.SelectedItem = lvw.ListItems(txtID.Tag)
    Call lvw_ItemClick(lvw.SelectedItem)
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    MsgBox "添加记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 保存修改的内容
Private Sub SaveEdit()
    
    txtID.Text = Val(txtID.Text)
    If Val(txtID.Text) < 1 Then
        MsgBox "编号应该是大于 0 的数字，请重新输入。", vbInformation, msgPrompt
        txtID.SetFocus
        Exit Sub
    End If
    
    cboName.Text = Trim(cboName.Text)
    If cboName.Text = "" Or StrLen(cboName.Text) > 50 Then
        MsgBox "名称应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        cboName.SetFocus
        Exit Sub
    End If
    
    If Val(txtPrice.Text) <= 0 Then
        MsgBox "请输入工序单价。", vbInformation, msgPrompt
        txtPrice.SetFocus
        Exit Sub
    End If
    
    Dim lngFromID As Long
    Dim lngToID As Long
    
    lngFromID = Val(lvw.SelectedItem.Text)
    lngToID = Val(txtID.Text)
    
    On Error GoTo PROC_ERR
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 工序 where  产品编号 = '" & lcProducts.Value & "' order by 工序编号", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If lngFromID = lngToID Then
        
            Do Until .EOF
            
                If !工序编号 = lngFromID Then
                
                    '!产品编号 = lcProducts.Value
                    '!工序编号 = Val(txtID.Text)
                    !工序名称 = cboName.Text
                    !单价 = Val(txtPrice.Text)
                    
                    Call FillUpdateRecordData(rsADO)
                    
                    .Update
                    
                    Exit Do
                    
                End If
                
                .MoveNext
                
            Loop
            
        ElseIf lngFromID < lngToID Then
        
            Do Until .EOF
            
                If !工序编号 = lngFromID Then
                
                    '!产品编号 = lcProducts.Value
                    !工序编号 = -1
                    !工序名称 = cboName.Text
                    !单价 = Val(txtPrice.Text)
                    
                    Call FillUpdateRecordData(rsADO)
                    
                    .Update
                    
                ElseIf lngFromID < !工序编号 And !工序编号 <= lngToID Then
                
                    !工序编号 = !工序编号 - 1
                    .Update
                    
                ElseIf !工序编号 > lngToID Then
                
                    Exit Do
                    
                End If
                
                .MoveNext
                
            Loop
            
            .MoveFirst
            
            Do Until .EOF
                
                If !工序编号 = -1 Then
                    !工序编号 = lngToID
                    .Update
                    Exit Do
                End If
                
                .MoveNext
                
            Loop
            
        Else
        
            .MoveLast
            
            Do Until .BOF
            
                If !工序编号 = lngFromID Then
                
                    '!产品编号 = lcProducts.Value
                    !工序编号 = -1
                    !工序名称 = cboName.Text
                    !单价 = Val(txtPrice.Text)
                    
                    Call FillUpdateRecordData(rsADO)
                    
                    .Update
                    
                ElseIf lngToID <= !工序编号 And !工序编号 < lngFromID Then
                
                    !工序编号 = !工序编号 + 1
                    .Update
                    
                ElseIf !工序编号 < lngToID Then
                
                    Exit Do
                    
                End If
                
                .MovePrevious
                
            Loop
            
            .MoveFirst
            
            Do Until .EOF
                
                If !工序编号 = -1 Then
                    !工序编号 = lngToID
                    .Update
                    Exit Do
                End If
                
                .MoveNext
                
            Loop
            
        End If
        
    End With
    
    blnEditing = False
    Call DoLock
    
    Call DisplayList
    Call RefreshNameList
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    MsgBox "修改记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

Private Sub lcProducts_Click()

    If lcProducts.Value = "" Then
        Exit Sub
    End If
    
    Call DisplayList
    
End Sub

Private Sub lcProducts_KeyPress(KeyAscii As Integer)

    txtFilter.Visible = True
    txtFilter.SetFocus
    txtFilter.Text = Trim(Chr(KeyAscii))
    txtFilter.SelStart = Len(txtFilter.Text)
    lstFilteredProducts.Visible = True
    
End Sub

Private Sub lstFilteredProducts_DblClick()

    txtID.SetFocus
    
End Sub

Private Sub lstFilteredProducts_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyUp Then
        If lstFilteredProducts.Selected(0) Then
            txtFilter.SetFocus
        End If
    ElseIf KeyCode = vbKeyReturn Then
        txtID.SetFocus
    End If
    
End Sub

Private Sub lstFilteredProducts_LostFocus()

    If ActiveControl.Name <> txtFilter.Name Then
        txtFilter.Visible = False
        lstFilteredProducts.Visible = False
        lcProducts.Value = m_ProductIDs.Item(lstFilteredProducts.ListIndex + 1)
    End If
    
End Sub

Private Sub lvw_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    lvw.SortKey = ColumnHeader.Index - 1
    
End Sub

Private Sub lvw_ItemClick(ByVal Item As MSComctlLib.ListItem)

    If blnAdding Or blnEditing Then
        Call CancelSave
    End If
    
    Call DisplayRecord
    
End Sub

Private Sub txtFilter_Change()

    If ActiveControl.Name = txtFilter.Name Then
        Call DisplayFilteredProducts
    End If
    
End Sub

Private Sub txtFilter_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyDown Then
        lstFilteredProducts.SetFocus
    End If
    
End Sub

Private Sub txtFilter_LostFocus()

    If ActiveControl.Name <> lstFilteredProducts.Name Then
        txtFilter.Visible = False
        lstFilteredProducts.Visible = False
        If lstFilteredProducts.ListIndex >= 0 Then
            lcProducts.Value = m_ProductIDs.Item(lstFilteredProducts.ListIndex + 1)
        End If
    End If

End Sub

Private Sub txtID_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub cboName_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtPrice_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub
