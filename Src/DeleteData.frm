VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form mfrmDeleteData 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "删除过期数据"
   ClientHeight    =   2445
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7035
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2445
   ScaleWidth      =   7035
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkProcedureSheet 
      Caption         =   "生产单记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   0
      Top             =   720
      Width           =   1515
   End
   Begin VB.CheckBox chkIncome 
      Caption         =   "收款记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4920
      TabIndex        =   2
      Top             =   720
      Width           =   1275
   End
   Begin VB.CheckBox chkSales 
      Caption         =   "发货记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3480
      TabIndex        =   1
      Top             =   720
      Width           =   1275
   End
   Begin VB.ComboBox cboDate 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "DeleteData.frx":0000
      Left            =   1260
      List            =   "DeleteData.frx":002B
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   1200
      Width           =   1635
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "确认删除(&D)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   2550
      TabIndex        =   6
      Top             =   1800
      Width           =   1935
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   3240
      TabIndex        =   4
      Top             =   1200
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   54067201
      CurrentDate     =   37302
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   5220
      TabIndex        =   5
      Top             =   1200
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   54067201
      CurrentDate     =   37302.9999884259
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "要删除的内容"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   240
      TabIndex        =   11
      Top             =   720
      Width           =   1440
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "从"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   9
      Left            =   2940
      TabIndex        =   10
      Top             =   1260
      Width           =   240
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "到"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   10
      Left            =   4920
      TabIndex        =   9
      Top             =   1260
      Width           =   240
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "时间区间"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   11
      Left            =   240
      TabIndex        =   8
      Top             =   1260
      Width           =   960
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "提示：删除前请备份现有数据！！"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   240
      Index           =   0
      Left            =   240
      TabIndex        =   7
      Top             =   240
      Width           =   3825
   End
End
Attribute VB_Name = "mfrmDeleteData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboDate_Click()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        dtpFrom.Enabled = True
        dtpTo.Enabled = True
    Else
        dtpFrom.Enabled = False
        dtpTo.Enabled = False
    End If
    
    SetDateRange cboDate.List(cboDate.ListIndex), dtpFrom, dtpTo
    
End Sub

Private Sub cmdDelete_Click()

    Call DeleteData(dtpFrom.Value, dtpTo.Value)
    
End Sub

' 删除指定时间段的数据
Private Sub DeleteData(dateFrom As Date, dateTo As Date)
    
    If chkProcedureSheet.Value = vbUnchecked And chkSales.Value = vbUnchecked And chkIncome.Value = vbUnchecked Then
        MsgBox "请选择需要删除的数据。", vbInformation, msgPrompt
        GoTo PROC_EXIT
    End If
    
    If MsgBox("确实要删除 " & Format(dateFrom, gc_DateFormat) & " 到 " & Format(dateTo, gc_DateFormat) & " 的数据吗？", vbQuestion + vbYesNo, msgPrompt) = vbNo Then
        GoTo PROC_EXIT
    End If
    
    Screen.MousePointer = vbHourglass
    DoEvents
    
    On Error GoTo PROC_ERR
    
    If chkProcedureSheet.Value = vbChecked Then
        g_cnADO.Execute "delete from 工序单主表 where 日期 between #" & Format(dateFrom, gc_DateFormat) & "# and #" & Format(dateTo, gc_DateFormat) & "#", , adCmdText
    End If
    
    If chkSales.Value = vbChecked Then
        g_cnADO.Execute "delete from 发货记录 where 日期 between #" & Format(dateFrom, gc_DateFormat) & "# and #" & Format(dateTo, gc_DateFormat) & "#", , adCmdText
    End If
    
    If chkIncome.Value = vbChecked Then
        g_cnADO.Execute "delete from 收款记录 where 日期 between #" & Format(dateFrom, gc_DateFormat) & "# and #" & Format(dateTo, gc_DateFormat) & "#", , adCmdText
    End If
    
    MsgBox "数据已成功删除！", vbInformation, msgOperationSuccess
    
PROC_EXIT:
    Screen.MousePointer = vbNormal
    Exit Sub
    
PROC_ERR:
    MsgBox "删除数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 初始化必要数据
Private Sub InitData()
    
    InitDateRange g_cnADO, "工序单主表,发货记录,收款记录", dtpFrom, dtpTo
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
    
    cboDate.ListIndex = 9
    
End Sub
