VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{19A6DCA1-B1C0-11D5-9712-5254AB22AAF0}#2.0#0"; "lc.ocx"
Begin VB.Form frmSales 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "发货管理"
   ClientHeight    =   6450
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9630
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6450
   ScaleWidth      =   9630
   StartUpPosition =   1  'CenterOwner
   Begin TabDlg.SSTab sstab 
      Height          =   6315
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   11139
      _Version        =   393216
      Style           =   1
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   635
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   " 浏览发货记录 "
      TabPicture(0)   =   "Sales.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lbl(4)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lbl(5)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lbl(0)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lvw"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "txtSum1"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "txtSum2"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdDelete"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cmdPay"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "cmdUnPay"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "cmdQuery"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "cboDate2"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).ControlCount=   11
      TabCaption(1)   =   " 录入发货记录 "
      TabPicture(1)   =   "Sales.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lbl(1)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lbl(8)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "lbl(7)"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "lvwSaleDetail"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "dtp"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "txtTotal"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "lcClients"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "cmdSave"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "cmdPrint"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "lcProducts"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "txtPrice"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "txtAmount"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).Control(12)=   "txtMoney"
      Tab(1).Control(12).Enabled=   0   'False
      Tab(1).Control(13)=   "cmdDeleteSaleDetailItem"
      Tab(1).Control(13).Enabled=   0   'False
      Tab(1).Control(14)=   "cmdAdd"
      Tab(1).Control(14).Enabled=   0   'False
      Tab(1).Control(15)=   "cmdModify"
      Tab(1).Control(15).Enabled=   0   'False
      Tab(1).Control(16)=   "cmdCancel"
      Tab(1).Control(16).Enabled=   0   'False
      Tab(1).Control(17)=   "lstFilteredProducts"
      Tab(1).Control(17).Enabled=   0   'False
      Tab(1).Control(18)=   "txtFilter"
      Tab(1).Control(18).Enabled=   0   'False
      Tab(1).ControlCount=   19
      TabCaption(2)   =   " 统计发货记录 "
      TabPicture(2)   =   "Sales.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "lbl(10)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "lbl(9)"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "lbl(11)"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "lcClientsT"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "dtpFrom"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "dtpTo"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "cboDate"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "cmdTotal"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).Control(8)=   "cmdTotalByClient"
      Tab(2).Control(8).Enabled=   0   'False
      Tab(2).Control(9)=   "cmdTotalByDate"
      Tab(2).Control(9).Enabled=   0   'False
      Tab(2).Control(10)=   "chkClients"
      Tab(2).Control(10).Enabled=   0   'False
      Tab(2).ControlCount=   11
      TabCaption(3)   =   " 统计结果 "
      TabPicture(3)   =   "Sales.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "lvwTotal"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).ControlCount=   1
      Begin VB.TextBox txtFilter 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -73740
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   2040
         Visible         =   0   'False
         Width           =   3255
      End
      Begin VB.ListBox lstFilteredProducts 
         Height          =   3210
         Left            =   -73740
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   2400
         Visible         =   0   'False
         Width           =   3255
      End
      Begin VB.CheckBox chkClients 
         Caption         =   "全部"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   -67140
         TabIndex        =   41
         Top             =   2467
         Value           =   1  'Checked
         Width           =   855
      End
      Begin VB.CommandButton cmdTotalByDate 
         Caption         =   "按日期统计发货记录"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -74580
         TabIndex        =   35
         Top             =   1320
         Width           =   2715
      End
      Begin VB.CommandButton cmdTotalByClient 
         Caption         =   "按客户统计发货记录"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -74580
         TabIndex        =   36
         Top             =   1860
         Width           =   2715
      End
      Begin VB.ComboBox cboDate2 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Sales.frx":0070
         Left            =   1320
         List            =   "Sales.frx":0095
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   5340
         Width           =   1575
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "取消(&C)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -68640
         TabIndex        =   25
         Top             =   5760
         Width           =   1335
      End
      Begin VB.CommandButton cmdModify 
         Caption         =   "修改(&M)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -73200
         TabIndex        =   22
         Top             =   5760
         Width           =   1335
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "添加(&A)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -74580
         TabIndex        =   21
         Top             =   5760
         Width           =   1335
      End
      Begin VB.CommandButton cmdDeleteSaleDetailItem 
         Caption         =   "删除选中项(&D)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -71820
         TabIndex        =   23
         Top             =   5760
         Width           =   1755
      End
      Begin VB.TextBox txtMoney 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -67620
         TabIndex        =   19
         Top             =   2040
         Visible         =   0   'False
         Width           =   1440
      End
      Begin VB.TextBox txtAmount 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -69060
         TabIndex        =   18
         Top             =   2040
         Width           =   1440
      End
      Begin VB.TextBox txtPrice 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -70500
         TabIndex        =   17
         Top             =   2040
         Width           =   1440
      End
      Begin LabelComboBox.lc lcProducts 
         Height          =   360
         Left            =   -73740
         TabIndex        =   16
         ToolTipText     =   "按空格键输入过滤条件"
         Top             =   2040
         Width           =   3240
         _ExtentX        =   5715
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ListIndex       =   -1
         Spacing         =   0
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "打印(&P)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -67260
         TabIndex        =   26
         Top             =   5760
         Width           =   1335
      End
      Begin VB.CommandButton cmdTotal 
         Caption         =   "浏览客户发货记录"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -74580
         TabIndex        =   37
         Top             =   2400
         Width           =   2715
      End
      Begin VB.ComboBox cboDate 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Sales.frx":00EF
         Left            =   -73560
         List            =   "Sales.frx":0117
         Style           =   2  'Dropdown List
         TabIndex        =   30
         Top             =   660
         Width           =   1695
      End
      Begin VB.CommandButton cmdQuery 
         Caption         =   "收款查询(&Q)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   5400
         TabIndex        =   7
         Top             =   5760
         Width           =   1935
      End
      Begin VB.CommandButton cmdUnPay 
         Caption         =   "标记为未付款(&U)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   2820
         TabIndex        =   6
         Top             =   5760
         Width           =   2415
      End
      Begin VB.CommandButton cmdPay 
         Caption         =   "标记为已付款(&F)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   240
         TabIndex        =   5
         Top             =   5760
         Width           =   2415
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "删除(&D)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   7500
         TabIndex        =   8
         Top             =   5760
         Width           =   1695
      End
      Begin VB.TextBox txtSum2 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   7440
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   5340
         Width           =   1815
      End
      Begin VB.TextBox txtSum1 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4440
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   5340
         Width           =   1815
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "保存(&S)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -70020
         TabIndex        =   24
         Top             =   5760
         Width           =   1335
      End
      Begin LabelComboBox.lc lcClients 
         Height          =   360
         Left            =   -74700
         TabIndex        =   12
         Top             =   1260
         Width           =   3795
         _ExtentX        =   6694
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "客户名称"
         ListIndex       =   -1
         Spacing         =   90
      End
      Begin VB.TextBox txtTotal 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -67260
         Locked          =   -1  'True
         TabIndex        =   20
         Top             =   1260
         Width           =   1395
      End
      Begin MSComctlLib.ListView lvw 
         Height          =   4755
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   9195
         _ExtentX        =   16219
         _ExtentY        =   8387
         SortKey         =   1
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "序号"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "日期"
            Object.Width           =   2857
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "客户名称"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "金额"
            Object.Width           =   3704
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "付款情况"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComCtl2.DTPicker dtp 
         Height          =   375
         Left            =   -70140
         TabIndex        =   13
         Top             =   1260
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   54132737
         CurrentDate     =   37302
      End
      Begin MSComCtl2.DTPicker dtpTo 
         Height          =   375
         Left            =   -69180
         TabIndex        =   34
         Top             =   675
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   661
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   54132737
         CurrentDate     =   37302.9999884259
      End
      Begin MSComCtl2.DTPicker dtpFrom 
         Height          =   375
         Left            =   -71280
         TabIndex        =   32
         Top             =   675
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   661
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   54132737
         CurrentDate     =   37302
      End
      Begin MSComctlLib.ListView lvwSaleDetail 
         Height          =   3975
         Left            =   -74700
         TabIndex        =   38
         Top             =   1680
         Width           =   8835
         _ExtentX        =   15584
         _ExtentY        =   7011
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "序号"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "产品名称"
            Object.Width           =   5715
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "单价"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "数量"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "金额"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "产品编号"
            Object.Width           =   0
         EndProperty
      End
      Begin LabelComboBox.lc lcClientsT 
         Height          =   360
         Left            =   -71640
         TabIndex        =   40
         Top             =   2400
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "客户名称"
         ListIndex       =   -1
      End
      Begin MSComctlLib.ListView lvwTotal 
         Height          =   5655
         Left            =   -74820
         TabIndex        =   42
         Top             =   480
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   9975
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "日期"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "产品名称"
            Object.Width           =   6879
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "数量"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "金额"
            Object.Width           =   3598
         EndProperty
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "时间区间"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   240
         TabIndex        =   39
         Top             =   5400
         Width           =   960
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "时间区间"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   11
         Left            =   -74640
         TabIndex        =   29
         Top             =   720
         Width           =   960
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "从"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   9
         Left            =   -71640
         TabIndex        =   31
         Top             =   720
         Width           =   240
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "到"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   10
         Left            =   -69540
         TabIndex        =   33
         Top             =   720
         Width           =   240
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "付款合计"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   5
         Left            =   6390
         MousePointer    =   9  'Size W E
         TabIndex        =   28
         ToolTipText     =   "切换付款/欠款合计"
         Top             =   5400
         Width           =   960
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "交易额合计"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   3120
         TabIndex        =   27
         Top             =   5400
         Width           =   1200
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "合计金额"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   7
         Left            =   -68340
         TabIndex        =   11
         Top             =   1320
         Width           =   960
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "柳州市东苑制衣厂发货单"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   18
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         Left            =   -72247
         TabIndex        =   10
         Top             =   660
         Width           =   4125
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "日期"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   -70740
         TabIndex        =   9
         Top             =   1320
         Width           =   480
      End
   End
End
Attribute VB_Name = "frmSales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_ccyTotal As Currency, m_ccyTotalPay As Currency ' 合计发货金额和合计付款金额
Dim m_lngCurrentRow As Long ' 发货明细记录列表当前选中的项的序号
Dim m_ProductIDs As New Collection
Dim m_RecentProducts As New Collection
Dim m_lngMaxRecentCount As Long

Dim blnAdding As Boolean
Dim blnEditing As Boolean

Public Sub Add()

    Call cmdAdd_Click
    
End Sub

' 放弃保存
Private Sub CancelSave()
    
    blnAdding = False
    blnEditing = False
    
    If TypeName(lvw.SelectedItem) = "IListItem" Then
        Call lvw_ItemClick(lvw.SelectedItem)
    Else
        Call ClearDisplay
    End If
    
    Call DoLock
    
End Sub

Private Sub cboDate_Click()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        dtpFrom.Enabled = True
        dtpTo.Enabled = True
    Else
        dtpFrom.Enabled = False
        dtpTo.Enabled = False
    End If
    
    SetDateRange cboDate.List(cboDate.ListIndex), dtpFrom, dtpTo
    
End Sub

Private Sub cboDate2_Click()

    If TypeName(ActiveControl) <> "Nothing" Then
        If ActiveControl.Name = cboDate2.Name Then
            If cboDate2.List(cboDate2.ListIndex) = "全部" Then
                cboDate.ListIndex = cboDate2.ListIndex + 1
            Else
                cboDate.ListIndex = cboDate2.ListIndex
            End If
            'If cboDate2.ListIndex <> 10 Then
                Call cmdTotal_Click
            'End If
        End If
    End If
    
End Sub

' 检查输入的内容
Private Function CheckInput() As Boolean
    
    Dim i As Long
    
    CheckInput = True
    
    If lcClients.Value = "" Or lcClients.Value = "AddNew" Then
        MsgBox "必须输入客户名称。", vbInformation, msgPrompt
        lcClients.SetFocus
        CheckInput = False
        Exit Function
    End If
    
'    If Val(txtTotal.Text) = 0 Then
'        MsgBox "必须输入金额。", vbInformation, msgPrompt
'        txtTotal.SetFocus
'        CheckInput = False
'        Exit Function
'    End If
    
    If lvwSaleDetail.ListItems.Count < 1 Then
        MsgBox "必须输入发货明细记录。", vbInformation, msgPrompt
        CheckInput = False
        Exit Function
    End If
    
    For i = 1 To lvwSaleDetail.ListItems.Count
    
        If lvwSaleDetail.ListItems(i).SubItems(5) = "" And lvwSaleDetail.ListItems(i).SubItems(2) = "" And lvwSaleDetail.ListItems(i).SubItems(3) = "" Then
        
            If m_lngCurrentRow = i And lcProducts.Value <> "" And Val(txtPrice.Text) > 0 And Val(txtAmount.Text) <> 0 Then
            
                ' 如果输入的内容未存储到列表中，先存储当前行输入的内容
                lvwSaleDetail.ListItems(i).SubItems(1) = lcProducts.Text
                lvwSaleDetail.ListItems(i).SubItems(2) = Val(txtPrice.Text)
                lvwSaleDetail.ListItems(i).SubItems(3) = Val(txtAmount.Text)
                lvwSaleDetail.ListItems(i).SubItems(4) = Val(txtPrice.Text) * Val(txtAmount.Text) ' Val(txtMoney.Text)
                lvwSaleDetail.ListItems(i).SubItems(5) = lcProducts.Value
                Call SumMoney
                
            'End If
            ElseIf i <> lvwSaleDetail.ListItems.Count Or lvwSaleDetail.ListItems.Count = 1 Or lvwSaleDetail.ListItems(i).SubItems(5) <> "" And (Val(txtPrice.Text) <= 0 Or Val(txtAmount.Text) = 0) Then
            
                ' 发生异常，列表中间出现了空行
                MsgBox "发货明细未填写完整，请填写完整。", vbInformation, msgPrompt
                CheckInput = False
                Exit For
                
            Else
            
                ' 该行是最后一行，且未填写，将其删除
                lvwSaleDetail.ListItems.Remove i
                Exit For
                
            End If
            
        ElseIf lvwSaleDetail.ListItems(i).SubItems(5) = "" Then
        
            If m_lngCurrentRow = i And lcProducts.Value <> "" And Val(txtPrice.Text) > 0 And Val(txtAmount.Text) <> 0 Then
                lvwSaleDetail.ListItems(i).SubItems(5) = lcProducts.Value
                lvwSaleDetail.ListItems(i).SubItems(1) = lcProducts.Text
                lvwSaleDetail.ListItems(i).SubItems(2) = Val(txtPrice.Text)
                lvwSaleDetail.ListItems(i).SubItems(3) = Val(txtAmount.Text)
                lvwSaleDetail.ListItems(i).SubItems(4) = Val(txtPrice.Text) * Val(txtAmount.Text)
            ElseIf lcProducts.Value <> "" Or Val(txtPrice.Text) > 0 Or Val(txtAmount.Text) <> 0 Then
                MsgBox "发货明细记录中的第 " & i & " 条未填写完整，无法保存记录，请填写完整或删除不完整的条目再保存。", vbInformation, msgPrompt
                CheckInput = False
                Exit For
            End If
            
'        ElseIf Val(lvwSaleDetail.ListItems(i).SubItems(2)) = 0 Then

'            If MsgBox("发货明细记录中的第 " & i & " 条单价为 0 ，是否继续保存？", vbQuestion + vbYesNo, msgPrompt) = vbNo Then
'                CheckInput = False
'                Exit For
'            End If

'        ElseIf Val(lvwSaleDetail.ListItems(i).SubItems(4)) = 0 Then

'            If MsgBox("发货明细记录中的第 " & i & " 条数量为 0 ，是否继续保存？", vbQuestion + vbYesNo, msgPrompt) = vbNo Then
'                CheckInput = False
'                Exit For
'            End If

'        ElseIf Val(lvwSaleDetail.ListItems(i).SubItems(2)) <= 0 Then

'            MsgBox "发货明细记录中的第 " & i & " 条单价无效，请修改。", vbInformation, msgPrompt
'            CheckInput = False
'            Exit For

'        ElseIf Val(lvwSaleDetail.ListItems(i).SubItems(4)) <= 0 Then

'            MsgBox "发货明细记录中的第 " & i & " 条数量无效，请修改。", vbInformation, msgPrompt
'            CheckInput = False
'            Exit For

        Else
        
            lvwSaleDetail.ListItems(i).SubItems(4) = Val(lvwSaleDetail.ListItems(i).SubItems(2)) * Val(lvwSaleDetail.ListItems(i).SubItems(3))
            
        End If
        
    Next i
    
    If lvwSaleDetail.ListItems.Count > 1 And lvwSaleDetail.ListItems(lvwSaleDetail.ListItems.Count).SubItems(5) = "" Then
        lvwSaleDetail.ListItems.Remove lvwSaleDetail.ListItems.Count
    End If
    
    Call SumMoney
    
End Function

Private Sub chkClients_Click()

    If chkClients.Value = vbChecked Then
    
        If lcClientsT.Value <> "*" Then
            lcClientsT.Value = "*"
        End If
        
        DoEvents
        
        chkClients.Value = vbChecked
        
    End If
    
End Sub

' 清除发货明细单当前显示的内容
Private Sub ClearDisplay()
    
    lcClients.Value = ""
    dtp.Value = Date
    txtTotal.Text = ""
    lcProducts.Value = ""
    txtPrice.Text = ""
    txtAmount.Text = ""
    txtMoney.Text = ""
    lvwSaleDetail.ListItems.Clear
    m_lngCurrentRow = 1
    
End Sub

Private Sub cmdAdd_Click()

    If blnAdding Or blnEditing Then
    
        Exit Sub
        
    End If
    
    blnAdding = True
    sstab.Tab = 1
    
    Call DoLock
    Call ClearDisplay
    
    lvwSaleDetail.ListItems.Add , , "1"
    m_lngCurrentRow = 1
    
    Call MoveControls(m_lngCurrentRow)
    DoEvents
    
    On Error Resume Next
    
    lcClients.SetFocus
    
End Sub

Private Sub cmdCancel_Click()

    Call CancelSave
    
End Sub

Private Sub cmdDelete_Click()

    Call DeleteRecord
    
End Sub

Private Sub cmdDeleteSaleDetailItem_Click()

    Call DeleteRow(m_lngCurrentRow)
    
End Sub

Private Sub cmdModify_Click()

    If blnAdding Or blnEditing Then
    
        Exit Sub
        
    Else
    
        If TypeName(lvw.SelectedItem) <> "IListItem" Or lcClients.Value = "" Then
            Exit Sub
        End If
        
        blnEditing = True
        Call DoLock
        
        If m_lngCurrentRow <= lvwSaleDetail.ListItems.Count Then
            Call lvwSaleDetail_ItemClick(lvwSaleDetail.ListItems(m_lngCurrentRow))
        ElseIf lvwSaleDetail.ListItems.Count = 0 Then
            Call lvwSaleDetail.ListItems.Add(, , "1")
            Call MoveControls(1)
        End If
        
        lcClients.SetFocus
        
    End If
    
End Sub

Private Sub cmdPay_Click()

    Call DoPay
    
End Sub

Private Sub cmdPrint_Click()

'    Me.Hide
'    Me.Show
'    Call DoPreview
    Call DoPrint
    Call PrintMe
    
End Sub

Private Sub cmdQuery_Click()

    Load frmIncome
    frmIncome.lcClientsT.Value = lcClientsT.Value
    frmIncome.sstab.Tab = 2
    frmIncome.Show vbModal, Me
    
End Sub

Private Sub cmdSave_Click()

    'If blnAdding Then
        Call SaveAdd
    'ElseIf blnEditing Then
        'Call SaveEdit
    'End If
    
End Sub

Private Sub cmdTotal_Click()

    Call DoQuery
    
    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        cboDate2.ListIndex = -1
    ElseIf cboDate.List(cboDate.ListIndex) = "全部" Then
        cboDate2.ListIndex = cboDate.ListIndex - 1
    Else
        cboDate2.ListIndex = cboDate.ListIndex
    End If
    
End Sub

Private Sub cmdTotalByClient_Click()

    Call TotalByClient
    
End Sub

Private Sub cmdTotalByDate_Click()

    Call TotalByDate
    
End Sub

Private Sub cmdUnPay_Click()

    Call DoUnPay
    
End Sub

' 删除一条发货记录
Private Sub DeleteRecord()
    
    If sstab.Tab <> 0 Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    If MsgBox("确实要删除这条记录吗？", vbInformation + vbYesNo, msgPrompt) = vbNo Then
        Exit Sub
    End If
    
    g_cnADO.Execute "delete from 发货记录 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText
    
    Call RemoveSelectedRow(lvw)
    
    lvw.SetFocus
    
End Sub

' 删除一行明细记录
Private Sub DeleteRow(lngRow As Long)
    
    Dim i As Long
    
    If lngRow <= 0 Or lngRow > lvwSaleDetail.ListItems.Count Then
        Exit Sub
    End If
    
    ' 如果列表中只有一行，则清除当前行的内容，否则从列表移除一行
    If lvwSaleDetail.ListItems.Count = 1 Then
        lvwSaleDetail.ListItems(lngRow).SubItems(1) = ""
        lvwSaleDetail.ListItems(lngRow).SubItems(2) = ""
        lvwSaleDetail.ListItems(lngRow).SubItems(3) = ""
        lvwSaleDetail.ListItems(lngRow).SubItems(4) = ""
        lvwSaleDetail.ListItems(lngRow).SubItems(5) = ""
        lcProducts.Value = ""
        txtPrice.Text = ""
        txtAmount.Text = ""
        txtMoney.Text = ""
    Else
        Call lvwSaleDetail.ListItems.Remove(lngRow)
    End If
    
    ' 如果删除的是最后一行，需调整行号
    If lngRow > lvwSaleDetail.ListItems.Count Then
        lngRow = lvwSaleDetail.ListItems.Count
    End If
    
    ' 更新被移除行后的行序号
    If lngRow > 0 And lngRow <= lvwSaleDetail.ListItems.Count Then
        For i = lngRow To lvwSaleDetail.ListItems.Count
            lvwSaleDetail.ListItems(i).Text = i
        Next i
        ' 重置当前选中的行
        m_lngCurrentRow = lngRow
        Set lvwSaleDetail.SelectedItem = lvwSaleDetail.ListItems(lngRow)
        Call lvwSaleDetail_ItemClick(lvwSaleDetail.SelectedItem)
    End If
    
End Sub

' 显示一条发货记录
Private Sub DisplayRecord(strGUID As String)
    
    Call ClearDisplay
    
    Dim lngRecordNo As Long
    Dim itemTemp As ListItem
    
    With g_cnADO.Execute("select * from 发货记录 where GUID = '" & strGUID & "'", , adCmdText)
    
        If .BOF And .EOF Then
            Exit Sub
        End If
        
        lcClients.Value = !客户编号
        dtp.Value = !日期
        txtTotal.Text = !金额
        lngRecordNo = !记录号
        
    End With
    
    With g_cnADO.Execute("select 发货明细记录.*, 产品.名称 from 发货明细记录 inner join 产品 on 发货明细记录.产品编号 = 产品.编号 where 记录号 = " & lngRecordNo, , adCmdText)
    
        Do Until .EOF
        
            Set itemTemp = lvwSaleDetail.ListItems.Add(, , lvwSaleDetail.ListItems.Count + 1)
            itemTemp.SubItems(1) = .Fields("名称").Value
            itemTemp.SubItems(2) = .Fields("单价").Value
            itemTemp.SubItems(3) = .Fields("数量").Value
            itemTemp.SubItems(4) = .Fields("金额").Value
            itemTemp.SubItems(5) = .Fields("产品编号").Value
            
            .MoveNext
            
        Loop
        
    End With
    
End Sub

' 显示已过滤的产品列表
Private Sub DisplayFilteredProducts()

    lstFilteredProducts.Clear
    Set m_ProductIDs = New Collection
    
    txtFilter.Tag = Replace(Trim(txtFilter.Text), "'", "''")
    
    If txtFilter.Tag = "" Then
    
        Dim i As Long
        Dim lngPos As Long
        
        ' 把最近使用的产品排在前面
        For i = 1 To m_RecentProducts.Count
        
            lngPos = InStr(m_RecentProducts.Item(i), ",")
            
            lstFilteredProducts.AddItem Mid(m_RecentProducts.Item(i), lngPos + 1)
            m_ProductIDs.Add Left(m_RecentProducts.Item(i), lngPos - 1)
            
        Next i
        
    End If
    
    Dim strSQL As String
    
    strSQL = "select * from 产品 where 名称 like '%" & txtFilter.Tag & "%' and not 停产 order by 名称"
    
    With g_cnADO.Execute(strSQL, , adCmdText)
        Do Until .EOF
            lstFilteredProducts.AddItem !名称
            m_ProductIDs.Add !编号 & ""
            .MoveNext
        Loop
    End With
    
End Sub

' 显示发货记录列表
Public Sub DisplayList()
    
    Dim strSQL As String
    
    If lcClientsT.Value = "*" Then
        strSQL = ""
    Else
        strSQL = "名称='" & lcClientsT.Text & "'"
    End If
    
    If Trim(strSQL) <> "" Then
        strSQL = strSQL & " and "
    End If
    
    strSQL = strSQL & "日期 between #" & Format(dtpFrom.Value, gc_DateFormat) & "# and #" & Format(dtpTo.Value, gc_DateFormat) & " 23:59:59#"
    
    If Trim(strSQL) = "" Then
        strSQL = "select * from 发货记录列表"
    Else
        strSQL = "select * from 发货记录列表 where " & strSQL
    End If
    
    Dim itemTemp As ListItem
    Dim i As Long
    
    m_ccyTotal = 0
    m_ccyTotalPay = 0
    
    Screen.MousePointer = vbHourglass
    lvw.Visible = False
    DoEvents
    
    lvw.ListItems.Clear
    i = 1
    
    With g_cnADO.Execute(strSQL, , adCmdText)
    
        Do Until .EOF
        
            Set itemTemp = lvw.ListItems.Add(, !GUID, i)
            itemTemp.SubItems(1) = Format(!日期, gc_DateFormat)
            itemTemp.SubItems(2) = !名称 & ""
            itemTemp.SubItems(3) = FormatMoney(!金额)
            itemTemp.SubItems(4) = FormatMoney(!付款)
            m_ccyTotal = m_ccyTotal + !金额
            m_ccyTotalPay = m_ccyTotalPay + !付款
            
            .MoveNext
            i = i + 1
            
        Loop
        
    End With
    
PROC_EXIT:
    Call DisplayTotal
    lvw.Visible = True
    Screen.MousePointer = vbNormal
    DoEvents
    
End Sub

Public Sub DisplayTotal()

    txtSum1.Text = FormatMoney(m_ccyTotal)
    txtSum2.Text = FormatMoney(m_ccyTotalPay)
    
End Sub

Private Sub DoLock()

    txtMoney.Visible = False
    
    If blnAdding Or blnEditing Then
        cmdAdd.Enabled = False
        cmdModify.Enabled = False
        cmdDeleteSaleDetailItem.Enabled = True
        cmdSave.Enabled = True
        cmdCancel.Enabled = True
        cmdPrint.Enabled = False
        lcClients.Enabled = True
        dtp.Enabled = True
        lcProducts.Visible = True
        txtPrice.Visible = True
        txtAmount.Visible = True
    Else
        cmdAdd.Enabled = True
        cmdModify.Enabled = True
        cmdDeleteSaleDetailItem.Enabled = False
        cmdSave.Enabled = False
        cmdCancel.Enabled = False
        cmdPrint.Enabled = True
        lcClients.Enabled = False
        dtp.Enabled = False
        lcProducts.Visible = False
        txtPrice.Visible = False
        txtAmount.Visible = False
    End If
    
End Sub

' 标记一条发货记录为已付款
Private Sub DoPay()
    
    Dim ccyTotalReceived As Currency, ccyTotalMarkedPayd As Currency
    Dim strClientID As String
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    ccyTotalMarkedPayd = -1
    ccyTotalReceived = -1
    
    With g_cnADO.Execute("select * from 发货记录 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            strClientID = !客户编号 & ""
        Else
            Exit Sub
        End If
    End With
    
    With g_cnADO.Execute("select sum(付款) from 发货记录 where 客户编号 = '" & strClientID & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            ccyTotalMarkedPayd = IIf(IsNull(.Fields(0).Value), 0, .Fields(0).Value)
        End If
    End With
            
    With g_cnADO.Execute("select sum(金额) + 0 from 收款记录 where 客户编号 = '" & strClientID & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            ccyTotalReceived = IIf(IsNull(.Fields(0).Value), 0, .Fields(0).Value)
        End If
    End With
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 发货记录 where GUID = '" & lvw.SelectedItem.Key & "'", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If Not (.BOF And .EOF) Then
        
            If ccyTotalReceived <> -1 And ccyTotalMarkedPayd <> -1 And ccyTotalReceived < ccyTotalMarkedPayd + !金额 - !付款 Then
                If MsgBox("该客户实际支付的金额不足以完成该操作。仍然要继续吗？", vbQuestion + vbYesNo, msgPrompt) = vbNo Then
                    Exit Sub
                End If
            End If
            
            m_ccyTotalPay = m_ccyTotalPay + !金额 - !付款
            
            !付款 = !金额
            .Update
            
            lvw.SelectedItem.SubItems(4) = lvw.SelectedItem.SubItems(3)
            
            Call DisplayTotal
            
        End If
        
        .Close
        
    End With
    
    Set rsADO = Nothing
    
End Sub

' 执行统计
Private Sub DoQuery()
    
    Call DisplayList
    
    sstab.Tab = 0
    
End Sub

' 标记一条发货记录为未付款
Private Sub DoUnPay()
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 发货记录 where GUID = '" & lvw.SelectedItem.Key & "'", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If Not (.BOF And .EOF) Then
        
            m_ccyTotalPay = m_ccyTotalPay - !付款
            
            !付款 = 0
            .Update
            
            lvw.SelectedItem.SubItems(4) = FormatMoney(0)
            
            Call DisplayTotal
            
        End If
        
        .Close
        
    End With
    
    Set rsADO = Nothing
    
End Sub

Private Sub dtp_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
        'txtFilter.SetFocus
        lcProducts.SetFocus
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyEscape Then
        Unload Me
    End If

End Sub

Private Sub Form_Load()

    Call InitData
    
    blnAdding = False
    blnEditing = False
    
    Call DoLock
    'Call DisplayList
    
    cboDate.ListIndex = 5
    
    Call cmdTotal_Click
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    If blnAdding Or blnEditing Then
        
        If MsgBox("是否取消编辑？", vbQuestion + vbYesNo) = vbYes Then
            Call cmdCancel_Click
        Else
            Cancel = 1
        End If
        
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)

    m_ccyTotal = 0
    m_ccyTotalPay = 0
    blnAdding = False
    blnEditing = False
    
End Sub

' 加载最近使用的产品集
Private Sub GetRecentProducts()

    If m_RecentProducts.Count > 0 Then
        Exit Sub
    End If
    
    Dim i As Long
    Dim blnExists As Boolean
    Dim lngRecentCount As Long
    
    m_lngMaxRecentCount = 10
    
    With g_cnADO.Execute("select top 50 发货明细记录.产品编号, 产品.名称 " & _
        "from (发货明细记录 left join 产品 on 发货明细记录.产品编号 = 产品.编号) left join 发货记录 on 发货明细记录.记录号 = 发货记录.记录号 " & _
        "order by 发货记录.日期 desc", , adCmdText)
    
        Do Until .EOF
        
            blnExists = False
            
            For i = 1 To m_RecentProducts.Count
                If m_RecentProducts.Item(i) = !产品编号 & "," & !名称 Then
                    blnExists = True
                    Exit For
                End If
            Next i
            
            If Not blnExists Then
            
                m_RecentProducts.Add !产品编号 & "," & !名称
                
                lngRecentCount = lngRecentCount + 1
                
                If lngRecentCount >= m_lngMaxRecentCount Then
                    Exit Do
                End If
                
            End If
            
            .MoveNext
            
        Loop
        
    End With
    
End Sub

Private Sub InitData()

    Set lcClients.DataSource = g_cnADO.Execute("select 编号, 名称 from 客户 where not 停用 order by 名称", , adCmdText)
    Call lcClients.AddItem("<添加新客户>", "<AddNew>")
    
    Set lcClientsT.DataSource = g_cnADO.Execute("select 编号, 名称 from 客户 order by 名称", , adCmdText)
    lcClientsT.AddItem "全部", "*"
    lcClientsT.Value = "*"
    
    Set lcProducts.DataSource = g_cnADO.Execute("select 编号, 名称 from 产品 where not 停产 order by 名称", , adCmdText)
    
    dtp.Value = Date
    
    InitDateRange g_cnADO, "发货记录", dtpFrom, dtpTo
    
    Call GetRecentProducts
    
End Sub

Private Sub lbl_Click(Index As Integer)

    If Index = 5 Then
        If lbl(5).Caption = "付款合计" Then
            lbl(5).Caption = "欠款合计"
            txtSum2.Text = FormatMoney(m_ccyTotal - m_ccyTotalPay)
        Else
            lbl(5).Caption = "付款合计"
            txtSum2.Text = FormatMoney(m_ccyTotalPay)
        End If
    End If
    
End Sub

Private Sub lcClients_Click()

    If lcClients.Value = "<AddNew>" Then
    
        mfrmClients.blnAutoAdd = True
        
        mfrmClients.Show vbModal, Me
        
        Call InitData
        
        If mfrmClients.strNewItemID <> "" Then
            lcClients.Value = mfrmClients.strNewItemID
            mfrmClients.strNewItemID = ""
        End If
        
    End If

End Sub

Private Sub lcClients_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        dtp.SetFocus
    End If
    
End Sub

Private Sub lcClientsT_Click()

    DoEvents
    If lcClientsT.Value <> "*" Then
        chkClients.Value = vbUnchecked
    End If
    
End Sub

Private Sub lcProducts_Click()

    txtPrice.Text = GetSet("Price " & lcProducts.Value, "")
    txtMoney.Text = ToMoney(Val(txtPrice.Text) * Val(txtAmount.Text))
    
    If ActiveControl.Name = lcProducts.Name And lcProducts.Value <> "" Then
        On Error Resume Next
        lvwSaleDetail.ListItems(m_lngCurrentRow).SubItems(1) = lcProducts.Text
        lvwSaleDetail.ListItems(m_lngCurrentRow).SubItems(5) = lcProducts.Value
        lvwSaleDetail.ListItems(m_lngCurrentRow).SubItems(2) = ToMoney(txtPrice.Text)
        lvwSaleDetail.ListItems(m_lngCurrentRow).SubItems(4) = txtMoney.Text
    End If
    
End Sub

Private Sub lcProducts_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        txtPrice.SetFocus
    Else
        txtFilter.Visible = True
        txtFilter.SetFocus
        txtFilter.Text = Trim(Chr(KeyAscii))
        txtFilter.SelStart = Len(txtFilter.Text)
        lstFilteredProducts.Visible = True
    End If
    
End Sub

Private Sub lcProducts_LostFocus()

    Call txtAmount_LostFocus
    
End Sub

Private Sub lvw_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    lvw.SortKey = ColumnHeader.Index - 1
    
End Sub

Private Sub lstFilteredProducts_DblClick()

    txtPrice.SetFocus
    
End Sub

Private Sub lstFilteredProducts_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyUp Then
        If lstFilteredProducts.Selected(0) Then
            txtFilter.SetFocus
        End If
    ElseIf KeyCode = vbKeyReturn Then
        txtPrice.SetFocus
    End If
    
End Sub

Private Sub lstFilteredProducts_LostFocus()

    If ActiveControl.Name <> txtFilter.Name Then
        txtFilter.Visible = False
        lstFilteredProducts.Visible = False
        lcProducts.Value = m_ProductIDs.Item(lstFilteredProducts.ListIndex + 1)
    End If
    
End Sub

Private Sub lvw_DblClick()

    If TypeName(lvw.SelectedItem) = "IListItem" Then
        sstab.Tab = 1
    End If
    
End Sub

Private Sub lvw_ItemClick(ByVal Item As MSComctlLib.ListItem)

    If TypeName(lvw.SelectedItem) <> "IListItem" Then
        Exit Sub
    End If
    
    Call DisplayRecord(Item.Key)
    
End Sub

Private Sub lvwSaleDetail_GotFocus()

    If lcProducts.Visible Then
        lcProducts.SetFocus
    End If
    
End Sub

Private Sub lvwSaleDetail_ItemClick(ByVal Item As MSComctlLib.ListItem)

    m_lngCurrentRow = Item.Index
    
    lcProducts.Value = Item.SubItems(5)
    txtPrice.Text = Item.SubItems(2)
    txtAmount.Text = Item.SubItems(3)
    txtMoney.Text = Item.SubItems(4)
    
    lcProducts.Top = Item.Top + lvwSaleDetail.Top + 30
    txtPrice.Top = Item.Top + lvwSaleDetail.Top + 30
    txtAmount.Top = Item.Top + lvwSaleDetail.Top + 30
    txtMoney.Top = Item.Top + lvwSaleDetail.Top + 30
'    If Item.Left + Item.Width < txtPrice.Left Then
        If lcProducts.Visible Then
            lcProducts.SetFocus
        End If
'    ElseIf Item.Left + Item.Width < txtAmount.Left Then
'        txtPrice.SetFocus
'    ElseIf Item.Left + Item.Width < txtMoney.Left Then
'        txtAmount.SetFocus
'    Else
'        txtMoney.SetFocus
'    End If

End Sub

' 重新调整输入控件位置，与指定的行对齐
Private Sub MoveControls(lngRow As Long)
    
    lcProducts.Top = lvwSaleDetail.ListItems(lngRow).Top + lvwSaleDetail.Top + 30
    txtFilter.Top = lcProducts.Top
    lstFilteredProducts.Top = txtFilter.Top + txtFilter.Height
    txtPrice.Top = lvwSaleDetail.ListItems(lngRow).Top + lvwSaleDetail.Top + 30
    txtAmount.Top = lvwSaleDetail.ListItems(lngRow).Top + lvwSaleDetail.Top + 30
    txtMoney.Top = lvwSaleDetail.ListItems(lngRow).Top + lvwSaleDetail.Top + 30
    
End Sub

' 移动到下一行
Private Sub NextRow(lngCurrentRow As Long)
    
    Dim itemTemp As ListItem
    
    ' 存储当前行输入的内容
    lvwSaleDetail.ListItems(lngCurrentRow).SubItems(1) = lcProducts.Text
    lvwSaleDetail.ListItems(lngCurrentRow).SubItems(2) = ToMoney(txtPrice.Text)
    lvwSaleDetail.ListItems(lngCurrentRow).SubItems(3) = Val(txtAmount.Text)
    lvwSaleDetail.ListItems(lngCurrentRow).SubItems(4) = ToMoney(Val(txtPrice.Text) * Val(txtAmount.Text))
    lvwSaleDetail.ListItems(lngCurrentRow).SubItems(5) = lcProducts.Value
    
    If lngCurrentRow >= lvwSaleDetail.ListItems.Count Then
        ' 创建新行
        Set itemTemp = lvwSaleDetail.ListItems.Add(, , lvwSaleDetail.ListItems.Count + 1)
    Else
        ' 移动到下一行
        Set itemTemp = lvwSaleDetail.ListItems(lngCurrentRow + 1)
    End If
    
    ' 确保当前行可见
    itemTemp.EnsureVisible
    
    ' 刷新当前合计金额
    Call SumMoney
    
    m_lngCurrentRow = lngCurrentRow + 1
    
    ' 重新调整输入控件位置
    Call MoveControls(m_lngCurrentRow)
    
    lvw.SetFocus
    
    Set lvwSaleDetail.SelectedItem = itemTemp
    Call lvwSaleDetail_ItemClick(lvwSaleDetail.SelectedItem)
    
End Sub

' 打印发货单
Private Sub PrintMe()
    
    Dim sngTitleFontSize As Single
    Dim sngClientFontSize As Single
    Dim sngScale As Single
    Dim sngTotalWidth As Single, sngTotalHeight As Single
    Dim sngPageWidth As Single, sngPageHeight As Single
    Dim sngStartX As Single, sngStartY As Single
    Dim sngCurX As Single, sngCurY As Single
    Dim sngTableStartY As Single, sngTableEndY As Single
    Dim lngCurPage As Long, lngTotalPages As Long
    Dim i As Long, j As Long, lngLines As Long, lngPrintedLines As Long
    
    If lvw.ListItems.Count <= 1 Then Exit Sub
    
    ' 限定用 A4 纸打印
    g_Printer.Width = 210 * 56.7
    g_Printer.Height = 297 * 56.7
    
    sngPageWidth = g_Printer.Width / 100 * 80 ' 可打印范围在纸张范围的 80% 以内
    sngPageHeight = g_Printer.Height / 100 * 90
    
    sngTotalWidth = 0
    For i = 1 To lvwSaleDetail.ColumnHeaders.Count
        sngTotalWidth = sngTotalWidth + lvwSaleDetail.ColumnHeaders(i).Width
    Next i
    If sngTotalWidth > sngPageWidth Then
        MsgBox "欲打印列宽之和超出了纸张宽度 " & Format((sngTotalWidth - sngPageWidth) / 56.7, "0.0") & " 毫米，请减少列数或缩小列宽。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    If MsgBox("请准备好打印机和 A4 幅面的纸张，按确定键开始打印发货单，按取消键放弃打印。", vbInformation + vbOKCancel, msgPrompt) = vbCancel Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    Me.Enabled = False
    DoEvents
    
    On Error GoTo PROC_ERR
    
    lngLines = lvwSaleDetail.ListItems.Count + 1
    
    ' 计算打印起始位置
    sngStartX = (g_Printer.Width - sngTotalWidth) / 2
    sngStartY = (g_Printer.Height - sngPageHeight) / 2
    
    sngTitleFontSize = 18
    sngClientFontSize = 12
    
    ' 分页打印
    lngPrintedLines = 1
    lngCurPage = 1
    Do While lngPrintedLines < lngLines
        
        ' 打印表头
        sngCurY = sngStartY
        g_Printer.DrawWidth = 2
        'g_Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        sngCurY = sngCurY + 60
        g_Printer.FontSize = sngTitleFontSize
        g_Printer.FontBold = True
        If lngCurPage = 1 Then
            Call PrintText(sngStartX + (sngPageWidth - g_Printer.TextWidth("柳州市东苑制衣厂发货单")) / 2, sngCurY, sngTitleFontSize, "柳州市东苑制衣厂发货单")
        Else
            Call PrintText(sngStartX + (sngPageWidth - g_Printer.TextWidth("柳州市东苑制衣厂发货单（续）")) / 2, sngCurY, sngTitleFontSize, "柳州市东苑制衣厂发货单（续）")
        End If
        sngCurY = sngCurY + g_Printer.TextHeight("柳") + 240
        'g_Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        g_Printer.DrawWidth = 1
        
        g_Printer.FontSize = sngClientFontSize
        g_Printer.FontBold = True
        Call PrintText(sngStartX + 150, sngCurY, sngClientFontSize, "客户：" & lcClients.Text)
        Call PrintText(g_Printer.Width - sngStartX - 150 - g_Printer.TextWidth(Format(dtp.Value, "yyyy 年 mm 月 dd 日")), sngCurY, sngClientFontSize, Format(dtp.Value, "yyyy 年 mm 月 dd 日"))
        sngCurY = sngCurY + g_Printer.TextHeight("柳") + 120
        g_Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        g_Printer.DrawWidth = 1
        
        sngTableStartY = sngCurY
        sngCurY = sngCurY + 30
        For i = 1 To lvw.ColumnHeaders.Count
            If lvwSaleDetail.ColumnHeaders(i).Width > 60 Then
                Call PrintText(sngStartX + 60 + lvwSaleDetail.ColumnHeaders(i).Left, sngCurY, lvwSaleDetail.Font.Size, lvwSaleDetail.ColumnHeaders(i).Text)
            End If
        Next i
        
        sngCurY = sngCurY + lvwSaleDetail.ListItems(1).Height
        sngCurY = sngCurY + 15
        g_Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        
        ' 打印表中内容
        g_Printer.FontBold = False
        Do While sngCurY < sngPageHeight - lvw.ListItems(1).Height * 2 And lngPrintedLines < lngLines
            sngCurY = sngCurY + 45
            Call PrintText(sngStartX + 120, sngCurY, lvwSaleDetail.Font.Size, lvwSaleDetail.ListItems(lngPrintedLines).Text)
            For j = 1 To lvwSaleDetail.ColumnHeaders.Count - 1
                If lvwSaleDetail.ColumnHeaders(j + 1).Width > 60 Then
                    Call PrintText(sngStartX + 60 + lvwSaleDetail.ColumnHeaders(j + 1).Left, sngCurY, lvwSaleDetail.Font.Size, lvwSaleDetail.ListItems(lngPrintedLines).SubItems(j))
                End If
            Next j
            sngCurY = sngCurY + lvwSaleDetail.ListItems(lngPrintedLines).Height + 15
            g_Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
            lngPrintedLines = lngPrintedLines + 1 ' 已打印行数增加 1
        Loop
        
        sngTableEndY = sngCurY
        
        If lngLines >= lvwSaleDetail.ListItems.Count Then
            sngCurY = sngCurY + 30
            Call PrintText(sngStartX + sngTotalWidth - 300 - g_Printer.TextWidth("合计：" & FormatMoney(Val(txtTotal.Text))), sngCurY, sngClientFontSize, "合计：" & FormatMoney(Val(txtTotal.Text)))
            sngCurY = sngCurY + g_Printer.TextHeight("合") + 30
            g_Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        End If
        
        sngTotalHeight = sngCurY - 15
        
        If lngCurPage = 1 Then
            lngTotalPages = (lngLines - 1) \ lngPrintedLines + 1
        End If
        
        ' 打印页脚
        sngCurY = sngCurY + 30
        Call PrintText(sngStartX, sngCurY, 10.5, "  ※  第 " & lngCurPage & " 页  ※  共 " & lngTotalPages & " 页  ※  ")
        sngCurY = sngCurY + lvwSaleDetail.ListItems(1).Height + 30
        g_Printer.DrawWidth = 2
        g_Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        
        ' 打印分隔竖线
        g_Printer.Line (sngStartX, sngTableStartY)-(sngStartX, sngTotalHeight + lvw.ListItems(1).Height + 60)
        g_Printer.DrawWidth = 1
        For i = 2 To lvwSaleDetail.ColumnHeaders.Count
            If lvwSaleDetail.ColumnHeaders(i - 1).Width > 60 Then
                g_Printer.Line (sngStartX + lvwSaleDetail.ColumnHeaders(i).Left, sngTableStartY)-(sngStartX + lvwSaleDetail.ColumnHeaders(i).Left, sngTableEndY)
            End If
        Next i
        g_Printer.DrawWidth = 2
        g_Printer.Line (sngStartX + sngTotalWidth, sngTableStartY)-(sngStartX + sngTotalWidth, sngTotalHeight + lvwSaleDetail.ListItems(1).Height + 60)
        g_Printer.DrawWidth = 1
        
        ' 开始打印
        If Not g_Preview Then
            g_Printer.EndDoc
        End If
        
        lngCurPage = lngCurPage + 1
        
        ' 适当延时，避免因打印机内存不足而导致打印错乱
        If Not g_Preview Then
            Call Delay(gc_DelayBetweenMultiPages)
        End If
    Loop
    
PROC_EXIT:
    Screen.MousePointer = vbNormal
    Me.Enabled = True
    Exit Sub
    
PROC_ERR:
    GoTo PROC_EXIT
    
End Sub

' 更新最近使用的产品集
Private Sub RefreshRecentProducts()

    Dim i As Long
    Dim j As Long
    Dim strNewItem As String
    
    For i = 1 To lvwSaleDetail.ListItems.Count
    
        If lvwSaleDetail.ListItems(i).SubItems(5) <> "" Then
        
            strNewItem = lvwSaleDetail.ListItems(i).SubItems(5) & "," & lvwSaleDetail.ListItems(i).SubItems(1)
            
            Call m_RecentProducts.Add(strNewItem, , 1)
            
            For j = 2 To m_RecentProducts.Count
                If m_RecentProducts.Item(j) = strNewItem Then
                    Call m_RecentProducts.Remove(j)
                    Exit For
                End If
            Next j
            
        End If
        
    Next i
    
    For i = m_RecentProducts.Count To m_lngMaxRecentCount + 1 Step -1
        Call m_RecentProducts.Remove(i)
    Next i
    
End Sub

' 保存添加发货记录
Private Sub SaveAdd()
    
    If sstab.Tab <> 1 Then
        Exit Sub
    End If
    
    If CheckInput() = False Then
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    Dim rsADO As New ADODB.Recordset
    Dim lngRecordNo As Long
    Dim i As Long
    Dim itemTemp As ListItem
    
    ' 保存主记录
    With rsADO
    
        .Open "select * from 发货记录", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If blnAdding Then
        
            .AddNew
            
            Call FillNewRecordData(rsADO)
        Else
        
            .Find "GUID = '" & lvw.SelectedItem.Key & "'"
            
            If .EOF Then
                MsgBox "保存记录的过程中发生错误，原记录丢失！", vbCritical, msgPrompt
                Exit Sub
            End If
            
            Call FillUpdateRecordData(rsADO)
            
        End If
        
        !日期 = dtp.Value
        !客户编号 = lcClients.Value
        !金额 = Val(txtTotal.Text)
        
        lngRecordNo = !记录号
        
        .Update
        
        .Close
        
    End With
    
    ' 编辑方式下先删除原有明细记录
    If blnEditing Then
        g_cnADO.Execute "delete from 发货明细记录 where 记录号 = " & lngRecordNo
    End If
    
    ' 保存明细记录
    With rsADO
    
        .Open "发货明细记录", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
        
        For i = 1 To lvwSaleDetail.ListItems.Count
        
            If lvwSaleDetail.ListItems(i).SubItems(5) <> "" Then
            
                .AddNew
                
                !记录号 = lngRecordNo
                !产品编号 = lvwSaleDetail.ListItems(i).SubItems(5)
                !规格 = " "
                !单价 = Val(lvwSaleDetail.ListItems(i).SubItems(2))
                !数量 = Val(lvwSaleDetail.ListItems(i).SubItems(3))
                !金额 = Val(lvwSaleDetail.ListItems(i).SubItems(4))
                
                .Update
                
            End If
            
        Next i
        
    End With
    
    Call RefreshRecentProducts
    
    MsgBox "保存成功。", vbInformation, msgPrompt
    
    blnAdding = False
    blnEditing = False
    Call DoLock
    
    If dtp.Value < CDate(dtpFrom.Tag) Then
        dtpFrom.Tag = dtp.Value
    End If
    
    If dtp.Value > CDate(dtpTo.Tag) Then
        dtpTo.Tag = dtp.Value
    End If
    
    Call DisplayList
    
'    txtTotal.Text = ""
'    lcClients.SetFocus
    cmdAdd.SetFocus
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox "保存记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 合计金额
Private Sub SumMoney()
    
    Dim i As Long
    Dim ccyMoney As Currency
    
    For i = 1 To lvwSaleDetail.ListItems.Count
        ccyMoney = ccyMoney + Val(lvwSaleDetail.ListItems(i).SubItems(4))
    Next i
    
    txtTotal.Text = ccyMoney
    
End Sub

' 按客户统计发货记录
Private Sub TotalByClient()
    
    Dim strSQL As String
    Dim strClient As String
    Dim lngTotal As Long
    Dim lngTotalAll As Long
    Dim ccyTotal As Currency
    Dim ccyTotalAll As Currency
    Dim itemTemp As ListItem
    
    If dtpTo.Value < dtpFrom.Value Then
        MsgBox "统计的起始时间不能超过结束时间。", vbInformation, msgPrompt
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    
    lvwTotal.ListItems.Clear
    lvwTotal.ColumnHeaders(1).Text = "客户名称"
    
    strSQL = "SELECT 客户.名称, 产品.名称, Sum(发货明细记录.数量) AS 数量, Sum(发货明细记录.金额) AS 金额 " & _
        "FROM 客户 INNER JOIN (发货记录 INNER JOIN (产品 INNER JOIN 发货明细记录 ON 产品.编号 = 发货明细记录.产品编号) ON 发货记录.记录号 = 发货明细记录.记录号) ON 客户.编号 = 发货记录.客户编号 " & _
        "where 发货记录.日期 between #" & Format(dtpFrom.Value, gc_DateFormat) & "# and #" & Format(dtpTo.Value, gc_DateFormat) & " 23:59:59#" & _
        "GROUP BY 客户.名称, 产品.名称 " & _
        "ORDER BY 客户.名称, 产品.名称"
    
    With g_cnADO.Execute(strSQL, , adCmdText)
    
        Do Until .EOF
        
            If strClient = ![客户.名称] Then
            
                ' 继续当前客户
                Set itemTemp = lvwTotal.ListItems.Add
                
            Else
            
                ' 显示上一客户的合计金额
                If lvwTotal.ListItems.Count > 0 Then
                    Set itemTemp = lvwTotal.ListItems.Add
                    itemTemp.SubItems(1) = "  合计"
                    itemTemp.SubItems(2) = lngTotal
                    itemTemp.SubItems(3) = FormatMoney(ccyTotal)
                    Set itemTemp = lvwTotal.ListItems.Add
                End If
                
                ' 切换到一个新的日期
                strClient = ![客户.名称]
                Set itemTemp = lvwTotal.ListItems.Add(, , strClient)
                lngTotal = 0
                ccyTotal = 0
                
            End If
            
            itemTemp.SubItems(1) = ![产品.名称] & ""
            itemTemp.SubItems(2) = !数量 & ""
            itemTemp.SubItems(3) = FormatMoney(!金额)
            lngTotal = lngTotal + !数量
            ccyTotal = ccyTotal + !金额
            lngTotalAll = lngTotalAll + !数量
            ccyTotalAll = ccyTotalAll + !金额
            
            .MoveNext
            
        Loop
        
    End With
    
    ' 显示最后一个客户的合计
    Set itemTemp = lvwTotal.ListItems.Add
    itemTemp.SubItems(1) = "  合计"
    itemTemp.SubItems(2) = lngTotal
    itemTemp.SubItems(3) = FormatMoney(ccyTotal)
    Set itemTemp = lvwTotal.ListItems.Add
    
    ' 显示总计
    Set itemTemp = lvwTotal.ListItems.Add
    itemTemp.SubItems(1) = Format(dtpFrom.Value, gc_DateFormat) & " 至 " & Format(dtpTo.Value, gc_DateFormat) & " 总计"
    itemTemp.SubItems(2) = lngTotalAll
    itemTemp.SubItems(3) = FormatMoney(ccyTotalAll)
    
    sstab.Tab = 3
    
PROC_EXIT:
    Screen.MousePointer = vbNormal
    Exit Sub
    
PROC_ERR:
    MsgBox "统计数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgPrompt
    GoTo PROC_EXIT
    
End Sub

' 按日期统计发货记录
Private Sub TotalByDate()
    
    Dim strSQL As String
    Dim dateTemp As Date
    Dim lngTotal As Long
    Dim lngTotalAll As Long
    Dim ccyTotal As Currency
    Dim ccyTotalAll As Currency
    Dim itemTemp As ListItem
    
    If dtpTo.Value < dtpFrom.Value Then
        MsgBox "统计的起始时间不能超过结束时间。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    If DateDiff("m", dtpFrom.Value, dtpTo.Value) > 1 Then
        MsgBox "按日期统计发货记录的时间长度不能超过1个月，请缩短要统计的时间区间。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    lvwTotal.ListItems.Clear
    lvwTotal.ColumnHeaders(1).Text = "日期"
    
    strSQL = "SELECT Format(发货记录.日期, """ & gc_DateFormat & """) AS 日期, 产品.名称, Sum(发货明细记录.数量) AS 数量, Sum(发货明细记录.金额) AS 金额 " & _
        "FROM 产品 INNER JOIN (发货记录 INNER JOIN 发货明细记录 ON 发货记录.记录号 = 发货明细记录.记录号) ON 产品.编号 = 发货明细记录.产品编号 " & _
        "where 发货记录.日期 between #" & Format(dtpFrom.Value, gc_DateFormat) & "# and #" & Format(dtpTo.Value, gc_DateFormat) & " 23:59:59#" & _
        "GROUP BY Format(发货记录.日期, """ & gc_DateFormat & """), 产品.名称 " & _
        "ORDER BY Format(发货记录.日期, """ & gc_DateFormat & """), 产品.名称"
    
    With g_cnADO.Execute(strSQL, , adCmdText)
    
        Do Until .EOF
        
            If dateTemp = !日期 Then
            
                ' 继续当前日期
                Set itemTemp = lvwTotal.ListItems.Add
                
            Else
            
                ' 显示上一日期的合计金额
                If lvwTotal.ListItems.Count > 0 Then
                    Set itemTemp = lvwTotal.ListItems.Add
                    itemTemp.SubItems(1) = "  当日合计"
                    itemTemp.SubItems(2) = lngTotal
                    itemTemp.SubItems(3) = FormatMoney(ccyTotal)
                    Set itemTemp = lvwTotal.ListItems.Add
                End If
                
                ' 切换到一个新的日期
                dateTemp = !日期
                Set itemTemp = lvwTotal.ListItems.Add(, , Format(dateTemp, gc_DateFormat))
                lngTotal = 0
                ccyTotal = 0
                
            End If
            
            itemTemp.SubItems(1) = !名称 & ""
            itemTemp.SubItems(2) = !数量 & ""
            itemTemp.SubItems(3) = FormatMoney(!金额)
            lngTotal = lngTotal + !数量
            ccyTotal = ccyTotal + !金额
            lngTotalAll = lngTotalAll + !数量
            ccyTotalAll = ccyTotalAll + !金额
            
            .MoveNext
            
        Loop
        
    End With
    
    ' 显示最后一个日期的合计
    Set itemTemp = lvwTotal.ListItems.Add
    itemTemp.SubItems(1) = "  当日合计"
    itemTemp.SubItems(2) = lngTotal
    itemTemp.SubItems(3) = FormatMoney(ccyTotal)
    Set itemTemp = lvwTotal.ListItems.Add
    
    ' 显示总计
    Set itemTemp = lvwTotal.ListItems.Add
    itemTemp.SubItems(1) = Format(dtpFrom.Value, gc_DateFormat) & " 至 " & Format(dtpTo.Value, gc_DateFormat) & " 总计"
    itemTemp.SubItems(2) = lngTotalAll
    itemTemp.SubItems(3) = FormatMoney(ccyTotalAll)
    
    sstab.Tab = 3
    
PROC_EXIT:
    Screen.MousePointer = vbNormal
    Exit Sub
    
PROC_ERR:
    MsgBox "统计数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgPrompt
    GoTo PROC_EXIT
    
End Sub

Private Sub sstab_Click(PreviousTab As Integer)

    If PreviousTab = 1 And (blnAdding Or blnEditing) Then
        If MsgBox("是否取消编辑？", vbQuestion + vbYesNo) = vbYes Then
            Call cmdCancel_Click
        Else
            sstab.Tab = 1
        End If
    End If
    
End Sub

Private Sub txtAmount_Change()

    If ActiveControl.Name = txtAmount.Name Then
        txtMoney.Text = ToMoney(Val(txtPrice.Text) * Val(txtAmount.Text))
        On Error Resume Next
        lvwSaleDetail.ListItems(m_lngCurrentRow).SubItems(3) = Val(txtAmount.Text)
        lvwSaleDetail.ListItems(m_lngCurrentRow).SubItems(4) = txtMoney.Text
    End If
    
End Sub

Private Sub txtAmount_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtAmount_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        Call txtMoney_KeyPress(KeyAscii)
    End If
    
End Sub

Private Sub txtAmount_LostFocus()

    If ActiveControl.Name <> lcProducts.Name And ActiveControl.Name <> txtPrice.Name And ActiveControl.Name <> txtAmount.Name Then
'        Call txtMoney_KeyPress(vbKeyReturn)
    End If
    
End Sub

Private Sub txtFilter_Change()

    If ActiveControl.Name = txtFilter.Name Then
        Call DisplayFilteredProducts
    End If
    
End Sub

Private Sub txtFilter_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyDown Then
        lstFilteredProducts.SetFocus
    End If
    
End Sub

Private Sub txtFilter_LostFocus()

    If ActiveControl.Name <> lstFilteredProducts.Name Then
        txtFilter.Visible = False
        lstFilteredProducts.Visible = False
        If lstFilteredProducts.ListIndex >= 0 Then
            lcProducts.Value = m_ProductIDs.Item(lstFilteredProducts.ListIndex + 1)
        End If
    End If

End Sub

Private Sub txtMoney_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtMoney_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        If lcProducts.Value = "" Then
            lcProducts.SetFocus
        ElseIf Val(txtPrice.Text) <= 0 Then
            txtPrice.SetFocus
        ElseIf Val(txtAmount.Text) = 0 Then
            txtAmount.SetFocus
        Else
            Call NextRow(m_lngCurrentRow)
        End If
    End If
    
End Sub

Private Sub txtPrice_Change()

    If ActiveControl.Name = txtPrice.Name Then
        txtMoney.Text = ToMoney(Val(txtPrice.Text) * Val(txtAmount.Text))
        On Error Resume Next
        lvwSaleDetail.ListItems(m_lngCurrentRow).SubItems(2) = ToMoney(txtPrice.Text)
        lvwSaleDetail.ListItems(m_lngCurrentRow).SubItems(4) = txtMoney.Text
    End If
    
End Sub

Private Sub txtPrice_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtPrice_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        txtAmount.SetFocus
        If lcProducts.Value <> "" And txtPrice.Text <> GetSet("Price " & lcProducts.Value, "") Then
            Call SaveSet("Price " & lcProducts.Value, ToMoney(txtPrice.Text))
        End If
    End If
    
End Sub

Private Sub txtPrice_LostFocus()

    Call txtAmount_LostFocus
    
End Sub

Private Sub txtTotal_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtTotal_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        txtFilter.SetFocus
    End If
    
End Sub
