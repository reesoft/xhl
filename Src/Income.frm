VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{19A6DCA1-B1C0-11D5-9712-5254AB22AAF0}#2.0#0"; "lc.ocx"
Begin VB.Form frmIncome 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "收款管理"
   ClientHeight    =   6450
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9630
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6450
   ScaleWidth      =   9630
   StartUpPosition =   1  'CenterOwner
   Begin TabDlg.SSTab sstab 
      Height          =   6315
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   11139
      _Version        =   393216
      Style           =   1
      TabHeight       =   635
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   " 浏览收款记录 "
      TabPicture(0)   =   "Income.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lbl(4)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lvw"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "txtSum1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdDelete"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdModify"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmdAdd"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   " 录入收款记录 "
      TabPicture(1)   =   "Income.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lbl(1)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lbl(8)"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "lbl(7)"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "lblOperation"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "dtp"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "txtTotal"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "lcClients"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "cmdSave"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).ControlCount=   8
      TabCaption(2)   =   " 统计收款记录 "
      TabPicture(2)   =   "Income.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "lbl(11)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "lbl(9)"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "lbl(10)"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "dtpFrom"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "dtpTo"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "cmdTotal"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "chkClients"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "lcClientsT"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).Control(8)=   "cboDate"
      Tab(2).Control(8).Enabled=   0   'False
      Tab(2).ControlCount=   9
      Begin VB.CommandButton cmdAdd 
         Caption         =   "添加(&A)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   3960
         TabIndex        =   3
         Top             =   5805
         Width           =   1635
      End
      Begin VB.ComboBox cboDate 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Income.frx":0054
         Left            =   -73620
         List            =   "Income.frx":007C
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   1380
         Width           =   1575
      End
      Begin VB.CommandButton cmdModify 
         Caption         =   "修改(&M)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   5760
         TabIndex        =   4
         Top             =   5805
         Width           =   1635
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "删除(&D)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   7560
         TabIndex        =   5
         Top             =   5805
         Width           =   1635
      End
      Begin LabelComboBox.lc lcClientsT 
         Height          =   360
         Left            =   -74700
         TabIndex        =   14
         Top             =   840
         Width           =   7695
         _ExtentX        =   13573
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "要统计的客户名称"
         ListIndex       =   -1
      End
      Begin VB.CheckBox chkClients 
         Caption         =   "全部"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   -66660
         TabIndex        =   15
         Top             =   900
         Value           =   1  'Checked
         Width           =   855
      End
      Begin VB.TextBox txtSum1 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   5820
         Width           =   2235
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "保存(&S)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -67380
         TabIndex        =   12
         Top             =   1800
         Width           =   1515
      End
      Begin LabelComboBox.lc lcClients 
         Height          =   360
         Left            =   -74700
         TabIndex        =   9
         Top             =   1260
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "客户名称"
         ListIndex       =   -1
         Spacing         =   90
      End
      Begin VB.CommandButton cmdTotal 
         Caption         =   "开始统计"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   -67800
         TabIndex        =   22
         Top             =   1380
         Width           =   1815
      End
      Begin VB.TextBox txtTotal 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -67380
         TabIndex        =   11
         Top             =   1260
         Width           =   1515
      End
      Begin MSComctlLib.ListView lvw 
         Height          =   5175
         Left            =   180
         TabIndex        =   1
         Top             =   480
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   9128
         SortKey         =   1
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "序号"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "日期"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "客户名称"
            Object.Width           =   4762
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "金额"
            Object.Width           =   3704
         EndProperty
      End
      Begin MSComCtl2.DTPicker dtp 
         Height          =   375
         Left            =   -70200
         TabIndex        =   10
         Top             =   1260
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   54067201
         CurrentDate     =   37302
      End
      Begin MSComCtl2.DTPicker dtpTo 
         Height          =   375
         Left            =   -69600
         TabIndex        =   21
         Top             =   1395
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   661
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   54067201
         CurrentDate     =   37302.9999884259
      End
      Begin MSComCtl2.DTPicker dtpFrom 
         Height          =   375
         Left            =   -71640
         TabIndex        =   19
         Top             =   1395
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   661
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   54067201
         CurrentDate     =   37302
      End
      Begin VB.Label lblOperation 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "当前操作：添加记录"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   -69780
         TabIndex        =   23
         Top             =   1860
         Width           =   2160
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "到"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   10
         Left            =   -69900
         TabIndex        =   20
         Top             =   1440
         Width           =   240
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "从"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   9
         Left            =   -71940
         TabIndex        =   18
         Top             =   1440
         Width           =   240
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "时间区间"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   11
         Left            =   -74700
         TabIndex        =   16
         Top             =   1440
         Width           =   960
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "收款合计"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   360
         TabIndex        =   13
         Top             =   5880
         Width           =   960
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "付款金额"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   7
         Left            =   -68460
         TabIndex        =   8
         Top             =   1320
         Width           =   960
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "收  款  单"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   18
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         Left            =   -71340
         TabIndex        =   7
         Top             =   660
         Width           =   1920
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "日期"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   -70800
         TabIndex        =   6
         Top             =   1320
         Width           =   480
      End
   End
End
Attribute VB_Name = "frmIncome"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim blnEditFlag As Boolean

Public Sub Add()

    Call cmdAdd_Click
    
End Sub

Private Sub cboDate_Click()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        dtpFrom.Enabled = True
        dtpTo.Enabled = True
    Else
        dtpFrom.Enabled = False
        dtpTo.Enabled = False
    End If
    
    SetDateRange cboDate.List(cboDate.ListIndex), dtpFrom, dtpTo
    
End Sub

Private Sub chkClients_Click()

    If chkClients.Value = vbChecked Then
        If lcClientsT.Value <> "*" Then lcClientsT.Value = "*"
        DoEvents
        chkClients.Value = vbChecked
    End If
    
End Sub

Private Sub cmdAdd_Click()

    On Error Resume Next
    
    sstab.Tab = 1
    lcClients.Value = ""
    lcClients.SetFocus
    txtTotal.Text = ""
    
End Sub

Private Sub cmdDelete_Click()

    Call DeleteRecord
    
End Sub

Private Sub cmdModify_Click()

    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    sstab.Tab = 1
    lcClients.SetFocus
    txtTotal.Tag = txtTotal.Text
    blnEditFlag = True
    lblOperation.Caption = "当前操作：修改记录"
    
End Sub

Private Sub cmdSave_Click()

    If blnEditFlag Then
        Call SaveEdit
    Else
        Call SaveAdd
    End If
    
End Sub

Private Sub cmdTotal_Click()

    Call DoQuery
    
End Sub

Private Sub DeleteRecord()

    If sstab.Tab <> 0 Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    If MsgBox("确实要删除这条记录吗？", vbInformation + vbYesNo, msgPrompt) = vbNo Then
        Exit Sub
    End If
    
    g_cnADO.Execute "delete from 收款记录 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText
    
    Call RemoveSelectedRow(lvw)
    
    lvw.SetFocus
    
    If TypeName(lvw.SelectedItem) = "IListItem" Then
        Call lvw_ItemClick(lvw.SelectedItem)
    End If
    
End Sub

Public Sub DisplayList()

    Dim strSQL As String
    Dim strCondition As String
    
    If lcClientsT.Value <> "*" Then
        strCondition = "名称 = '" & lcClientsT.Text & "'"
    End If
    
    If Trim(strCondition) <> "" Then
        strCondition = strCondition & " and "
    End If
    
    strCondition = strCondition & "日期 between #" & Format(dtpFrom.Value, gc_DateFormat) & "# and #" & Format(dtpTo.Value, gc_DateFormat) & " 23:59:59#"
    
    If Trim(strCondition) = "" Then
        strSQL = "select * from 收款记录列表"
    Else
        strSQL = "select * from 收款记录列表 where " & strCondition
    End If
    
    Screen.MousePointer = vbHourglass
    lvw.Visible = False
    DoEvents
    
    lvw.ListItems.Clear
    Call DisplayTotal(0)
    
    Dim itemTemp As ListItem
    Dim ccyTotal As Currency
    Dim i As Long
    
    i = 1
    
    On Error GoTo PROC_ERR
    
    With g_cnADO.Execute(strSQL, , adCmdText)
    
        Do Until .EOF
        
            Set itemTemp = lvw.ListItems.Add(, !GUID, i)
            itemTemp.SubItems(1) = Format(!日期, gc_DateFormat)
            itemTemp.SubItems(2) = !名称 & ""
            itemTemp.SubItems(3) = FormatMoney(!金额)
            ccyTotal = ccyTotal + !金额
            
            .MoveNext
            i = i + 1
            
        Loop
        
    End With
    
    Call DisplayTotal(ccyTotal)
    
PROC_EXIT:
    lvw.Visible = True
    Screen.MousePointer = vbNormal
    DoEvents
    Exit Sub
    
PROC_ERR:
    MsgBox "显示记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

Public Sub DisplayTotal(ccyTotal As Currency)

    txtSum1.Tag = ccyTotal
    txtSum1.Text = FormatMoney(ccyTotal)
    
End Sub

Private Sub DoQuery()

    Call DisplayList
    
    sstab.Tab = 0
    
End Sub

Private Sub dtp_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
        txtTotal.SetFocus
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyEscape Then
        Unload Me
    End If

End Sub

Private Sub Form_Load()

    Call InitData
    'Call DisplayList
    cboDate.ListIndex = 9
    Call cmdTotal_Click
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    blnEditFlag = False
    
End Sub

Private Sub InitData()

    Set lcClients.DataSource = g_cnADO.Execute("select 编号, 名称 from 客户 order by 地区, 名称", , adCmdText)
    Set lcClientsT.DataSource = g_cnADO.Execute("select 编号, 名称 from 客户 order by 地区, 名称", , adCmdText)
    lcClientsT.AddItem "全部", "*"
    lcClientsT.Value = "*"
    
    dtp.Value = Date
    
    InitDateRange g_cnADO, "收款记录", dtpFrom, dtpTo
    
End Sub

Private Sub lcClients_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        dtp.SetFocus
    End If
    
End Sub

Private Sub SaveAdd()

    If sstab.Tab <> 1 Then
        Exit Sub
    End If
    
    If lcClients.Value = "" Then
        MsgBox "必须输入客户名称。", vbInformation, msgPrompt
        lcClients.SetFocus
        Exit Sub
    End If
    
    If Val(txtTotal.Text) = 0 Then
        MsgBox "必须输入金额。", vbInformation, msgPrompt
        txtTotal.SetFocus
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    Dim strGUID As String
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "收款记录", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
        
        .AddNew
        
        !日期 = dtp.Value
        !客户编号 = lcClients.Value
        !金额 = Val(txtTotal.Text)
        
        Call FillNewRecordData(rsADO)
        
        strGUID = !GUID
        
        .Update
        
    End With
    
    MsgBox "输入成功。", vbInformation, msgPrompt
    
    If dtpFrom.Value <= dtp.Value And dtp.Value <= dtpTo.Value Then
    
        Dim itemTemp As ListItem
        
        Set itemTemp = lvw.ListItems.Add(, strGUID, lvw.ListItems.Count + 1)
        itemTemp.SubItems(1) = Format(dtp.Value, gc_DateFormat)
        itemTemp.SubItems(2) = lcClients.Text
        itemTemp.SubItems(3) = FormatMoney(Val(txtTotal.Text))
        
        Dim ccyTotal As Currency
        
        ccyTotal = Val(txtSum1.Tag) + Val(txtTotal.Text)
        Call DisplayTotal(ccyTotal)
        
    End If
    
    If dtp.Value < CDate(dtpFrom.Tag) Then
        dtpFrom.Tag = dtp.Value
    End If
    
    If dtp.Value > CDate(dtpTo.Tag) Then
        dtpTo.Tag = dtp.Value
    End If
    
    txtTotal.Text = ""
    lcClients.SetFocus
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox "添加记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

Private Sub SaveEdit()

    If sstab.Tab <> 1 Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    If lcClients.Value = "" Then
        MsgBox "必须输入客户名称。", vbInformation, msgPrompt
        lcClients.SetFocus
        Exit Sub
    End If
    
    If Val(txtTotal.Text) = 0 Then
        MsgBox "必须输入金额。", vbInformation, msgPrompt
        txtTotal.SetFocus
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 收款记录 where GUID = '" & lvw.SelectedItem.Key & "'", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If .BOF And .EOF Then
            MsgBox "发生错误，原记录丢失，无法修改。", vbExclamation, msgPrompt
            GoTo PROC_EXIT
        End If
        
        !日期 = dtp.Value
        !客户编号 = lcClients.Value
        !金额 = Val(txtTotal.Text)
        
        Call FillUpdateRecordData(rsADO)
        
        .Update
        
    End With
    
    MsgBox "修改成功。", vbInformation, msgPrompt
    
    lvw.SelectedItem.SubItems(1) = Format(dtp.Value, gc_DateFormat)
    lvw.SelectedItem.SubItems(2) = lcClients.Text
    lvw.SelectedItem.SubItems(3) = FormatMoney(Val(txtTotal.Text))
    
    txtSum1.Tag = Val(txtSum1.Tag) - Val(txtTotal.Tag) + Val(txtTotal.Text)
    txtSum1.Text = FormatMoney(Val(txtSum1.Tag))
    
    blnEditFlag = False
    
    lblOperation.Caption = "当前操作：添加记录"
    
    sstab.Tab = 0
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Set rsADO = Nothing
    Exit Sub
    
PROC_ERR:
    MsgBox "修改记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

Private Sub lcClientsT_Click()

    DoEvents
    
    If lcClientsT.Value <> "*" Then
        chkClients.Value = vbUnchecked
    End If
    
End Sub

Private Sub lvw_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    lvw.SortKey = ColumnHeader.Index - 1
    
End Sub

Private Sub lvw_ItemClick(ByVal Item As MSComctlLib.ListItem)

    With g_cnADO.Execute("select * from 收款记录 where GUID = '" & Item.Key & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            lcClients.Value = !客户编号
            txtTotal.Text = !金额
            lblOperation.Caption = "当前操作：添加记录"
        End If
    End With
    
End Sub

Private Sub sstab_Click(PreviousTab As Integer)

    blnEditFlag = False
    
End Sub

Private Sub txtTotal_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtTotal_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        cmdSave.SetFocus
    End If
    
End Sub
