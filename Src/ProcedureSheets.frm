VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{19A6DCA1-B1C0-11D5-9712-5254AB22AAF0}#2.0#0"; "lc.ocx"
Begin VB.Form frmProcedureSheets 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "生产单管理"
   ClientHeight    =   7125
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11685
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7125
   ScaleWidth      =   11685
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdClone 
      Caption         =   "复制(&L)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   112
      Top             =   3960
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "取消(&C)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   91
      Top             =   6360
      Width           =   1335
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "保存(&S)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   90
      Top             =   5880
      Width           =   1335
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "删除(&D)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   89
      Top             =   5400
      Width           =   1335
   End
   Begin VB.CommandButton cmdModify 
      Caption         =   "修改(&M)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   88
      Top             =   4920
      Width           =   1335
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "添加(&A)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   87
      Top             =   4440
      Width           =   1335
   End
   Begin TabDlg.SSTab sstab 
      Height          =   6975
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   12303
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   635
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   " 列表 "
      TabPicture(0)   =   "ProcedureSheets.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lbl(9)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lbl(10)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lbl(11)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "dtpTo"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "dtpFrom"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lvw"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cboDate"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabCaption(1)   =   " 详细信息 "
      TabPicture(1)   =   "ProcedureSheets.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtFilter"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lstFilteredProducts"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "txtAmount3"
      Tab(1).Control(3)=   "txtAmount2"
      Tab(1).Control(4)=   "txtAmount1"
      Tab(1).Control(5)=   "txtMemo(12)"
      Tab(1).Control(6)=   "txtMemo(11)"
      Tab(1).Control(7)=   "txtMemo(10)"
      Tab(1).Control(8)=   "txtMemo(9)"
      Tab(1).Control(9)=   "txtMemo(8)"
      Tab(1).Control(10)=   "txtMemo(7)"
      Tab(1).Control(11)=   "txtMemo(6)"
      Tab(1).Control(12)=   "txtMemo(5)"
      Tab(1).Control(13)=   "txtMemo(4)"
      Tab(1).Control(14)=   "txtMemo(3)"
      Tab(1).Control(15)=   "txtMemo(2)"
      Tab(1).Control(16)=   "txtMemo(1)"
      Tab(1).Control(17)=   "txtMemo(0)"
      Tab(1).Control(18)=   "txtMoney(12)"
      Tab(1).Control(19)=   "txtMoney(11)"
      Tab(1).Control(20)=   "txtMoney(10)"
      Tab(1).Control(21)=   "txtMoney(9)"
      Tab(1).Control(22)=   "txtMoney(8)"
      Tab(1).Control(23)=   "txtMoney(7)"
      Tab(1).Control(24)=   "txtMoney(6)"
      Tab(1).Control(25)=   "txtMoney(5)"
      Tab(1).Control(26)=   "txtMoney(4)"
      Tab(1).Control(27)=   "txtMoney(3)"
      Tab(1).Control(28)=   "txtMoney(2)"
      Tab(1).Control(29)=   "txtMoney(1)"
      Tab(1).Control(30)=   "txtMoney(0)"
      Tab(1).Control(31)=   "lcOperator(0)"
      Tab(1).Control(32)=   "lcOperator(1)"
      Tab(1).Control(33)=   "lcOperator(2)"
      Tab(1).Control(34)=   "lcOperator(3)"
      Tab(1).Control(35)=   "lcOperator(4)"
      Tab(1).Control(36)=   "lcOperator(5)"
      Tab(1).Control(37)=   "lcOperator(6)"
      Tab(1).Control(38)=   "lcOperator(7)"
      Tab(1).Control(39)=   "lcOperator(8)"
      Tab(1).Control(40)=   "lcOperator(9)"
      Tab(1).Control(41)=   "lcOperator(10)"
      Tab(1).Control(42)=   "lcOperator(11)"
      Tab(1).Control(43)=   "lcOperator(12)"
      Tab(1).Control(44)=   "txtAmount(12)"
      Tab(1).Control(45)=   "txtPrice(12)"
      Tab(1).Control(46)=   "txtProcedure(12)"
      Tab(1).Control(47)=   "txtAmount(11)"
      Tab(1).Control(48)=   "txtPrice(11)"
      Tab(1).Control(49)=   "txtProcedure(11)"
      Tab(1).Control(50)=   "txtAmount(10)"
      Tab(1).Control(51)=   "txtPrice(10)"
      Tab(1).Control(52)=   "txtProcedure(10)"
      Tab(1).Control(53)=   "txtAmount(9)"
      Tab(1).Control(54)=   "txtPrice(9)"
      Tab(1).Control(55)=   "txtProcedure(9)"
      Tab(1).Control(56)=   "txtAmount(8)"
      Tab(1).Control(57)=   "txtPrice(8)"
      Tab(1).Control(58)=   "txtProcedure(8)"
      Tab(1).Control(59)=   "txtAmount(7)"
      Tab(1).Control(60)=   "txtPrice(7)"
      Tab(1).Control(61)=   "txtProcedure(7)"
      Tab(1).Control(62)=   "txtAmount(6)"
      Tab(1).Control(63)=   "txtPrice(6)"
      Tab(1).Control(64)=   "txtProcedure(6)"
      Tab(1).Control(65)=   "txtAmount(5)"
      Tab(1).Control(66)=   "txtPrice(5)"
      Tab(1).Control(67)=   "txtProcedure(5)"
      Tab(1).Control(68)=   "txtAmount(4)"
      Tab(1).Control(69)=   "txtPrice(4)"
      Tab(1).Control(70)=   "txtProcedure(4)"
      Tab(1).Control(71)=   "txtAmount(3)"
      Tab(1).Control(72)=   "txtPrice(3)"
      Tab(1).Control(73)=   "txtProcedure(3)"
      Tab(1).Control(74)=   "txtAmount(2)"
      Tab(1).Control(75)=   "txtPrice(2)"
      Tab(1).Control(76)=   "txtProcedure(2)"
      Tab(1).Control(77)=   "txtAmount(1)"
      Tab(1).Control(78)=   "txtPrice(1)"
      Tab(1).Control(79)=   "txtProcedure(1)"
      Tab(1).Control(80)=   "txtAmount(0)"
      Tab(1).Control(81)=   "txtAmount0"
      Tab(1).Control(82)=   "txtID"
      Tab(1).Control(83)=   "txtPrice(0)"
      Tab(1).Control(84)=   "txtProcedure(0)"
      Tab(1).Control(85)=   "lcProducts"
      Tab(1).Control(86)=   "dtp"
      Tab(1).Control(87)=   "lbl(14)"
      Tab(1).Control(88)=   "lbl(13)"
      Tab(1).Control(89)=   "lbl(12)"
      Tab(1).Control(90)=   "lbl(8)"
      Tab(1).Control(91)=   "lbl(7)"
      Tab(1).Control(92)=   "lbl(6)"
      Tab(1).Control(93)=   "lbl(5)"
      Tab(1).Control(94)=   "lbl(3)"
      Tab(1).Control(95)=   "lbl(0)"
      Tab(1).Control(96)=   "lbl(1)"
      Tab(1).Control(97)=   "lbl(2)"
      Tab(1).Control(98)=   "lbl(4)"
      Tab(1).ControlCount=   99
      Begin VB.TextBox txtFilter 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -68940
         TabIndex        =   111
         TabStop         =   0   'False
         Top             =   540
         Visible         =   0   'False
         Width           =   3255
      End
      Begin VB.ListBox lstFilteredProducts 
         Height          =   5940
         Left            =   -68940
         TabIndex        =   110
         TabStop         =   0   'False
         Top             =   900
         Visible         =   0   'False
         Width           =   3255
      End
      Begin VB.TextBox txtAmount3 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -64860
         TabIndex        =   8
         Top             =   3000
         Width           =   1035
      End
      Begin VB.TextBox txtAmount2 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -64860
         TabIndex        =   7
         Top             =   2220
         Width           =   1035
      End
      Begin VB.TextBox txtAmount1 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -64860
         TabIndex        =   6
         Top             =   1440
         Width           =   1035
      End
      Begin VB.ComboBox cboDate 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "ProcedureSheets.frx":0038
         Left            =   9720
         List            =   "ProcedureSheets.frx":0060
         Style           =   2  'Dropdown List
         TabIndex        =   105
         Top             =   540
         Width           =   1635
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   12
         Left            =   -67860
         TabIndex        =   86
         Top             =   6480
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   11
         Left            =   -67860
         TabIndex        =   80
         Top             =   6060
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   10
         Left            =   -67860
         TabIndex        =   74
         Top             =   5640
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   9
         Left            =   -67860
         TabIndex        =   68
         Top             =   5220
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         Left            =   -67860
         TabIndex        =   62
         Top             =   4800
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   7
         Left            =   -67860
         TabIndex        =   56
         Top             =   4380
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         Left            =   -67860
         TabIndex        =   50
         Top             =   3960
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         Left            =   -67860
         TabIndex        =   44
         Top             =   3540
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   -67860
         TabIndex        =   38
         Top             =   3120
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   -67860
         TabIndex        =   32
         Top             =   2700
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   -67860
         TabIndex        =   26
         Top             =   2280
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   -67860
         TabIndex        =   20
         Top             =   1860
         Width           =   2775
      End
      Begin VB.TextBox txtMemo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   -67860
         TabIndex        =   14
         Top             =   1440
         Width           =   2775
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   12
         Left            =   -68880
         TabIndex        =   85
         Top             =   6480
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   11
         Left            =   -68880
         TabIndex        =   79
         Top             =   6060
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   10
         Left            =   -68880
         TabIndex        =   73
         Top             =   5640
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   9
         Left            =   -68880
         TabIndex        =   67
         Top             =   5220
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         Left            =   -68880
         TabIndex        =   61
         Top             =   4800
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   7
         Left            =   -68880
         TabIndex        =   55
         Top             =   4380
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         Left            =   -68880
         TabIndex        =   49
         Top             =   3960
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         Left            =   -68880
         TabIndex        =   43
         Top             =   3540
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   -68880
         TabIndex        =   37
         Top             =   3120
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   -68880
         TabIndex        =   31
         Top             =   2700
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   -68880
         TabIndex        =   25
         Top             =   2280
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   -68880
         TabIndex        =   19
         Top             =   1860
         Width           =   1035
      End
      Begin VB.TextBox txtMoney 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   -68880
         TabIndex        =   13
         Top             =   1440
         Width           =   1035
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   0
         Left            =   -70440
         TabIndex        =   12
         Top             =   1440
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   1
         Left            =   -70440
         TabIndex        =   18
         Top             =   1860
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   2
         Left            =   -70440
         TabIndex        =   24
         Top             =   2280
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   3
         Left            =   -70440
         TabIndex        =   30
         Top             =   2700
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   4
         Left            =   -70440
         TabIndex        =   36
         Top             =   3120
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   5
         Left            =   -70440
         TabIndex        =   42
         Top             =   3540
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   6
         Left            =   -70440
         TabIndex        =   48
         Top             =   3960
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   7
         Left            =   -70440
         TabIndex        =   54
         Top             =   4380
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   8
         Left            =   -70440
         TabIndex        =   60
         Top             =   4800
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   9
         Left            =   -70440
         TabIndex        =   66
         Top             =   5220
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   10
         Left            =   -70440
         TabIndex        =   72
         Top             =   5640
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   11
         Left            =   -70440
         TabIndex        =   78
         Top             =   6060
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Index           =   12
         Left            =   -70440
         TabIndex        =   84
         Top             =   6480
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Spacing         =   0
         Enabled         =   0   'False
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   12
         Left            =   -71460
         TabIndex        =   83
         Top             =   6480
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   12
         Left            =   -72480
         TabIndex        =   82
         Top             =   6480
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   12
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   81
         Top             =   6480
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   11
         Left            =   -71460
         TabIndex        =   77
         Top             =   6060
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   11
         Left            =   -72480
         TabIndex        =   76
         Top             =   6060
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   11
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   75
         Top             =   6060
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   10
         Left            =   -71460
         TabIndex        =   71
         Top             =   5640
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   10
         Left            =   -72480
         TabIndex        =   70
         Top             =   5640
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   10
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   69
         Top             =   5640
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   9
         Left            =   -71460
         TabIndex        =   65
         Top             =   5220
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   9
         Left            =   -72480
         TabIndex        =   64
         Top             =   5220
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   9
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   63
         Top             =   5220
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         Left            =   -71460
         TabIndex        =   59
         Top             =   4800
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         Left            =   -72480
         TabIndex        =   58
         Top             =   4800
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   8
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   57
         Top             =   4800
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   7
         Left            =   -71460
         TabIndex        =   53
         Top             =   4380
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   7
         Left            =   -72480
         TabIndex        =   52
         Top             =   4380
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   7
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   51
         Top             =   4380
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         Left            =   -71460
         TabIndex        =   47
         Top             =   3960
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         Left            =   -72480
         TabIndex        =   46
         Top             =   3960
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   6
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   45
         Top             =   3960
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         Left            =   -71460
         TabIndex        =   41
         Top             =   3540
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         Left            =   -72480
         TabIndex        =   40
         Top             =   3540
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   5
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   39
         Top             =   3540
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   -71460
         TabIndex        =   35
         Top             =   3120
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   -72480
         TabIndex        =   34
         Top             =   3120
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   4
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   33
         Top             =   3120
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   -71460
         TabIndex        =   29
         Top             =   2700
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   -72480
         TabIndex        =   28
         Top             =   2700
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   3
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   27
         Top             =   2700
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   -71460
         TabIndex        =   23
         Top             =   2280
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   -72480
         TabIndex        =   22
         Top             =   2280
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   2280
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   -71460
         TabIndex        =   17
         Top             =   1860
         Width           =   1035
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   -72480
         TabIndex        =   16
         Top             =   1860
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   1860
         Width           =   2415
      End
      Begin VB.TextBox txtAmount 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   -71460
         TabIndex        =   11
         Top             =   1440
         Width           =   1035
      End
      Begin VB.TextBox txtAmount0 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -64860
         TabIndex        =   5
         Top             =   540
         Width           =   1035
      End
      Begin VB.TextBox txtID 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -74220
         TabIndex        =   2
         Top             =   540
         Width           =   1515
      End
      Begin VB.TextBox txtPrice 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   -72480
         TabIndex        =   10
         Top             =   1440
         Width           =   1035
      End
      Begin VB.TextBox txtProcedure 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   -74880
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   1440
         Width           =   2415
      End
      Begin LabelComboBox.lc lcProducts 
         Height          =   360
         Left            =   -70020
         TabIndex        =   4
         ToolTipText     =   "按空格键输入过滤条件"
         Top             =   540
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "产品名称"
         Spacing         =   90
      End
      Begin MSComCtl2.DTPicker dtp 
         Height          =   375
         Left            =   -71880
         TabIndex        =   3
         Top             =   540
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "yyyy-MM-dd"
         Format          =   54067203
         CurrentDate     =   37533
      End
      Begin MSComctlLib.ListView lvw 
         Height          =   6315
         Left            =   120
         TabIndex        =   0
         Top             =   480
         Width           =   8355
         _ExtentX        =   14737
         _ExtentY        =   11139
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "日期"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "工序单号"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "产品"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "数量"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComCtl2.DTPicker dtpFrom 
         Height          =   375
         Left            =   9720
         TabIndex        =   101
         Top             =   1020
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   661
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   54067201
         CurrentDate     =   37302
      End
      Begin MSComCtl2.DTPicker dtpTo 
         Height          =   375
         Left            =   9720
         TabIndex        =   102
         Top             =   1500
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   661
         _Version        =   393216
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   54067201
         CurrentDate     =   37302.9999884259
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "直销数"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   14
         Left            =   -64860
         TabIndex        =   109
         Top             =   2700
         Width           =   720
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "入库数"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   13
         Left            =   -64860
         TabIndex        =   108
         Top             =   1920
         Width           =   720
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "合格数"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   12
         Left            =   -64860
         TabIndex        =   107
         Top             =   1140
         Width           =   720
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "时间区间"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   11
         Left            =   8640
         TabIndex        =   106
         Top             =   600
         Width           =   960
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "到"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   10
         Left            =   9360
         TabIndex        =   104
         Top             =   1560
         Width           =   240
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "从"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   9
         Left            =   9360
         TabIndex        =   103
         Top             =   1080
         Width           =   240
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "日期"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   8
         Left            =   -72480
         TabIndex        =   100
         Top             =   600
         Width           =   480
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "备  注"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   7
         Left            =   -66780
         TabIndex        =   99
         Top             =   1080
         Width           =   720
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "工  价"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   6
         Left            =   -68700
         TabIndex        =   98
         Top             =   1080
         Width           =   720
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "操作者"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   5
         Left            =   -70020
         TabIndex        =   97
         Top             =   1080
         Width           =   720
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "数  量"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   3
         Left            =   -71280
         TabIndex        =   96
         Top             =   1080
         Width           =   720
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "单号"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   -74820
         TabIndex        =   95
         Top             =   600
         Width           =   480
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "数量"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   -65460
         TabIndex        =   94
         Top             =   600
         Width           =   600
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "工  序"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   2
         Left            =   -73980
         TabIndex        =   93
         Top             =   1080
         Width           =   720
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "单  价"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   4
         Left            =   -72300
         TabIndex        =   92
         Top             =   1080
         Width           =   720
      End
   End
End
Attribute VB_Name = "frmProcedureSheets"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_ProductIDs As Collection
Dim m_RecentProducts As New Collection
Dim m_lngMaxRecentCount As Long

Dim blnAdding As Boolean
Dim blnEditing As Boolean

Public Sub Add()

    Call cmdAdd_Click
    
End Sub

' 放弃保存
Private Sub CancelSave()
    
    blnAdding = False
    blnEditing = False
    
    Call DisplayRecord
    Call DoLock
    
End Sub

Private Sub cboDate_Click()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        dtpFrom.Enabled = True
        dtpTo.Enabled = True
    Else
        dtpFrom.Enabled = False
        dtpTo.Enabled = False
    End If
    
    SetDateRange cboDate.List(cboDate.ListIndex), dtpFrom, dtpTo
    
    Call DisplayList
    
End Sub

' 清除控件内容以免对后来的显示造成影响
Private Sub ClearDisplay()
    
    On Error Resume Next
    
    txtID.Text = ""
    dtp.Value = CDate("1899-12-30")
    lcProducts.Value = ""
    txtAmount0.Text = ""
    txtAmount1.Text = ""
    txtAmount2.Text = ""
    txtAmount3.Text = ""
    Call ClearDisplay0
    
End Sub

' 清除控件内容以免对后来的显示造成影响
Private Sub ClearDisplay0()
    
    Dim i As Long
    
    On Error Resume Next
    
    For i = 0 To 12
        txtProcedure(i).Text = ""
        txtPrice(i).Text = ""
        txtAmount(i).Text = ""
        lcOperator(i).Value = " "
        txtMoney(i).Text = ""
        txtMemo(i).Text = ""
    Next i
    
End Sub

Private Sub cmdAdd_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    blnAdding = True
    sstab.Tab = 1
    
    txtID.Tag = GenerateNewID(txtID.Text)
    
    Call ClearDisplay
    Call DoLock
    
    txtID.Text = txtID.Tag
    dtp.Value = Date
    
    On Error Resume Next
    
    txtID.SetFocus
    
End Sub

Private Sub cmdCancel_Click()

    Call CancelSave
    
End Sub

Private Sub cmdClone_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    blnAdding = True
    sstab.Tab = 1
    
    Call DoLock
    
    txtID.Text = GenerateNewID(txtID.Text)
    
    dtp.SetFocus
    
End Sub

Private Sub cmdDelete_Click()

    Call DeleteRecord
    
End Sub

Private Sub cmdModify_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    blnEditing = True
    sstab.Tab = 1
    
    Call DoLock
    
    dtp.SetFocus
    
End Sub

' 删除记录
Private Sub DeleteRecord()
    
    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    If MsgBox("确实要删除该记录吗？", vbInformation + vbYesNo, msgPrompt) = vbNo Then
        Exit Sub
    End If
    
    With g_cnADO.Execute("select * from 工序单主表 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            g_cnADO.Execute "delete from 工序单从表 where 主单编号 = " & !编号, , adCmdText
            g_cnADO.Execute "delete from 工序单主表 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText
        End If
    End With
    
    Call RemoveSelectedRow(lvw)
    
    Call DisplayRecord
    
End Sub

' 显示已过滤的产品列表
Private Sub DisplayFilteredProducts()

    lstFilteredProducts.Clear
    Set m_ProductIDs = New Collection
    
    txtFilter.Tag = Replace(Trim(txtFilter.Text), "'", "''")
    
    If txtFilter.Tag = "" Then
    
        Dim i As Long
        Dim lngPos As Long
        
        ' 把最近使用的产品排在前面
        For i = 1 To m_RecentProducts.Count
        
            lngPos = InStr(m_RecentProducts.Item(i), ",")
            
            lstFilteredProducts.AddItem Mid(m_RecentProducts.Item(i), lngPos + 1)
            m_ProductIDs.Add Left(m_RecentProducts.Item(i), lngPos - 1)
            
        Next i
        
    End If
    
    Dim strSQL As String
    
    strSQL = "select * from 产品 where 名称 like '%" & txtFilter.Tag & "%' and not 停产 order by 名称"
    
    With g_cnADO.Execute(strSQL, , adCmdText)
        Do Until .EOF
            lstFilteredProducts.AddItem !名称
            m_ProductIDs.Add !编号 & ""
            .MoveNext
        Loop
    End With
    
End Sub

' 显示列表
Private Sub DisplayList()
    
    Dim strSQL As String
    Dim itemTemp As ListItem
    
    Call ClearDisplay
    lvw.ListItems.Clear
    
    If cboDate.List(cboDate.ListIndex) = "全部" Then
        strSQL = "select 工序单主表.*, 产品.名称 from 工序单主表 left join 产品 on 工序单主表.产品编号 = 产品.编号 order by 日期"
    Else
        strSQL = "select 工序单主表.*, 产品.名称 from 工序单主表 left join 产品 on 工序单主表.产品编号 = 产品.编号 " & _
            "where 日期 between #" & Format(dtpFrom.Value, gc_DateFormat) & "# and #" & Format(dtpTo.Value, gc_DateFormat) & " 23:59:59# order by 日期"
    End If
    
    With g_cnADO.Execute(strSQL, , adCmdText)
        Do Until .EOF
            Set itemTemp = lvw.ListItems.Add(, !GUID, Format(!日期, gc_DateFormat))
            itemTemp.SubItems(1) = !单号 & ""
            itemTemp.SubItems(2) = !名称 & ""
            itemTemp.SubItems(3) = !数量 & ""
            .MoveNext
        Loop
    End With
    
    If lvw.ListItems.Count > 0 Then
        Set lvw.SelectedItem = lvw.ListItems.Item(1)
        Call lvw_ItemClick(lvw.SelectedItem)
    End If
    
    Call DoLock
    
End Sub

' 显示记录
Private Sub DisplayRecord()
    
    Dim lngMainRecordNo As Long
    Dim i As Long
    
    For i = 0 To 12
        txtProcedure(i).Tag = ""
    Next i
    
    Call ClearDisplay
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    With g_cnADO.Execute("select * from 工序单主表 where GUID = '" & lvw.SelectedItem.Key & "'")
        If Not (.BOF And .EOF) Then
            lngMainRecordNo = !编号
            txtID.Text = !单号 & ""
            dtp.Value = !日期
            lcProducts.Value = !产品编号 & ""
            txtAmount0.Text = !数量
            txtAmount1.Text = !合格数
            txtAmount2.Text = !入库数
            txtAmount3.Text = !直销数
        End If
    End With
    
    i = 0
    
    With g_cnADO.Execute("select * from 工序单从表 where 主单编号 = " & lngMainRecordNo & "")
        Do Until .EOF
            txtProcedure(i).Text = !工序 & " " & !工序名称
            txtProcedure(i).Tag = "1"
            txtPrice(i).Text = ToMoney(!单价)
            txtAmount(i).Text = !数量
            lcOperator(i).Value = !操作者编号 & ""
            txtMoney(i).Text = !单价 * !数量
            txtMemo(i).Text = !备注 & ""
            .MoveNext
            i = i + 1
        Loop
    End With
    
End Sub

' 锁定/解锁控件
Private Sub DoLock()
    
    Dim i As Long
    
    If blnAdding Or blnEditing Then
    
        If blnAdding Then
            txtID.Enabled = True
        End If
        
        dtp.Enabled = True
        lcProducts.Enabled = True
        txtAmount0.Enabled = True
        txtAmount1.Enabled = True
        txtAmount2.Enabled = True
        txtAmount3.Enabled = True
        
        For i = 0 To 12
            txtProcedure(i).Locked = False
            txtPrice(i).Locked = False
            txtAmount(i).Locked = False
            lcOperator(i).Locked = False
            txtMoney(i).Locked = False
            txtMemo(i).Locked = False
'            If txtProcedure(i).Tag = "" Then
'                txtProcedure(i).Enabled = False
'                txtPrice(i).Enabled = False
'                txtAmount(i).Enabled = False
'                lcOperator(i).Enabled = False
'                txtMoney(i).Enabled = False
'                txtMemo(i).Enabled = False
'            Else
                txtProcedure(i).Enabled = True
                txtPrice(i).Enabled = True
                txtAmount(i).Enabled = True
                lcOperator(i).Enabled = True
                'txtMoney(i).Enabled = True
                txtMemo(i).Enabled = True
'            End If
        Next i
        
        cmdClone.Enabled = False
        cmdAdd.Enabled = False
        cmdModify.Enabled = False
        cmdDelete.Enabled = False
        cmdSave.Enabled = True
        cmdCancel.Enabled = True
        
    Else
    
        txtID.Enabled = False
        dtp.Enabled = False
        lcProducts.Enabled = False
        txtAmount0.Enabled = False
        txtAmount1.Enabled = False
        txtAmount2.Enabled = False
        txtAmount3.Enabled = False
        
        For i = 0 To 12
            txtProcedure(i).Enabled = False
            txtPrice(i).Enabled = False
            txtAmount(i).Enabled = False
            lcOperator(i).Enabled = False
            txtMoney(i).Enabled = False
            txtMemo(i).Enabled = False
        Next i
        
        If TypeName(lvw.SelectedItem) = "Nothing" Then
            cmdClone.Enabled = False
            cmdModify.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdClone.Enabled = True
            cmdModify.Enabled = True
            cmdDelete.Enabled = True
        End If
        
        cmdAdd.Enabled = True
        cmdSave.Enabled = False
        cmdCancel.Enabled = False
        
    End If
    
End Sub

Private Sub cmdSave_Click()

    If blnAdding Then
        Call SaveAdd
    ElseIf blnEditing Then
        Call SaveEdit
    End If
    
End Sub

Private Sub dtpFrom_Change()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        Call DisplayList
    End If
    
End Sub

Private Sub dtpTo_Change()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        Call DisplayList
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
    
        If TypeName(ActiveControl) = "TextBox" _
            Or TypeName(ActiveControl) = "ComboBox" _
            Or TypeName(ActiveControl) = "CheckBox" _
            Or TypeName(ActiveControl) = "DTPicker" _
            Or TypeName(ActiveControl) = "lc" Then
            
            On Error Resume Next
            
            SendKeys "{TAB}"
            
        End If
        
    ElseIf KeyCode = vbKeyEscape Then
    
        Unload Me
        
    End If
    
End Sub

Private Sub Form_Load()

    Screen.MousePointer = vbHourglass
    DoEvents
    
    Call InitData
'    Call Displaylist

    Call DoLock
    
    cboDate.ListIndex = 5
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    If blnAdding Or blnEditing Then
        Call CancelSave
    End If
    
End Sub

Private Function GenerateNewID(strCurrentID As String)

    Dim strNewID As String
    Dim blnExists As Boolean
    Dim i As Long
    
    strNewID = strCurrentID
    
    If strNewID = "" Or lvw.ListItems.Count = 0 Then
        strNewID = "0"
    End If
    
    blnExists = True
    
    Do Until Not blnExists
    
        strNewID = IncreaseString(strNewID)
        blnExists = False
        
        For i = 1 To lvw.ListItems.Count
            If lvw.ListItems.Item(i).SubItems(1) = strNewID Then
                blnExists = True
                Exit For
            End If
        Next i
    
    Loop
    
    GenerateNewID = strNewID
    
End Function

' 加载最近使用的产品集
Private Sub GetRecentProducts()

    If m_RecentProducts.Count > 0 Then
        Exit Sub
    End If
    
    Dim i As Long
    Dim blnExists As Boolean
    Dim lngRecentCount As Long
    
    m_lngMaxRecentCount = 10
    
    With g_cnADO.Execute("select top 50 工序单主表.产品编号, 产品.名称 from 工序单主表 left join 产品 on 工序单主表.产品编号 = 产品.编号 order by 工序单主表.日期 desc", , adCmdText)
    
        Do Until .EOF
        
            blnExists = False
            
            For i = 1 To m_RecentProducts.Count
                If m_RecentProducts.Item(i) = !产品编号 & "," & !名称 Then
                    blnExists = True
                    Exit For
                End If
            Next i
            
            If Not blnExists Then
            
                m_RecentProducts.Add !产品编号 & "," & !名称
                
                lngRecentCount = lngRecentCount + 1
                
                If lngRecentCount >= m_lngMaxRecentCount Then
                    Exit Do
                End If
                
            End If
            
            .MoveNext
            
        Loop
        
    End With
    
End Sub

' 初始化必要数据
Private Sub InitData()

    Set lcProducts.DataSource = g_cnADO.Execute("select 编号, 名称 from 产品 where not 停产 order by 名称", , adCmdText)
    
    Dim i As Long
    
    For i = 0 To 12
        Set lcOperator(i).DataSource = g_cnADO.Execute("select 工号, 姓名 from 职工 where 是否在职 order by 姓名", , adCmdText)
        lcOperator(i).AddItem "    ", " "
    Next i
    
    InitDateRange g_cnADO, "工序单主表", dtpFrom, dtpTo
    
    Call GetRecentProducts
    
End Sub

' 更新最近使用的产品集
Private Sub RefreshRecentProducts()

    Dim i As Long
    Dim strNewItem As String
    
    If lcProducts.Value <> "" Then
    
        strNewItem = lcProducts.Value & "," & lcProducts.Text
        
        Call m_RecentProducts.Add(strNewItem, , 1)
        
        For i = 2 To m_RecentProducts.Count
            If m_RecentProducts.Item(i) = strNewItem Then
                Call m_RecentProducts.Remove(i)
                Exit For
            End If
        Next i
        
    End If
    
    For i = m_RecentProducts.Count To m_lngMaxRecentCount + 1 Step -1
        Call m_RecentProducts.Remove(i)
    Next i
    
End Sub

' 保存添加的内容
Private Sub SaveAdd()
    
    Dim i As Long
    
    txtID.Text = Trim(txtID.Text)
    If txtID.Text = "" Or StrLen(txtID.Text) > 50 Then
        MsgBox "单号应该是 1 至 50 个字符，请重新输入。", vbInformation, msgPrompt
        txtID.SetFocus
        GoTo PROC_EXIT
    End If
    txtID.Tag = Replace(txtID.Text, "'", "''")
    
    If lcProducts.Value = "" Then
        MsgBox "请选择一种产品。", vbInformation, msgPrompt
        lcProducts.SetFocus
        GoTo PROC_EXIT
    End If
    
    txtAmount0.Text = Val(txtAmount0.Text)
    If Val(txtAmount0.Text) <= 0 Then
        MsgBox "请输入数量。", vbInformation, msgPrompt
        txtAmount0.SetFocus
        GoTo PROC_EXIT
    End If
    
    For i = 0 To 12
        If txtProcedure(i).Text <> "" And lcOperator(i).Value <> "" Then
            If Val(txtPrice(i).Text) <= 0 Then
                MsgBox "请输入单价。", vbInformation, msgPrompt
                txtPrice(i).SetFocus
                GoTo PROC_EXIT
            End If
            If Val(txtAmount(i).Text) <= 0 Then
                MsgBox "请输入数量。", vbInformation, msgPrompt
                txtAmount(i).SetFocus
                GoTo PROC_EXIT
            End If
'            If lcOperator(i).Value = "" Then
'                MsgBox "请输入操作者。", vbInformation, msgPrompt
'                lcOperator(i).SetFocus
'                Exit Sub
'            End If
        End If
    Next i
    
    On Error GoTo PROC_ERR
    
    With g_cnADO.Execute("select * from 工序单主表 where 单号 = '" & txtID.Tag & "'")
        If Not (.BOF And .EOF) Then
            MsgBox "该单号已经存在，请检查，或者输入另一个单号。", vbInformation, msgPrompt
            txtID.SetFocus
            GoTo PROC_EXIT
        End If
    End With
    
    Dim lngRecordNo As Long
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "工序单主表", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
        
        .AddNew
        
        !单号 = txtID.Text
        !日期 = dtp.Value
        !产品编号 = lcProducts.Value
        !数量 = Val(txtAmount0.Text)
        !合格数 = Val(txtAmount1.Text)
        !入库数 = Val(txtAmount2.Text)
        !直销数 = Val(txtAmount3.Text)
        
        Call FillNewRecordData(rsADO)
        
        .Update
        
        txtID.Tag = !GUID
        lngRecordNo = !编号
        
    End With
    
    g_cnADO.Execute "delete from 工序单从表 where 主单编号 = " & lngRecordNo, , adCmdText
    
    With rsADO
    
        .Close
        
        .Open "工序单从表", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
        
        Dim lngPos As Long
        
        For i = 0 To 12
        
            If txtProcedure(i).Text = "" Then
                Exit For
            End If
            
            .AddNew
            
            !主单编号 = lngRecordNo
            !工序 = i + 1
            lngPos = InStr(txtProcedure(i).Text, " ")
            If lngPos > 1 Then
                !工序名称 = Mid(txtProcedure(i).Text, lngPos + 1)
            Else
                !工序名称 = txtProcedure(i).Text
            End If
            !单价 = Val(txtPrice(i).Text)
            !数量 = Val(txtAmount(i).Text)
            !操作者编号 = lcOperator(i).Value
            !备注 = txtMemo(i).Text
            
            .Update
            
        Next i
        
    End With
    
    blnAdding = False
    Call DoLock
    
    Call RefreshRecentProducts
    
    Dim itemTemp As ListItem
    
    Set itemTemp = lvw.ListItems.Add(, txtID.Tag, Format(dtp.Value, gc_DateFormat))
    itemTemp.SubItems(1) = txtID.Text
    itemTemp.SubItems(2) = lcProducts.Text
    itemTemp.SubItems(3) = txtAmount0.Text
    
    Set lvw.SelectedItem = itemTemp
    Call lvw_ItemClick(itemTemp)
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    MsgBox "添加记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 保存修改的内容
Private Sub SaveEdit()
    
    txtID.Text = Trim(txtID.Text)
    If txtID.Text = "" Or StrLen(txtID.Text) > 50 Then
        MsgBox "单号应该是 1 至 50 个字符，请重新输入。", vbInformation, msgPrompt
        txtID.SetFocus
        GoTo PROC_EXIT
    End If
    txtID.Tag = Replace(txtID.Text, "'", "''")
    
    If lcProducts.Value = "" Then
        MsgBox "请选择一种产品。", vbInformation, msgPrompt
        lcProducts.SetFocus
        GoTo PROC_EXIT
    End If
    
    txtAmount0.Text = Val(txtAmount0.Text)
    If Val(txtAmount0.Text) <= 0 Then
        MsgBox "请输入数量。", vbInformation, msgPrompt
        txtAmount0.SetFocus
        GoTo PROC_EXIT
    End If
    
    Dim i As Long
    
    For i = 0 To 12
        If txtProcedure(i).Text <> "" And lcOperator(i).Value <> "" Then
            If Val(txtPrice(i).Text) <= 0 Then
                MsgBox "请输入单价。", vbInformation, msgPrompt
                txtPrice(i).SetFocus
                Exit Sub
            End If
            If Val(txtAmount(i).Text) <= 0 Then
                MsgBox "请输入数量。", vbInformation, msgPrompt
                txtAmount(i).SetFocus
                Exit Sub
            End If
'            If lcOperator(i).Value = "" Then
'                MsgBox "请输入操作者。", vbInformation, msgPrompt
'                lcOperator(i).SetFocus
'                Exit Sub
'            End If
        End If
    Next i
    
    On Error GoTo PROC_ERR
    
    Dim lngRecordNo As Long
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 工序单主表 where GUID = '" & lvw.SelectedItem.Key & "'", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If .BOF And .EOF Then
            MsgBox "记录丢失，无法修改。", vbCritical, msgErrAbnormal
            GoTo PROC_EXIT
        End If
        
        '!单号 = txtID.Text
        !日期 = dtp.Value
        !产品编号 = lcProducts.Value
        !数量 = Val(txtAmount0.Text)
        !合格数 = Val(txtAmount1.Text)
        !入库数 = Val(txtAmount2.Text)
        !直销数 = Val(txtAmount3.Text)
        
        Call FillUpdateRecordData(rsADO)
        
        .Update
        
        lngRecordNo = !编号
        
    End With
    
    g_cnADO.Execute "delete from 工序单从表 where 主单编号 = " & lngRecordNo, , adCmdText
    
    With rsADO
    
        .Close
        
        .Open "工序单从表", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
        
        Dim lngPos As Long
        
        For i = 0 To 12
        
            If txtProcedure(i).Text = "" Then
                Exit For
            End If
            
            .AddNew
            
            !主单编号 = lngRecordNo
            !工序 = i + 1
            lngPos = InStr(txtProcedure(i).Text, " ")
            If lngPos > 1 Then
                !工序名称 = Mid(txtProcedure(i).Text, lngPos + 1)
            Else
                !工序名称 = txtProcedure(i).Text
            End If
            !单价 = Val(txtPrice(i).Text)
            !数量 = Val(txtAmount(i).Text)
            !操作者编号 = lcOperator(i).Value
            !备注 = txtMemo(i).Text
            
            .Update
            
        Next i
        
    End With
    
    blnEditing = False
    Call DoLock
    
    With lvw.SelectedItem
        .Text = Format(dtp.Value, gc_DateFormat)
        .SubItems(1) = txtID.Text
        .SubItems(2) = lcProducts.Text
        .SubItems(3) = txtAmount0.Text
    End With
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    If Err.Number = 3022 Then
        rsADO.CancelUpdate
        MsgBox "编号或名称重复，无法修改。", vbCritical, msgErrRuntime
    Else
        MsgBox "修改记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    End If
    GoTo PROC_EXIT
    
End Sub

Private Sub lvw_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    lvw.SortKey = ColumnHeader.Index - 1
    
End Sub

Private Sub lvw_DblClick()

    If TypeName(lvw.SelectedItem) = "IListItem" Then sstab.Tab = 1
    
End Sub

Private Sub lvw_ItemClick(ByVal Item As MSComctlLib.ListItem)

    If blnAdding Or blnEditing Then
        Call CancelSave
    End If
    
    Call DisplayRecord
    
End Sub

Private Sub lcProducts_Click()

    Dim i As Long
    
    Call ClearDisplay0
    
    If lcProducts.Value = "" Then
        Exit Sub
    End If
    
    If blnAdding Or blnEditing Then
        With g_cnADO.Execute("select * from 工序 where 产品编号='" & lcProducts.Value & "' order by 工序编号", , adCmdText)
            If .BOF And .EOF Then
                MsgBox "未为该产品维护工序，无法显示记录。", vbExclamation, msgPrompt
            Else
                i = 0
                Do Until .EOF
                    txtProcedure(i).Text = !工序编号 & " " & !工序名称
                    txtProcedure(i).Tag = "1"
                    txtPrice(i).Text = ToMoney(!单价)
                    .MoveNext
                    i = i + 1
                Loop
            End If
        End With
        Call DoLock
    Else
        'Call DisplayRecord
    End If
    
End Sub

Private Sub lcProducts_KeyPress(KeyAscii As Integer)

    txtFilter.Visible = True
    txtFilter.SetFocus
    txtFilter.Text = Trim(Chr(KeyAscii))
    txtFilter.SelStart = Len(txtFilter.Text)
    lstFilteredProducts.Visible = True
    
End Sub

Private Sub lstFilteredProducts_DblClick()

    txtAmount0.SetFocus
    
End Sub

Private Sub lstFilteredProducts_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyUp Then
        If lstFilteredProducts.Selected(0) Then
            txtFilter.SetFocus
        End If
    ElseIf KeyCode = vbKeyReturn Then
        txtAmount0.SetFocus
    End If
    
End Sub

Private Sub lstFilteredProducts_LostFocus()

    If ActiveControl.Name <> txtFilter.Name Then
        txtFilter.Visible = False
        lstFilteredProducts.Visible = False
        lcProducts.Value = m_ProductIDs.Item(lstFilteredProducts.ListIndex + 1)
    End If
    
End Sub

Private Sub sstab_Click(PreviousTab As Integer)

    If sstab.Tab = 0 Then
        If blnAdding Or blnEditing Then
            cmdDelete.Enabled = False
        Else
            cmdDelete.Enabled = True
        End If
    Else
        cmdDelete.Enabled = False
    End If
    
End Sub

Private Sub txtAmount_GotFocus(Index As Integer)

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtAmount_LostFocus(Index As Integer)

    Dim i As Long
    
    If blnAdding Or blnEditing Then
    
        If Index < 12 Then
            For i = Index To 11
                If txtProcedure(i + 1) <> "" Then
                    If txtAmount(i + 1).Text = "" Or Val(txtAmount(i + 1).Text) > Val(txtAmount(i).Text) Then txtAmount(i + 1).Text = txtAmount(i).Text
                End If
            Next i
            txtMoney(Index).Text = ToMoney(Val(txtPrice(Index).Text) * Val(txtAmount(Index).Text))
        End If
        
        txtAmount1.Text = txtAmount(Index).Text
        txtAmount2.Text = txtAmount(Index).Text
        
    End If
    
End Sub

Private Sub txtAmount0_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtAmount0_LostFocus()

    Dim i As Long
    
    If blnAdding Or blnEditing Then
    
        For i = 0 To 12
            If txtProcedure(i) = "" Then
                Exit For
            Else
                If txtAmount(i).Text = "" Or Val(txtAmount(i).Text) > Val(txtAmount0.Text) Then
                    txtAmount(i).Text = txtAmount0.Text
                    txtMoney(i).Text = ToMoney(Val(txtPrice(i).Text) * Val(txtAmount(i).Text))
                End If
            End If
        Next i
        
        If txtAmount1.Text = "" Then
            txtAmount1.Text = txtAmount0.Text
        End If
        If txtAmount2.Text = "" Then
            txtAmount2.Text = txtAmount0.Text
            txtAmount3.Text = 0
        End If
        
    End If
    
End Sub

Private Sub txtAmount1_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtAmount2_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtAmount3_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtFilter_Change()

    If ActiveControl.Name = txtFilter.Name Then
        Call DisplayFilteredProducts
    End If
    
End Sub

Private Sub txtFilter_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyDown Then
        lstFilteredProducts.SetFocus
    End If
    
End Sub

Private Sub txtFilter_LostFocus()

    If ActiveControl.Name <> lstFilteredProducts.Name Then
        txtFilter.Visible = False
        lstFilteredProducts.Visible = False
        If lstFilteredProducts.ListIndex >= 0 Then
            lcProducts.Value = m_ProductIDs.Item(lstFilteredProducts.ListIndex + 1)
        End If
    End If

End Sub

Private Sub txtID_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtMoney_GotFocus(Index As Integer)

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtPrice_GotFocus(Index As Integer)

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtPrice_LostFocus(Index As Integer)

    txtMoney(Index).Text = ToMoney(Val(txtPrice(Index).Text) * Val(txtAmount(Index).Text))
    
End Sub
