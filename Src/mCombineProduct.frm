VERSION 5.00
Object = "{19A6DCA1-B1C0-11D5-9712-5254AB22AAF0}#2.0#0"; "lc.ocx"
Begin VB.Form mfrmCombineProduct 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "合并产品"
   ClientHeight    =   2865
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5775
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2865
   ScaleWidth      =   5775
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCombine 
      Caption         =   "合并(&B)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   2100
      TabIndex        =   4
      ToolTipText     =   "合并名称相同、编号不同的产品"
      Top             =   2220
      Width           =   1575
   End
   Begin LabelComboBox.lc lcProducts 
      Height          =   360
      Left            =   960
      TabIndex        =   3
      Top             =   660
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   635
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "到："
      ListIndex       =   -1
      Spacing         =   60
   End
   Begin VB.Label lblToCombine 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "产品编号及名称"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1560
      TabIndex        =   1
      Top             =   240
      Width           =   1680
   End
   Begin VB.Label lblNotice 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "提示：被合并的产品编号将被删除，相关生产单、发货记录中用到的产品编号将改为合并到的产品编号，操作不可回退！！"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   240
      Left            =   240
      TabIndex        =   2
      Top             =   1200
      Width           =   13770
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "合并产品："
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1200
   End
End
Attribute VB_Name = "mfrmCombineProduct"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public ProductID As String
Public ProductName As String
Public Success As Boolean

Private Sub cmdCombine_Click()

    cmdCombine.Enabled = False
    
    If CombineProduct(ProductID, lcProducts.Value) Then
        Success = True
        MsgBox "合并产品成功。", vbInformation, msgPrompt
    Else
        MsgBox "合并产品失败。", vbExclamation, msgPrompt
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
    
End Sub

Private Sub Form_Load()

    lblToCombine.Caption = ProductID & " - " & ProductName
    
    lblNotice.Caption = "提示：被合并的产品编号将被删除，相关生产" & vbCrLf & "　　单、发货记录中用到的产品编号将改为" & vbCrLf & "　　合并到的产品编号，操作不可回退！！"
    
    Set lcProducts.DataSource = g_cnADO.Execute("select 编号, 编号 + ' - ' + 名称 from 产品 where 名称 = '" & ProductName & "' and 编号 <> '" & ProductID & "'")
    
    If lcProducts.ListCount > 0 Then
        lcProducts.ListIndex = 0
    End If
    
End Sub
