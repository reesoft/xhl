VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "小花篮管理系统"
   ClientHeight    =   2595
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   6360
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Main.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2595
   ScaleWidth      =   6360
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fra 
      Height          =   2235
      Left            =   180
      TabIndex        =   6
      Top             =   120
      Width           =   6015
      Begin VB.CommandButton cmdShortcut 
         Caption         =   "浏览发货记录"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Index           =   3
         Left            =   240
         TabIndex        =   3
         Top             =   1260
         Width           =   1815
      End
      Begin VB.CommandButton cmdShortcut 
         Caption         =   "浏览收款记录"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Index           =   4
         Left            =   2100
         TabIndex        =   4
         Top             =   1260
         Width           =   1815
      End
      Begin VB.CommandButton cmdShortcut 
         Caption         =   "出对帐单"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Index           =   5
         Left            =   3960
         TabIndex        =   5
         Top             =   1260
         Width           =   1815
      End
      Begin VB.CommandButton cmdShortcut 
         Caption         =   "浏览生产单"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Index           =   0
         Left            =   240
         TabIndex        =   0
         Top             =   360
         Width           =   1815
      End
      Begin VB.CommandButton cmdShortcut 
         Caption         =   "计算工资"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Index           =   1
         Left            =   2100
         TabIndex        =   1
         Top             =   360
         Width           =   1815
      End
      Begin VB.CommandButton cmdShortcut 
         Caption         =   "统计产销量"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Index           =   2
         Left            =   3960
         TabIndex        =   2
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Menu mnuOperate 
      Caption         =   "操作(&O)"
      Begin VB.Menu mnuOBrowseProcedureSheets 
         Caption         =   "浏览生产单"
      End
      Begin VB.Menu mnuOInputProcedureSheet 
         Caption         =   "录入生产单"
      End
      Begin VB.Menu mnuOBrowseSaleSheets 
         Caption         =   "浏览发货记录"
      End
      Begin VB.Menu mnuOInputSaleSheet 
         Caption         =   "录入发货单"
      End
      Begin VB.Menu mnuOBrowseIncomeRecords 
         Caption         =   "浏览收款记录"
      End
      Begin VB.Menu mnuOInputIncomeRecord 
         Caption         =   "录入收款项"
      End
      Begin VB.Menu mnuOACSheet 
         Caption         =   "输出对帐单"
      End
      Begin VB.Menu mnuOStatSalary 
         Caption         =   "计算工资"
      End
      Begin VB.Menu mnuOStatProduce 
         Caption         =   "统计产销量"
      End
      Begin VB.Menu mnuOStatStorage 
         Caption         =   "统计当前库存量"
      End
   End
   Begin VB.Menu mnuMaintain 
      Caption         =   "维护(&M)"
      Begin VB.Menu mnuMRegions 
         Caption         =   "维护地区"
      End
      Begin VB.Menu mnuMClients 
         Caption         =   "维护客户"
      End
      Begin VB.Menu mnuSplit1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMProducts 
         Caption         =   "维护产品"
      End
      Begin VB.Menu mnuMProcedures 
         Caption         =   "维护工序"
      End
      Begin VB.Menu mnuSplit2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMStaff 
         Caption         =   "职工管理"
      End
      Begin VB.Menu mnuSplit3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuMShortcuts 
         Caption         =   "维护快捷按钮"
      End
   End
   Begin VB.Menu mnuSecurity 
      Caption         =   "安全(&S)"
      Begin VB.Menu mnuSPassword 
         Caption         =   "设置密码"
      End
   End
   Begin VB.Menu mnuData 
      Caption         =   "数据(&D)"
      Begin VB.Menu mnuDBackup 
         Caption         =   "备份数据"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDRestore 
         Caption         =   "恢复数据"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDMoveToHistory 
         Caption         =   "归档数据(&A)"
      End
      Begin VB.Menu mnuDImportData 
         Caption         =   "导入数据(&I)"
      End
      Begin VB.Menu mnuSplitD1 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDDelete 
         Caption         =   "删除过期数据(&D)"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "帮助(&H)"
      Begin VB.Menu mnuHContent 
         Caption         =   "阅读帮助"
      End
      Begin VB.Menu mnuSplitH1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHRegister 
         Caption         =   "软件注册"
      End
      Begin VB.Menu mnuAbout 
         Caption         =   "关于本软件"
      End
   End
   Begin VB.Menu mnuExit 
      Caption         =   "退出(&X)"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' 备份数据
Private Sub BackupDB()
    
    Dim strFilename As String
    
'    If g_strLoginID <> gc_SuperUserID Then
'        MsgBox "只有超级用户才能执行此操作。", vbInformation, msgPrompt
'        Exit Sub
'    End If
    
    On Error GoTo PROC_ERR
    
    strFilename = CommonDialogFileSave(Me.hwnd, "小花篮数据备份" & Format(Date, "yymmdd") & ".mdb", "将数据备份到...", , "数据库文件(*.mdb)|*.mdb")
    
    If strFilename = "" Then GoTo PROC_EXIT
    
    If Dir(strFilename) <> "" Then
        If UCase(strFilename) = UCase(g_DataPath & gc_DBName) Then
            MsgBox "不能将数据备份到这个文件。", vbInformation, msgPrompt
            GoTo PROC_EXIT
        End If
        If MsgBox("文件 " & strFilename & " 已经存在，是否覆盖？", vbQuestion + vbYesNo, msgPrompt) = vbNo Then
            GoTo PROC_EXIT
        Else
            Kill strFilename
        End If
    End If
    FileCopy g_DBName, strFilename
    
    MsgBox "数据已经成功备份到 " & strFilename & "。", vbInformation, msgOperationSuccess
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    If Err.Number = 70 Then
        MsgBox "备份数据时发生错误！数据库文件可能已经被其他程序打开。请关闭所以程序（包括本程序），再试一次。", vbCritical, "运行时段错误"
    Else
        MsgBox "备份数据时发生错误：" & vbCrLf & Err.Description, vbCritical, "运行时段错误"
    End If
    
End Sub

' 恢复数据
Private Sub RestoreDB()
    
    Dim strFilename As String
    
'    If g_strLoginID <> gc_SuperUserID Then
'        MsgBox "只有超级用户才能执行此操作。", vbInformation, msgPrompt
'        Exit Sub
'    End If
    
    On Error GoTo PROC_ERR
    
    strFilename = CommonDialogFileOpen(Me.hwnd, , "恢复数据自...", , "数据库文件(*.mdb)|*.mdb")
    
    If strFilename = "" Then GoTo PROC_EXIT
    
    If UCase(strFilename) = UCase(g_DataPath & gc_DBName) Then
        MsgBox "无法从该文件恢复数据。", vbExclamation, msgPrompt
        GoTo PROC_EXIT
    End If
    
    If Dir(strFilename) = "" Then
        MsgBox "文件 " & strFilename & " 并不存在，无法从该文件恢复数据。", vbExclamation, msgWarn
        GoTo PROC_EXIT
    End If
    If Dir(g_DBName & ".bak") <> "" Then Kill g_DBName & ".bak"
    Name g_DBName As g_DBName & ".bak"
    FileCopy strFilename, g_DBName
    Kill g_DBName & ".bak"
    
    MsgBox "数据已经成功恢复。", vbInformation, msgOperationSuccess
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    If Err.Number = 70 Then
        MsgBox "恢复数据时发生错误！数据库文件可能已经被其他程序打开。请关闭所以程序（包括本程序），再试一次。", vbCritical, "运行时段错误"
    Else
        MsgBox "恢复数据时发生错误：" & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    End If
    
End Sub

Private Sub cmdShortcut_Click(Index As Integer)

    Dim i As Long
    
    For i = 1 To g_Shortcuts.Count
        If g_Shortcuts.Item(i).Name = cmdShortcut(Index).Caption Then
            Call ExecuteCommand(g_Shortcuts.Item(i).Command)
            Exit For
        End If
    Next i
    
End Sub

Private Sub ExecuteCommand(strCommand As String)

    Select Case strCommand
        Case "BrowseProcedureSheets"
            Call mnuOBrowseProcedureSheets_Click
        Case "InputProcedureSheet"
            Call mnuOInputProcedureSheet_Click
        Case "BrowseSaleSheets"
            Call mnuOBrowseSaleSheets_Click
        Case "InputSaleSheet"
            Call mnuOInputSaleSheet_Click
        Case "BrowseIncomeRecords"
            Call mnuOBrowseIncomeRecords_Click
        Case "InputIncomeRecord"
            Call mnuOInputIncomeRecord_Click
        Case "GenerateACSheet"
            Call mnuOACSheet_Click
        Case "StatSalary"
            Call mnuOStatSalary_Click
        Case "StatProduce"
            Call mnuOStatProduce_Click
        Case "StatStorage"
            Call mnuOStatStorage_Click
        Case "ImportData"
            Call mnuDImportData_Click
        Case "MoveToHistory"
            Call mnuDMoveToHistory_Click
    End Select
    
End Sub

Private Sub Form_Load()

    If Not g_DemoVersion Then
        mnuHRegister.Visible = False
    End If
    
    Me.Caption = Me.Caption & " V" & App.Major & "." & App.Minor
    
    Call DisplayShortcuts
    
End Sub

Private Sub DisplayShortcuts()

    Const ROWS As Long = 2
    Const COLUMNS As Long = 3
    Dim iRow As Long
    Dim iColumn As Long
    
    For iRow = 0 To ROWS - 1
        For iColumn = 0 To COLUMNS - 1
            cmdShortcut(iRow * 3 + iColumn).Caption = g_Shortcuts.Item(iRow * 3 + iColumn + 1).Name
        Next iColumn
    Next iRow
    
End Sub

Private Sub mnuAbout_Click()

    frmAbout.Show vbModal, Me
    
End Sub

Private Sub mnuDBackup_Click()

    Me.Enabled = False
    DoEvents
    Call BackupDB
    Me.Enabled = True
    
End Sub

Private Sub mnuDDelete_Click()

    mfrmDeleteData.Show vbModal, Me
    
End Sub

Public Sub mnuDImportData_Click()

    mfrmImportData.Show vbModal, Me
    
End Sub

Public Sub mnuDMoveToHistory_Click()

    mfrmMoveToHistory.Show vbModal, Me
    
End Sub

Private Sub mnuDRestore_Click()

    Me.Enabled = False
    DoEvents
    Call RestoreDB
    Me.Enabled = True
    
End Sub

Private Sub mnuExit_Click()

    Call EndProgram
    
End Sub

Private Sub mnuHContent_Click()

    Call ShellExecute(Me.hwnd, "open", gc_HelpFile, "", g_WorkPath, SW_RESTORE)
    
End Sub

Private Sub mnuHRegister_Click()

    frmRegister.Show vbModal, Me
    
End Sub

Private Sub mnuMClients_Click()

    mfrmClients.Show vbModal, Me
    
End Sub

Private Sub mnuMProcedures_Click()

    mfrmProcedures.Show vbModal, Me
    
End Sub

Private Sub mnuMProducts_Click()

    mfrmProducts.Show vbModal, Me
    
End Sub

Private Sub mnuMRegions_Click()

    mfrmRegions.Show vbModal, Me
    
End Sub

Private Sub mnuMShortcuts_Click()

    mfrmShortcuts.Show vbModal, Me
    Call DisplayShortcuts
    
End Sub

Private Sub mnuMStaff_Click()

    mfrmStaff.Show vbModal, Me
    
End Sub

Private Sub mnuOACSheet_Click()

    frmACSheet.Show vbModal, Me
    
End Sub

Private Sub mnuOBrowseIncomeRecords_Click()

    frmIncome.Show vbModal, Me
    
End Sub

Private Sub mnuOBrowseProcedureSheets_Click()

    frmProcedureSheets.Show vbModal, Me
    
End Sub

Private Sub mnuOBrowseSaleSheets_Click()

    frmSales.Show vbModal, Me
    
End Sub

Private Sub mnuOInputIncomeRecord_Click()

    Load frmIncome
    Call frmIncome.Add
    frmIncome.Show vbModal, Me
    
End Sub

Public Sub mnuOInputProcedureSheet_Click()

    Load frmProcedureSheets
    Call frmProcedureSheets.Add
    frmProcedureSheets.Show vbModal, Me
    
End Sub

Public Sub mnuOInputSaleSheet_Click()

    Load frmSales
    Call frmSales.Add
    frmSales.Show vbModal, Me
    
End Sub

Private Sub mnuOStatProduce_Click()

    frmProduceReport.Show vbModal, Me
    
End Sub

Public Sub mnuOStatSalary_Click()

    frmSalaries.Show vbModal, Me
    
End Sub

Public Sub mnuOStatStorage_Click()

    frmStorageReport.Show vbModal, Me
    
End Sub

Private Sub mnuSPassword_Click()

    If g_LoginID <> gc_SuperUserID Then
        MsgBox "只有超级用户才能执行此操作。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    mfrmUsers.Show vbModal, Me
    
End Sub
