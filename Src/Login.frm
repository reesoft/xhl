VERSION 5.00
Begin VB.Form frmLogin 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "小花篮管理系统 - 登录"
   ClientHeight    =   2355
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4575
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2355
   ScaleWidth      =   4575
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtPass 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      IMEMode         =   3  'DISABLE
      Left            =   1800
      PasswordChar    =   "*"
      TabIndex        =   1
      ToolTipText     =   "默认管理员账号和密码都是星号 *"
      Top             =   1200
      Width           =   1995
   End
   Begin VB.TextBox txtUser 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1800
      TabIndex        =   0
      ToolTipText     =   "默认管理员账号和密码都是星号 *"
      Top             =   720
      Width           =   1995
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "取消(&C)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   2340
      TabIndex        =   3
      Top             =   1740
      Width           =   1575
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "确定(&O)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   660
      TabIndex        =   2
      Top             =   1740
      Width           =   1575
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "欢迎使用小花篮管理系统"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   15.75
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   315
      Index           =   2
      Left            =   405
      TabIndex        =   6
      Top             =   240
      Width           =   3810
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "密    码"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   720
      TabIndex        =   5
      Top             =   1260
      Width           =   960
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "用户名称"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   720
      TabIndex        =   4
      Top             =   780
      Width           =   960
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public LoginOK As Boolean

Private Sub cmdCancel_Click()

    LoginOK = False
    Me.Hide
    
End Sub

Private Sub cmdOK_Click()

    If txtUser.Text = "" Then
        MsgBox "请输入合法用户名称！", vbInformation, "登录"
        txtUser.SetFocus
        Exit Sub
    End If
    
    txtUser.Tag = Replace(txtUser.Text, "'", "''")
    
    With g_cnADO.Execute("select 密码 from 用户 where 用户名称 = '" & txtUser.Tag & "'", , adCmdText)
    
        If .BOF And .EOF Then
            MsgBox "用户名称或密码密码无效！", vbInformation, "登录"
            DoEvents
            txtUser.SetFocus
            Exit Sub
        End If
        
        If Encrypt(txtPass.Text) = !密码 Then
            LoginOK = True
            g_LoginID = txtUser.Text
            g_Password = txtPass.Text
            Call SaveSet("LastLoginID", g_LoginID)
            Me.Hide
            frmMain.Show
        Else
            MsgBox "用户名称或密码密码无效！", vbExclamation, "登录"
            txtUser.SetFocus
            Exit Sub
        End If
        
    End With
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
        If ActiveControl.Name = txtUser.Name Then
            If txtUser.Text <> "" Then
                KeyCode = 0
                txtPass.SetFocus
            End If
        ElseIf ActiveControl.Name = txtPass.Name Then
            If txtPass.Text <> "" Then
                KeyCode = 0
                cmdOK.SetFocus
            End If
        End If
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
    
    If txtUser.Text <> "" Then
        txtPass.TabIndex = 0
    End If
    
End Sub

Private Sub InitData()
    
    txtUser.Text = GetSet("LastLoginID", "")
    
End Sub

Private Sub txtPass_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtUser_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub
