VERSION 5.00
Begin VB.Form mfrmUsers 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "操作员维护"
   ClientHeight    =   3150
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5085
   Icon            =   "mUsers.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3150
   ScaleWidth      =   5085
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtPass2 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      IMEMode         =   3  'DISABLE
      Left            =   3660
      MaxLength       =   20
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   533
      Width           =   1275
   End
   Begin VB.TextBox txtPass 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      IMEMode         =   3  'DISABLE
      Left            =   3660
      MaxLength       =   20
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   120
      Width           =   1275
   End
   Begin VB.TextBox txtName 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1200
      MaxLength       =   10
      TabIndex        =   1
      Top             =   120
      Width           =   1695
   End
   Begin VB.CommandButton cmdExit 
      Cancel          =   -1  'True
      Caption         =   "关闭(&X)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3060
      TabIndex        =   7
      Top             =   2580
      Width           =   1875
   End
   Begin VB.CommandButton cmdUpdate 
      Caption         =   "修改(&E)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3060
      TabIndex        =   6
      Top             =   2100
      Width           =   1875
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "删除(&D)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3060
      TabIndex        =   5
      Top             =   1620
      Width           =   1875
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "添加(&A)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3060
      TabIndex        =   4
      Top             =   1140
      Width           =   1875
   End
   Begin VB.ListBox lst 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2460
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   2775
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "确认"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3060
      TabIndex        =   10
      Top             =   585
      Width           =   480
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "密码"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3060
      TabIndex        =   9
      Top             =   165
      Width           =   480
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "用户名称"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   120
      TabIndex        =   8
      Top             =   165
      Width           =   960
   End
End
Attribute VB_Name = "mfrmUsers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_UserIDs As New Collection
Dim m_Passwords As New Collection

Private Sub cmdAdd_Click()

    Call SaveAdd
    
End Sub

Private Sub cmdDelete_Click()

    If m_UserIDs.Item(lst.ListIndex + 1) = gc_SuperUserID Then
        MsgBox "不能删除超级用户帐号。", vbExclamation, msgPrompt
        Exit Sub
    End If
    
    txtName.Tag = Replace(txtName.Text, "'", "''")
    
    g_cnADO.Execute "delete from 用户 where 用户名称 = '" & txtName.Tag & "'"
    
    Dim intPos As Integer
    
    intPos = lst.ListIndex
    Call DisplayList
    lst.ListIndex = IIf(intPos < lst.ListCount - 1, intPos, lst.ListCount - 1)
    Call lst_Click
    
End Sub

Private Sub cmdExit_Click()

    Unload Me
    
End Sub

Private Sub cmdUpdate_Click()

    Call SaveEdit
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
    
        If TypeName(ActiveControl) = "TextBox" _
            Or TypeName(ActiveControl) = "ComboBox" _
            Or TypeName(ActiveControl) = "CheckBox" _
            Or TypeName(ActiveControl) = "DTPicker" _
            Or TypeName(ActiveControl) = "lc" Then
            
            On Error Resume Next
            
            SendKeys "{TAB}"
            
        End If
        
    End If
    
End Sub

Private Sub Form_Load()

    Call DisplayList
    
End Sub

Private Sub lst_Click()

    If lst.ListIndex < 0 Or lst.ListIndex >= m_UserIDs.Count Then
        Exit Sub
    End If
    
    txtName.Text = m_UserIDs.Item(lst.ListIndex + 1)
    txtPass.Text = m_Passwords.Item(lst.ListIndex + 1)
    txtPass2.Text = txtPass.Text
    
End Sub

Private Sub DisplayList()

    Set m_UserIDs = New Collection
    Set m_Passwords = New Collection
    lst.Clear
    
    With g_cnADO.Execute("select * from 用户 order by 用户名称", , adCmdText)
        Do Until .EOF
            m_UserIDs.Add !用户名称 & ""
            m_Passwords.Add !密码 & ""
            lst.AddItem !用户名称 & ""
            .MoveNext
        Loop
    End With
    
    If lst.ListCount > 0 Then
        lst.ListIndex = 0
        Call lst_Click
    End If
    
End Sub

' 添加操作用户信息
Private Sub SaveAdd()

    If txtName.Text = "" Then
        MsgBox "请输入用户名称。", vbInformation, msgPrompt
        txtName.SetFocus
        Exit Sub
    End If
    
    txtName.Tag = Replace(txtName.Text, "'", "''")
    
    If StrLen(txtName.Text) > 20 Then
        MsgBox "用户名称应该在 20 个字符以内（每个汉字占两个字符）。", vbInformation, msgPrompt
        txtName.SetFocus
        Exit Sub
    End If
    
    If txtPass.Text <> txtPass2.Text Then
        MsgBox "您两次输入的密码不一致，请重新输入。", vbInformation, msgPrompt
        txtPass.SetFocus
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    With g_cnADO.Execute("select * from 用户 where 用户名称 = '" & txtName.Tag & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            MsgBox "无法添加，因为另一个同名用户已经存在了。", vbInformation, msgPrompt
            Exit Sub
        End If
    End With
    
    g_cnADO.Execute "insert into 用户 (用户名称, 密码) values ('" & txtName.Tag & "', '" & Encrypt(txtPass.Text) & "')"
    
    Dim intPos As Integer
    
    intPos = lst.ListIndex
    Call DisplayList
    lst.ListIndex = intPos
    Call lst_Click
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox "添加记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 修改操作用户信息
Private Sub SaveEdit()

    If txtName.Text = "" Then
        MsgBox "请输入用户名称。", vbInformation, msgPrompt
        txtName.SetFocus
        Exit Sub
    End If
    
    txtName.Tag = Replace(txtName.Text, "'", "''")
    
    If StrLen(txtName.Text) > 20 Then
        MsgBox "用户名称应该在 20 个字符以内（每个汉字占两个字符）。", vbInformation, msgPrompt
        txtName.SetFocus
        Exit Sub
    End If
    
    If txtPass.Text <> txtPass2.Text Then
        MsgBox "您两次输入的密码不一致，请重新输入。", vbInformation, msgPrompt
        txtPass.SetFocus
        Exit Sub
    End If
    
    ' 如果没有任何改变，直接跳出
    If lst.ListIndex < 0 Then
        Exit Sub
    End If
    
    If txtName.Text = m_UserIDs.Item(lst.ListIndex + 1) And txtPass.Text = m_Passwords(lst.ListIndex + 1) Then
        Exit Sub
    End If
    
    If m_UserIDs.Item(lst.ListIndex + 1) = gc_SuperUserID And m_UserIDs.Item(lst.ListIndex + 1) <> txtName.Text Then
        MsgBox "不能修改超级用户帐号。", vbExclamation, msgPrompt
        Exit Sub
    End If
    
    Dim i As Long
    
    For i = 1 To m_UserIDs.Count
        If i <> lst.ListIndex + 1 And m_UserIDs.Item(i) = txtName.Text Then
            MsgBox "无法修改，因为另一个同名用户已经存在了。", vbInformation, msgPrompt
            Exit Sub
        End If
    Next i
    
    ' 在未修改密码的情况下，密码文本框的内容与密文是一样的，不用更新到数据库
    If txtPass.Text <> m_Passwords.Item(lst.ListIndex + 1) Then
        g_cnADO.Execute "update 用户 set 用户名称 = '" & txtName.Tag & "', 密码 = '" & Encrypt(txtPass.Text) & "' where 用户名称 = '" & m_UserIDs.Item(lst.ListIndex + 1) & "'", , adCmdText
    Else
        g_cnADO.Execute "update 用户 set 用户名称 = '" & txtName.Tag & "' where 用户名称 = '" & m_UserIDs.Item(lst.ListIndex + 1) & "'", , adCmdText
    End If
    
    Dim intPos As Integer
    
    intPos = lst.ListIndex
    Call DisplayList
    lst.ListIndex = intPos
    Call lst_Click
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox "添加记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

Private Sub txtName_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtPass_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtPass2_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub
