Attribute VB_Name = "XHLUtilities"
Option Explicit

Global Const gc_DBName = "XHL.mdb"
Global Const gc_NewDBName = "XHL.new.mdb"
Global Const gc_HelpFile = "XHL.chm"
Global Const gc_Char = "R"
Global Const gc_SuperUserID = "*"

Global Const gc_ProductSN = "07100001"
Global Const gc_DelayBetweenMultiPages = 12 ' 多页连续打印的间隔时间

Global g_WorkPath As String
Global g_DataPath As String
Global g_DBName As String

Global g_DemoVersion As Boolean
Global g_UserName As String
Global g_Company As String
Global g_ProductSN As String
Global g_RegisterCode As String
Global g_DiskID As String
Global g_Code As String

Global g_LoginID As String
Global g_Password As String
Global g_DBPassword As String

' Printer object or preview object
Global g_Preview As Boolean
Global g_Printer As Variant

Global g_Shortcuts As New Collection

' Base64 Encryption
Global Const gc_B64EncryptKey = "XHLABCDEFGIJKM#OPQRSTUVWYZ0123456789$%abcdefghijklmnopqrstuvwxyz"
Global Const gc_B64FillCharacter = "N"

Global g_cnADO As New ADODB.Connection  ' 全局的数据库连接
Global g_rsADO As New ADODB.Recordset  ' 全局的记录集 1

Declare Function DiskID32 Lib "XHL.dll" (ByRef DiskModel As Byte, ByRef DiskID As Byte) As Long

' 开始打印预览
Public Sub DoPreview()
    
    g_Preview = True
    frmPreview.Show
    Set g_Printer = frmPreview.picPreview
    
End Sub

' 开始打印
Public Sub DoPrint()
    
    g_Preview = False
    Set g_Printer = Printer
    
End Sub

' 关闭数据库连接
Public Sub CloseDatabase()
    
    If g_cnADO.State <> adStateClosed Then
        g_cnADO.Close
    End If
    
    Set g_cnADO = Nothing
    
End Sub

' 程序出口
Public Sub EndProgram()
    
    Dim frm As Form
    
    For Each frm In Forms
        Unload frm
    Next
    
    Call CloseDatabase
    
    End
    
End Sub

' 填充新记录的公用字段
Public Sub FillNewRecordData(rsADO As ADODB.Recordset)

    rsADO!GUID = GenerateGUID()
    rsADO!MachineCode = GetDiskID()
    rsADO!CreatedBy = g_LoginID
    rsADO!CreatedTime = Now
    rsADO!LastUpdatedBy = g_LoginID
    rsADO!LastUpdatedTime = Now
    
End Sub

' 填充修改记录的公用字段
Public Sub FillUpdateRecordData(rsADO As ADODB.Recordset)

    rsADO!MachineCode = GetDiskID()
    rsADO!LastUpdatedBy = g_LoginID
    rsADO!LastUpdatedTime = Now
    
End Sub

' 填充记录空缺的公用字段
Public Sub FillBlankRecordData(rsADO As ADODB.Recordset)

    If IsNull(rsADO!GUID) Or rsADO!GUID = "" Then
        rsADO!GUID = GenerateGUID()
    End If
    
    If IsNull(rsADO!MachineCode) Or rsADO!MachineCode = "" Then
        rsADO!MachineCode = GetDiskID()
    End If
    
    If IsNull(rsADO!CreatedBy) Or rsADO!CreatedBy = "" Then
        rsADO!CreatedBy = g_LoginID
        rsADO!CreatedTime = Now
    End If
    
    If IsNull(rsADO!LastUpdatedBy) Or rsADO!LastUpdatedBy = "" Then
        rsADO!LastUpdatedBy = g_LoginID
        rsADO!LastUpdatedTime = Now
    End If
    
End Sub

' 从数据库指定表的指定字段取得时间范围，用来初始化日期选择控件
Public Sub InitDateRange(cnADO As ADODB.Connection, strTables As String, dtpFrom As DTPicker, dtpTo As DTPicker)

    Dim arrTables() As String
    Dim i As Long
    Dim dateFrom As Date, dateTo As Date
    Dim blnGot As Boolean
    
    dateFrom = Date
    
    arrTables = Split(strTables, ",")
    
    For i = 0 To UBound(arrTables)
        If GetDateRange(cnADO, arrTables(i), "日期", dateFrom, dateTo) Then
            blnGot = True
        End If
    Next i
    
    If blnGot Then
        dtpFrom.Tag = dateFrom
        dtpTo.Tag = dateTo
    Else
        dtpFrom.Tag = CDate(Year(Date) & "-01-01")
        dtpTo.Tag = Date
    End If
    
End Sub

' 取得硬盘序号
Public Function GetDiskID() As String
    
    Dim bytDiskModuel(31) As Byte, bytDiskID(31) As Byte
    Dim lngReturnValue As Long
    Dim strDiskID As String
    
    lngReturnValue = DiskID32(bytDiskModuel(0), bytDiskID(0))
    
    If lngReturnValue = 0 Then
        GetDiskID = ""
    Else
        strDiskID = StrConv(bytDiskModuel, vbUnicode)
        strDiskID = strDiskID & " " & StrConv(bytDiskID, vbUnicode)
        strDiskID = Replace(strDiskID, Chr(0), "")
        GetDiskID = Trim(strDiskID)
    End If
    
End Function

Public Sub LoadShortcuts()

    Dim strCommand As String
    Dim strCommands() As String
    Dim i As Long
    Dim lngPos As Long
    
    strCommand = GetSet("Shortcuts", "浏览生产单,BrowseProcedureSheets;计算工资,StatSalary;统计产销量,StatProduce;浏览发货记录,BrowseSaleSheets;浏览收款记录,BrowseIncomeRecords;出对帐单,GenerateACSheet;录入生产单,InputProcedureSheet;录入发货单,InputSaleSheet;录入收款项,InputIncomeRecord;导入数据,ImportData;归档数据,MoveToHistory;统计库存,StatStorage")
    
    strCommands = Split(strCommand, ";")
    
    For i = 0 To UBound(strCommands)
    
        lngPos = InStr(strCommands(i), ",")
        
        If lngPos > 0 Then
            Call AddShortcut(Left(strCommands(i), lngPos - 1), Mid(strCommands(i), lngPos + 1))
        End If
        
    Next i
    
End Sub

Public Sub SaveShortcuts()

    Dim i As Long
    Dim strCommands As String
    
    For i = 1 To g_Shortcuts.Count
        strCommands = strCommands & g_Shortcuts.Item(i).Name & "," & g_Shortcuts.Item(i).Command & ";"
    Next i
    
    If strCommands <> "" Then
        Call SaveSet("Shortcuts", strCommands)
    End If
    
End Sub

Private Sub AddShortcut(strName As String, strCommand As String)

    Dim sc As New CShortcut
    
    sc.Name = strName
    sc.Command = strCommand
    
    g_Shortcuts.Add sc
    
End Sub

' 程序入口
Sub Main()
    
    g_WorkPath = App.Path
    If Right(g_WorkPath, 1) <> "\" Then g_WorkPath = g_WorkPath & "\"
    g_DataPath = g_WorkPath
    g_DBName = g_DataPath & gc_DBName
    
    If Dir(g_DBName) = "" Then
        FileCopy g_DataPath & gc_NewDBName, g_DBName
    End If
    
    If Not OpenDatabase() Then
        Exit Sub
    End If
    
    Call InitRegistry("SOFTWARE\Reesoft\XHL")
    
    Call LoadShortcuts
    
    Set g_Printer = Printer
    
    If VerifyAuthority = False Then
        Call EndProgram
        Exit Sub
    End If
    
    g_DemoVersion = False
    
    frmLogin.Show vbModal
    
    If frmLogin.LoginOK Then
        Unload frmLogin
        Set frmLogin = Nothing
        frmMain.Show
    Else
        Call EndProgram
        Exit Sub
    End If
    
End Sub

' 打开数据库
Private Function OpenDatabase() As Boolean
    
    Const c_MDACDownloadURL = "http://download.microsoft.com/download/dasdk/SP/2.52/W9XNT4/CN/MDAC_TYP_cn.EXE"
    On Error GoTo PROC_ERR
    
    With g_cnADO
        If .State <> adStateClosed Then .Close
        .Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source= " & g_DBName & ";Persist Security Info=False"
    End With
    
    OpenDatabase = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    OpenDatabase = False
    If Err.Number = 429 Or Err.Number = 525 Or Err.Number = 3706 Or Err.Number = 713 Then
        If MsgBox("本软件需要 ADO 数据引擎支持，而您的系统上未安装该数据引擎或已安装的数据引擎版本与本软件不兼容，请从微软（Microsoft）网站下载并安装数据访问部件（MDAC）再运行本软件。" & vbCrLf & vbCrLf & "现在就下载数据访问部件吗？", vbQuestion + vbYesNo, msgPrompt) = vbYes Then
            Call ShellExecute(0, "open", c_MDACDownloadURL, vbNullString, vbNullString, 0)
        End If
    End If
    GoTo PROC_EXIT
    
End Function

' 打印文本
Public Sub PrintText(x As Single, y As Single, Fnt As Single, Text As String)
    
    If g_DemoVersion Then Exit Sub ' 试用版不打印任何东西
    
    g_Printer.CurrentX = x
    g_Printer.CurrentY = y
    g_Printer.FontSize = Fnt
    g_Printer.Print Text
    
End Sub

' 删除列表控件当前选中的项，选中下一项
Public Sub RemoveSelectedRow(lvw As ListView)

    Dim lngSelectedRow As Long
    
    lngSelectedRow = lvw.SelectedItem.Index
    
    lvw.ListItems.Remove lngSelectedRow
    
    If lvw.ListItems.Count >= lngSelectedRow Then
        Set lvw.SelectedItem = lvw.ListItems.Item(lngSelectedRow)
    ElseIf lvw.ListItems.Count > 0 Then
        Set lvw.SelectedItem = lvw.ListItems.Item(lvw.ListItems.Count)
    End If
    
End Sub

' 选中文本框控件的内容
Public Sub SelectMe(ctl As Control)
    
    If TypeName(ctl) = "TextBox" Then
        ctl.SelStart = 0
        ctl.SelLength = Len(ctl.Text)
    End If
    
End Sub

' 根据时间范围设置日期选择控件的值
Public Sub SetDateRange(strDateRange As String, dtpFrom As DTPicker, dtpTo As DTPicker)

    Select Case strDateRange
    
        Case "前一周"
            dtpFrom.Value = DateAdd("ww", -2, Date)
            dtpFrom.Value = DateAdd("d", -1 * Weekday(dtpFrom.Value) + 1, dtpFrom.Value)
            dtpTo.Value = DateAdd("d", 6, dtpFrom.Value)
            
        Case "上一周"
            dtpFrom.Value = DateAdd("ww", -1, Date)
            dtpFrom.Value = DateAdd("d", -1 * Weekday(dtpFrom.Value) + 1, dtpFrom.Value)
            dtpTo.Value = DateAdd("d", 6, dtpFrom.Value)
            
        Case "这一周"
            dtpFrom.Value = DateAdd("d", -1 * Weekday(Date) + 1, Date)
            dtpTo.Value = DateAdd("d", 6, dtpFrom.Value)
            
        Case "前个月"
            dtpFrom.Value = DateAdd("m", -2, Date)
            dtpFrom.Value = DateAdd("d", 1 - Day(dtpFrom.Value), dtpFrom.Value)
            dtpTo.Value = DateAdd("d", -1, DateAdd("m", 1, dtpFrom.Value))
            
        Case "上个月"
            dtpFrom.Value = DateAdd("m", -1, Date)
            dtpFrom.Value = DateAdd("d", 1 - Day(dtpFrom.Value), dtpFrom.Value)
            dtpTo.Value = DateAdd("d", -1, DateAdd("m", 1, dtpFrom.Value))
            
        Case "这个月"
            dtpFrom.Value = DateAdd("d", 1 - Day(Date), Date)
            dtpTo.Value = DateAdd("d", -1, DateAdd("m", 1, dtpFrom.Value))
            
        Case "上一季"
            dtpFrom.Value = DateAdd("m", 1 - Month(Date) Mod 3 - 3, Date)
            dtpFrom.Value = DateAdd("d", 1 - Day(dtpFrom.Value), dtpFrom.Value)
            dtpTo.Value = DateAdd("d", -1, DateAdd("m", 3, dtpFrom.Value))
            
        Case "本季度"
            dtpFrom.Value = DateAdd("m", 1 - Month(Date) Mod 3, Date)
            dtpFrom.Value = DateAdd("d", 1 - Day(dtpFrom.Value), dtpFrom.Value)
            dtpTo.Value = DateAdd("d", -1, DateAdd("m", 3, dtpFrom.Value))
            
        Case "前一年"
            dtpFrom.Value = CDate((Year(Date) - 2) & "-01-01")
            dtpTo.Value = CDate((Year(Date) - 2) & "-12-31")
            
        Case "上一年"
            dtpFrom.Value = CDate((Year(Date) - 1) & "-01-01")
            dtpTo.Value = CDate((Year(Date) - 1) & "-12-31")
            
        Case "本年度"
            dtpFrom.Value = CDate(Year(Date) & "-01-01")
            dtpTo.Value = CDate(Year(Date) & "-12-31")
            
        Case Else '全部
            On Error Resume Next
            dtpFrom.Value = CDate(dtpFrom.Tag)
            dtpTo.Value = CDate(dtpTo.Tag)
            
    End Select
    
End Sub

' 与数据库进行同步
Public Function SynchronizeDatabase(strDBName As String) As Boolean
    
    Dim cnADOA As New ADODB.Connection
    Dim rsADOA1 As New ADODB.Recordset
    Dim rsADOA2 As New ADODB.Recordset
    Dim rsADOB1 As New ADODB.Recordset
    Dim rsADOB2 As New ADODB.Recordset
    Dim i As Long
    
    On Error GoTo PROC_ERR
    
    With cnADOA
        .Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source= " & strDBName & ";Persist Security Info=False"
    End With
    
    ' 同步职工表
    With rsADOA1
        If .State <> adStateClosed Then
            .Close
        End If
        .Open "职工", cnADOA, adOpenDynamic, adLockOptimistic, adCmdTable
    End With
    With rsADOB1
        If .State <> adStateClosed Then
            .Close
        End If
        .Open "职工", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
    End With
    
    Do Until rsADOA1.EOF
        rsADOB1.MoveFirst
        rsADOB1.Find "GUID = '" & rsADOA1!GUID & "'"
        If Not rsADOB1.EOF Then
            If rsADOA1!LastUpdateTime > rsADOB1!LastUpdateTime Then
                rsADOB1!工号 = rsADOA1!工号
                rsADOB1!姓名 = rsADOA1!姓名
                rsADOB1!性别 = rsADOA1!性别
                rsADOB1!年龄 = rsADOA1!年龄
                rsADOB1!技能 = rsADOA1!技能
                rsADOB1!入厂日期 = rsADOA1!入厂日期
                rsADOB1!是否在职 = rsADOA1!是否在职
                rsADOB1!联系电话 = rsADOA1!联系电话
                rsADOB1!住址 = rsADOA1!住址
                rsADOB1!备注 = rsADOA1!备注
                rsADOB1!GUID = rsADOA1!GUID
                rsADOB1!MachineCode = rsADOA1!MachineCode
                rsADOB1!CreatedBy = rsADOA1!CreatedBy
                rsADOB1!CreatedTime = rsADOA1!CreatedTime
                rsADOB1!LastUpdatedBy = rsADOA1!LastUpdatedBy
                rsADOB1!LastUpdatedTime = rsADOA1!LastUpdatedTime
                rsADOB1.Update
            End If
        Else
            rsADOB1.AddNew
            rsADOB1!工号 = rsADOA1!工号
            rsADOB1!姓名 = rsADOA1!姓名
            rsADOB1!性别 = rsADOA1!性别
            rsADOB1!年龄 = rsADOA1!年龄
            rsADOB1!技能 = rsADOA1!技能
            rsADOB1!入厂日期 = rsADOA1!入厂日期
            rsADOB1!是否在职 = rsADOA1!是否在职
            rsADOB1!联系电话 = rsADOA1!联系电话
            rsADOB1!住址 = rsADOA1!住址
            rsADOB1!备注 = rsADOA1!备注
            rsADOB1!GUID = rsADOA1!GUID
            rsADOB1!MachineCode = rsADOA1!MachineCode
            rsADOB1!CreatedBy = rsADOA1!CreatedBy
            rsADOB1!CreatedTime = rsADOA1!CreatedTime
            rsADOB1!LastUpdatedBy = rsADOA1!LastUpdatedBy
            rsADOB1!LastUpdatedTime = rsADOA1!LastUpdatedTime
        End If
        
        rsADOA1.MoveNext
    Loop
    
    ' 同步发货记录表
    With rsADOA1
        If .State <> adStateClosed Then
            .Close
        End If
        .Open "发货记录", cnADOA, adOpenDynamic, adLockOptimistic, adCmdTable
    End With
    With rsADOB1
        If .State <> adStateClosed Then
            .Close
        End If
        .Open "发货记录", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
    End With
    With rsADOB2
        If .State <> adStateClosed Then
            .Close
        End If
        .Open "发货明细记录", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
    End With
    
    Do Until rsADOA1.EOF
        If g_cnADO.Execute("select count(*) from 发货记录 where GUID = '" & rsADOA1!GUID & "'", , adCmdText).Fields(0).Value = 0 Then
            rsADOB1.AddNew
            For i = 0 To rsADOA1.Fields.Count
                If UCase(rsADOA1.Fields(i).Name) <> "RECORDNO" Then
                    rsADOB1.Fields(rsADOA1.Fields(i).Name).Value = rsADOA1.Fields(i).Value
                End If
            Next i
            
            With rsADOA2
                If .State <> adStateClosed Then
                    .Close
                End If
                .Open "select * from 发货明细记录 where RecordNo = " & rsADOA1!RecordNo, cnADOA, adOpenDynamic, adLockOptimistic, adCmdText
            End With
            
            Do Until rsADOA2.EOF
                rsADOB2.AddNew
                If UCase(rsADOA2.Fields(i).Name) = "主表记录号" Then
                    rsADOB1.Fields(rsADOA1.Fields(i).Name).Value = rsADOB1!RecordNo
                ElseIf UCase(rsADOA2.Fields(i).Name) <> "RECORDNO" Then
                    rsADOB1.Fields(rsADOA1.Fields(i).Name).Value = rsADOA1.Fields(i).Value
                End If
                rsADOB2.Update
                rsADOA2.MoveNext
            Loop
            
            rsADOB1.Update
        End If
        rsADOA1.MoveNext
    Loop
    
    SynchronizeDatabase = True
    
PROC_EXIT:
    If rsADOA1.State <> adStateClosed Then
        rsADOA1.Close
    End If
    If rsADOA2.State <> adStateClosed Then
        rsADOA2.Close
    End If
    If rsADOB1.State <> adStateClosed Then
        rsADOB1.Close
    End If
    If rsADOB2.State <> adStateClosed Then
        rsADOB2.Close
    End If
    If cnADOA.State <> adStateClosed Then
        cnADOA.Close
    End If
    Set rsADOA1 = Nothing
    Set rsADOA2 = Nothing
    Set rsADOB1 = Nothing
    Set rsADOB2 = Nothing
    Set cnADOA = Nothing
    Exit Function
    
PROC_ERR:
    SynchronizeDatabase = False
    MsgBox "同步数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 检验注册码是否正确
Public Function ValidRegisterCode(strUserName As String, strCompany As String, strProductSN As String, strDiskID As String, strRegisterCode As String) As Boolean
    
    Dim strSN As String
    
    strSN = EncryptIt(strDiskID)
    If strRegisterCode <> "" And strRegisterCode = EncodeN(EncryptIt(strUserName & strCompany & strProductSN & strSN)) Then
        ValidRegisterCode = True
    Else
        ValidRegisterCode = False
    End If
    
End Function

' 使用权校验，检验是否有权使用本软件，以及用户是否正式用户
Public Function VerifyAuthority() As Boolean
    
    Dim B64 As New CBase64
    
    On Error GoTo PROC_ERR
    
    B64.EncryptKey = gc_B64EncryptKey
    B64.FillCharacter = gc_B64FillCharacter
    
    With g_cnADO.Execute("select * from Master", , adCmdText)
        If Not (.BOF And .EOF) Then
            g_UserName = !UserName
            g_Company = !Company
            g_ProductSN = !ProductSN
            g_DiskID = GetDiskID
            g_RegisterCode = Trim(GetSet("RegisterCode", ""))
            g_Code = B64.Decrypt(GetSet("Code", ""))
        End If
    End With
    
    If g_UserName = "" Or g_Company = "" Or g_ProductSN = "" Then
        g_DemoVersion = True
    Else
        If InStr(1, g_Code, " == " & g_UserName & " == ", vbTextCompare) = 0 Then
            g_DemoVersion = True
        ElseIf InStr(1, g_Code, " == " & g_Company & " == ", vbTextCompare) = 0 Then
            g_DemoVersion = True
        ElseIf InStr(1, g_Code, " == " & g_ProductSN & " == ", vbTextCompare) = 0 Then
            g_DemoVersion = True
        ElseIf InStr(1, g_Code, " == " & g_DiskID & " == ", vbTextCompare) = 0 Then
            g_DemoVersion = True
        Else
            g_DemoVersion = False
        End If
    End If
    
'    If g_DemoVersion Then
'        If InStr(1, g_Code, " == Register Date == ", vbTextCompare) > 0 Then
'            ' 有此标记说明已经注册过，但硬盘序号变了
'            MsgBox "欢迎使用" & App.Title & "！本软件在您的计算机上没有正确注册，您将不能使用打印功能。如果您更换了计算机硬件，请联系软件开发商以重新注册。", vbInformation, msgPrompt
'        Else
'            MsgBox "欢迎使用" & App.Title & "！您尚未注册本软件，将不能使用打印功能。", vbInformation, msgPrompt
'        End If
'    End If
    
    VerifyAuthority = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox "发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

