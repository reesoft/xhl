VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form mfrmRegions 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "维护地区"
   ClientHeight    =   4305
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7155
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4305
   ScaleWidth      =   7155
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Caption         =   "取消(&C)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5700
      TabIndex        =   5
      Top             =   3780
      Width           =   1335
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "保存(&S)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5700
      TabIndex        =   4
      Top             =   3300
      Width           =   1335
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "删除(&D)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5700
      TabIndex        =   3
      Top             =   2820
      Width           =   1335
   End
   Begin VB.CommandButton cmdModify 
      Caption         =   "修改(&M)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5700
      TabIndex        =   2
      Top             =   2340
      Width           =   1335
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "添加(&A)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5700
      TabIndex        =   1
      Top             =   1860
      Width           =   1335
   End
   Begin MSComctlLib.ListView lvw 
      Height          =   4095
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   5475
      _ExtentX        =   9657
      _ExtentY        =   7223
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "地区名称"
         Object.Width           =   8996
      EndProperty
   End
End
Attribute VB_Name = "mfrmRegions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public blnAutoAdd As Boolean
Public strNewItemID As String

Dim blnAdding As Boolean
Dim blnEditing As Boolean
Dim intEditingIndex As Integer

' 放弃保存
Private Sub CancelSave()
    
    If blnAdding Then
        lvw.ListItems.Remove lvw.SelectedItem.Index
    End If
    
    blnAdding = False
    blnEditing = False
    intEditingIndex = 0
    
    Call DoLock
    
End Sub

Public Sub cmdAdd_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    Set lvw.SelectedItem = lvw.ListItems.Add
    DoEvents
    
    lvw.SelectedItem.EnsureVisible
    intEditingIndex = lvw.SelectedItem.Index
    
    blnAdding = True
    
    Call DoLock
    
    DoEvents
    
    lvw.SetFocus
    lvw.StartLabelEdit

End Sub

Private Sub cmdCancel_Click()

    Call CancelSave
    
End Sub

Private Sub cmdDelete_Click()

    Call DeleteRecord
    
End Sub

Private Sub cmdModify_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    blnEditing = True
    intEditingIndex = lvw.SelectedItem.Index
    
    Call DoLock
    
    lvw.SetFocus
    lvw.StartLabelEdit
    
End Sub

' 删除记录
Private Sub DeleteRecord()
    
    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    If MsgBox("确实要删除该记录吗？", vbInformation + vbYesNo, msgPrompt) = vbNo Then
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    g_cnADO.Execute "delete from 地区 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText
    
    If strNewItemID = lvw.SelectedItem.Text Then
        strNewItemID = ""
    End If
    
    Call RemoveSelectedRow(lvw)
    
    lvw.SetFocus
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox "删除记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 显示列表
Private Sub DisplayList()
    
    Dim itemTemp As ListItem
    
    lvw.ListItems.Clear
    
    With g_cnADO.Execute("select * from 地区 order by 名称", , adCmdText)
        Do Until .EOF
            Set itemTemp = lvw.ListItems.Add(, !GUID, !名称 & "")
            .MoveNext
        Loop
    End With
    
    If lvw.ListItems.Count > 0 Then
        Set lvw.SelectedItem = lvw.ListItems.Item(1)
    End If
    
End Sub

' 锁定/解锁控件
Private Sub DoLock()
    
    If blnAdding Or blnEditing Then
        cmdAdd.Enabled = False
        cmdModify.Enabled = False
        cmdDelete.Enabled = False
        cmdSave.Enabled = True
        cmdCancel.Enabled = True
    Else
        cmdAdd.Enabled = True
        cmdModify.Enabled = True
        cmdDelete.Enabled = True
        cmdSave.Enabled = False
        cmdCancel.Enabled = False
    End If
    
End Sub

Private Sub cmdSave_Click()

    If blnAdding Then
        Call SaveAdd
    ElseIf blnEditing Then
        Call SaveEdit
    End If
    
End Sub

Private Sub Form_Activate()
    
    If blnAutoAdd Then
        Call cmdAdd_Click
        blnAutoAdd = False
    End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
    
        If TypeName(ActiveControl) = "TextBox" _
            Or TypeName(ActiveControl) = "ComboBox" _
            Or TypeName(ActiveControl) = "CheckBox" _
            Or TypeName(ActiveControl) = "DTPicker" _
            Or TypeName(ActiveControl) = "lc" Then
            
            On Error Resume Next
            
            SendKeys "{TAB}"
            
        End If
        
    ElseIf KeyCode = vbKeyEscape Then
    
        Unload Me
        
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
    Call DisplayList
    Call DoLock
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Call CancelSave
    
End Sub

' 初始化必要数据
Private Sub InitData()

End Sub

' 保存添加的内容
Private Sub SaveAdd()

    If intEditingIndex = 0 Or intEditingIndex > lvw.ListItems.Count Then
        Exit Sub
    End If
    
    Dim strRegionName As String
    
    strRegionName = Trim(lvw.ListItems(intEditingIndex).Text)
    
    If strRegionName = "" Or StrLen(strRegionName) > 50 Then
        MsgBox "地区名称应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        Call OnEditFail
        GoTo PROC_EXIT
    End If
    
    On Error GoTo PROC_ERR
    
    With g_cnADO.Execute("select * from 地区 where 名称 = '" & Replace(strRegionName, "'", "''") & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            MsgBox "该名称已经存在，请输入另一个名称。", vbInformation, msgPrompt
            Call OnEditFail
            GoTo PROC_EXIT
        End If
    End With
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "地区", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
        
        .AddNew
        
        !编号 = strRegionName
        !名称 = strRegionName
        
        Call FillNewRecordData(rsADO)
        
        lvw.SelectedItem.Key = !GUID
        
        .Update
        
    End With
    
    strNewItemID = strRegionName
    
    blnAdding = False
    Call DoLock
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    MsgBox "添加记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 保存修改的内容
Private Sub SaveEdit()

    If intEditingIndex = 0 Or intEditingIndex > lvw.ListItems.Count Then
        Exit Sub
    End If
    
    Dim strRegionName As String
    
    strRegionName = Trim(lvw.ListItems(intEditingIndex).Text)
    
    If strRegionName = "" Or StrLen(strRegionName) > 50 Then
        MsgBox "地区名称应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        Call OnEditFail
        GoTo PROC_EXIT
    End If
    
    On Error GoTo PROC_ERR
    
    With g_cnADO.Execute("select * from 地区 where 名称 = '" & Replace(strRegionName, "'", "''") & "' and GUID <> '" & lvw.ListItems(intEditingIndex).Key & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            MsgBox "该名称已经存在，请输入另一个名称。", vbInformation, msgPrompt
            Call OnEditFail
            GoTo PROC_EXIT
        End If
    End With
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 地区 where GUID = '" & lvw.ListItems(intEditingIndex).Key & "'", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If .BOF Or .EOF Then
            GoTo PROC_EXIT
        End If
        
        !编号 = strRegionName
        !名称 = strRegionName
        
        Call FillUpdateRecordData(rsADO)
        
        .Update
        
    End With
    
    blnEditing = False
    Call DoLock
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    If Err.Number = 3022 Then
        rsADO.CancelUpdate
        MsgBox "名称重复，无法修改。", vbCritical, msgErrRuntime
    Else
        MsgBox "修改记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    End If
    GoTo PROC_EXIT
    
End Sub

Private Sub lvw_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    lvw.SortKey = ColumnHeader.Index - 1
    
End Sub

Private Sub lvw_ItemClick(ByVal Item As MSComctlLib.ListItem)

    If blnAdding Then
        Call SaveAdd
    ElseIf blnEditing Then
        Call SaveEdit
    End If
    
End Sub

Private Sub OnEditFail()

    Set lvw.SelectedItem = lvw.ListItems(intEditingIndex)
    DoEvents
    lvw.SetFocus
    lvw.StartLabelEdit

End Sub
