Attribute VB_Name = "DatabaseUtilities"
Option Explicit

' 检查数据库文件是否有效
Public Function CheckDatabase(strDatabase As String) As Boolean

    On Error GoTo PROC_ERR
    
    Dim cnADO As New ADODB.Connection
    
    cnADO.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source= " & strDatabase & ";Persist Security Info=False"
    
    With cnADO.Execute("select * from Master")
    
        If .BOF And .EOF Then
            GoTo PROC_EXIT
        End If
        
        'If Val(!Version) < Val(App.Major & "." & App.Minor) Or Val(!DBVersion) < 3 Then
        If Val(!DBVersion) < 3 Then
            GoTo PROC_EXIT
        End If
        
    End With
    
    CheckDatabase = True
    
PROC_EXIT:
    If cnADO.State <> adStateClosed Then
        cnADO.Close
    End If
    Set cnADO = Nothing
    Exit Function
    
PROC_ERR:
    'MsgBox "打开数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 归档指定时间点之前的数据
Public Function MoveToHistory(dateBefore As Date, strHistoryDatabase As String, blnCopyProcedureSheet As Boolean, blnCopySale As Boolean, blnCopyIncome As Boolean) As Boolean
    
    On Error GoTo PROC_ERR
    
    Dim cnADODest As New ADODB.Connection
    Dim rsADOSrc As New ADODB.Recordset
    Dim rsADODest As New ADODB.Recordset
    Dim blnSuccess As Boolean
    
    cnADODest.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source= " & strHistoryDatabase & ";Persist Security Info=False"
    
    rsADOSrc.Open "地区", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
    rsADODest.Open "地区", cnADODest, adOpenStatic, adLockOptimistic, adCmdTable
    
    blnSuccess = CopyTable(rsADOSrc, rsADODest)
    
    rsADOSrc.Close
    rsADODest.Close
    
    rsADOSrc.Open "客户", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
    rsADODest.Open "客户", cnADODest, adOpenStatic, adLockOptimistic, adCmdTable
    
    blnSuccess = CopyTable(rsADOSrc, rsADODest)
    
    rsADOSrc.Close
    rsADODest.Close
    
    rsADOSrc.Open "产品", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
    rsADODest.Open "产品", cnADODest, adOpenStatic, adLockOptimistic, adCmdTable
    
    blnSuccess = CopyTable(rsADOSrc, rsADODest)
    
    rsADOSrc.Close
    rsADODest.Close
    
    rsADOSrc.Open "工序", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
    rsADODest.Open "工序", cnADODest, adOpenStatic, adLockOptimistic, adCmdTable
    
    blnSuccess = CopyTable(rsADOSrc, rsADODest)
    
    rsADOSrc.Close
    rsADODest.Close
    
    rsADOSrc.Open "职工", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
    rsADODest.Open "职工", cnADODest, adOpenStatic, adLockOptimistic, adCmdTable
    
    blnSuccess = CopyTable(rsADOSrc, rsADODest)
    
    rsADOSrc.Close
    rsADODest.Close
    
    If blnCopyProcedureSheet Then
    
        rsADOSrc.Open "select * from 工序单主表 where 日期 < #" & Format(dateBefore, gc_DateFormat) & "#", g_cnADO, adOpenStatic, adLockOptimistic, adCmdText
        rsADODest.Open "工序单主表", cnADODest, adOpenStatic, adLockOptimistic, adCmdTable
        
        blnSuccess = CopyProcedureSheets(rsADOSrc, rsADODest)
        
        If blnSuccess And Not (rsADOSrc.BOF And rsADOSrc.EOF) Then
            rsADOSrc.MoveFirst
            Do Until rsADOSrc.EOF
                g_cnADO.Execute "delete from 工序单从表 where 主表编号 = " & rsADOSrc!编号, , adCmdText
                rsADOSrc.MoveNext
            Loop
        End If
        
        rsADOSrc.Close
        rsADODest.Close
        
        If blnSuccess Then
            g_cnADO.Execute "delete from 工序单主表 where 日期 < #" & Format(dateBefore, gc_DateFormat) & "#", , adCmdText
        End If
        
    End If
    
    If blnCopySale Then
    
        rsADOSrc.Open "select * from 发货记录 where 日期 < #" & Format(dateBefore, gc_DateFormat) & "#", g_cnADO, adOpenStatic, adLockOptimistic, adCmdText
        rsADODest.Open "发货记录", cnADODest, adOpenStatic, adLockOptimistic, adCmdTable
        
        blnSuccess = CopySaleSheets(rsADOSrc, rsADODest)
        
        If blnSuccess And Not (rsADOSrc.BOF And rsADOSrc.EOF) Then
            rsADOSrc.MoveFirst
            Do Until rsADOSrc.EOF
                g_cnADO.Execute "delete from 发货明细记录 where 记录号 = " & rsADOSrc!记录号, , adCmdText
                rsADOSrc.MoveNext
            Loop
        End If
        
        rsADOSrc.Close
        rsADODest.Close
        
        If blnSuccess Then
            g_cnADO.Execute "delete from 发货记录 where 日期 < #" & Format(dateBefore, gc_DateFormat) & "#", , adCmdText
        End If
        
    End If
    
    If blnCopyIncome Then
    
        rsADOSrc.Open "select * from 收款记录 where 日期 < #" & Format(dateBefore, gc_DateFormat) & "#", g_cnADO, adOpenStatic, adLockOptimistic, adCmdText
        rsADODest.Open "收款记录", cnADODest, adOpenStatic, adLockOptimistic, adCmdTable
        
        blnSuccess = CopyTable(rsADOSrc, rsADODest)
        
        rsADOSrc.Close
        rsADODest.Close
        
        If blnSuccess Then
            g_cnADO.Execute "delete from 收款记录 where 日期 < #" & Format(dateBefore, gc_DateFormat) & "#", , adCmdText
        End If
        
    End If
    
    MoveToHistory = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "复制数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 导入指定时间段的数据
Public Function ImportData(dateFrom As Date, dateTo As Date, cnADOSrc As ADODB.Connection, blnCopyProcedureSheet As Boolean, blnCopySale As Boolean, blnCopyIncome As Boolean) As Boolean
    
    On Error GoTo PROC_ERR
    
    Dim rsADOSrc As New ADODB.Recordset
    Dim rsADODest As New ADODB.Recordset
    Dim blnSuccess As Boolean
    
    rsADOSrc.Open "地区", cnADOSrc, adOpenStatic, adLockOptimistic, adCmdTable
    rsADODest.Open "地区", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
    
    blnSuccess = CopyTable(rsADOSrc, rsADODest)
    
    rsADOSrc.Close
    rsADODest.Close
    
    rsADOSrc.Open "客户", cnADOSrc, adOpenStatic, adLockOptimistic, adCmdTable
    rsADODest.Open "客户", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
    
    blnSuccess = CopyTable(rsADOSrc, rsADODest)
    
    rsADOSrc.Close
    rsADODest.Close
    
    rsADOSrc.Open "产品", cnADOSrc, adOpenStatic, adLockOptimistic, adCmdTable
    rsADODest.Open "产品", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
    
    blnSuccess = CopyTable(rsADOSrc, rsADODest)
    
    rsADOSrc.Close
    rsADODest.Close
    
    rsADOSrc.Open "工序", cnADOSrc, adOpenStatic, adLockOptimistic, adCmdTable
    rsADODest.Open "工序", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
    
    blnSuccess = CopyTable(rsADOSrc, rsADODest)
    
    rsADOSrc.Close
    rsADODest.Close
    
    rsADOSrc.Open "职工", cnADOSrc, adOpenStatic, adLockOptimistic, adCmdTable
    rsADODest.Open "职工", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
    
    blnSuccess = CopyTable(rsADOSrc, rsADODest)
    
    rsADOSrc.Close
    rsADODest.Close
    
    If blnCopyProcedureSheet Then
    
        rsADOSrc.Open "select * from 工序单主表 where 日期 between #" & Format(dateFrom, gc_DateFormat) & "# and #" & Format(dateTo, gc_DateFormat) & "#", cnADOSrc, adOpenStatic, adLockOptimistic, adCmdText
        rsADODest.Open "工序单主表", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
        
        blnSuccess = CopyProcedureSheets(rsADOSrc, rsADODest)
        
        rsADOSrc.Close
        rsADODest.Close
        
    End If
    
    If blnCopySale Then
    
        rsADOSrc.Open "select * from 发货记录 where 日期 between #" & Format(dateFrom, gc_DateFormat) & "# and #" & Format(dateTo, gc_DateFormat) & "#", cnADOSrc, adOpenStatic, adLockOptimistic, adCmdText
        rsADODest.Open "发货记录", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
        
        blnSuccess = CopySaleSheets(rsADOSrc, rsADODest)
        
        rsADOSrc.Close
        rsADODest.Close
        
    End If
    
    If blnCopyIncome Then
    
        rsADOSrc.Open "select * from 收款记录 where 日期 between #" & Format(dateFrom, gc_DateFormat) & "# and #" & Format(dateTo, gc_DateFormat) & "#", cnADOSrc, adOpenStatic, adLockOptimistic, adCmdText
        rsADODest.Open "收款记录", g_cnADO, adOpenStatic, adLockOptimistic, adCmdTable
        
        blnSuccess = CopyTable(rsADOSrc, rsADODest)
        
        rsADOSrc.Close
        rsADODest.Close
        
    End If
    
    ImportData = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "复制数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 合并名称相同、编号不同的产品
Public Function CombineProductsWithSameName()

    Dim strLastID As String
    Dim strLastName As String
    
    With g_cnADO.Execute("select 编号, 名称 from 产品 order by 名称, LastUpdatedTime", , adCmdText)
    
        Do Until .EOF
        
            If strLastName = "" Or strLastName <> !名称 Then
            
                strLastID = !编号
                strLastName = !名称
                
            ElseIf strLastName = !名称 And strLastID <> !编号 Then
            
                Call CombineProduct(!编号, strLastID)
                
            End If
            
            .MoveNext
            
        Loop
        
    End With
    
End Function

' 合并两个产品
Public Function CombineProduct(strToCombineProductID As String, strTargetProductID As String) As Boolean

    If Not ChangeProductID(strToCombineProductID, strTargetProductID) Then
        Exit Function
    End If
    
    On Error GoTo PROC_ERR
    
    g_cnADO.Execute "delete from 工序 where 产品编号 = '" & strToCombineProductID & "'", , adCmdText
    g_cnADO.Execute "delete from 产品 where 编号 = '" & strToCombineProductID & "'", , adCmdText
    
    CombineProduct = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "合并产品的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 修改生产单、销售记录中的产品编号
Public Function ChangeProductID(strOldProductID As String, strNewProductID As String) As Boolean

    On Error GoTo PROC_ERR
    
    If g_cnADO.Execute("select * from 工序 where 产品编号 = '" & strNewProductID & "'").EOF Then
        g_cnADO.Execute "update 工序 set 产品编号 = '" & strNewProductID & "' where 产品编号 = '" & strOldProductID & "'", , adCmdText
    End If
    
    g_cnADO.Execute "update 工序单主表 set 产品编号 = '" & strNewProductID & "' where 产品编号 = '" & strOldProductID & "'", , adCmdText
    
    g_cnADO.Execute "update 发货明细记录 set 产品编号 = '" & strNewProductID & "' where 产品编号 = '" & strOldProductID & "'", , adCmdText
    
    ChangeProductID = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "修改产品编号的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 修改产品的编号为与名称相同
Public Function ChangeProductsIDToName()

    ' 工序、发货明细记录表的字段会被级联修改
    Call g_cnADO.Execute("update 产品 set 编号 = 名称 where 编号 <> 名称", , adCmdText)
    
End Function

' 复制所有工序单
Public Function CopyProcedureSheets(rsADOSrc As ADODB.Recordset, rsADODest As ADODB.Recordset) As Boolean

    On Error GoTo PROC_ERR
    
    Dim blnSkip As Boolean
    
    Do Until rsADOSrc.EOF
        
        blnSkip = False
        
        If Not (rsADODest.BOF And rsADODest.EOF) Then
            rsADODest.MoveFirst
            rsADODest.Find "GUID = '" & rsADOSrc!GUID & "'"
        End If
        
        If rsADODest.EOF Then
            rsADODest.AddNew
        ElseIf rsADODest!LastUpdatedTime >= rsADOSrc!LastUpdatedTime Then
            blnSkip = True
        End If
        
        If Not blnSkip Then
            If Not CopyProcedureSheet(rsADOSrc, rsADODest) Then
                rsADODest.MoveFirst
            End If
        End If
        
        rsADOSrc.MoveNext
        
    Loop
    
    CopyProcedureSheets = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "复制数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 复制单张工序单
Public Function CopyProcedureSheet(rsADOSrc As ADODB.Recordset, rsADODest As ADODB.Recordset) As Boolean

    On Error GoTo PROC_ERR
    
    Dim rsADODetailDest As New ADODB.Recordset
    
    rsADODetailDest.Open "select * from 工序单从表 where 主单编号 = " & rsADODest!编号, rsADODest.ActiveConnection, adOpenStatic, adLockOptimistic, adCmdText
    
    Do Until rsADODetailDest.EOF
        rsADODetailDest.Delete
        rsADODetailDest.MoveNext
    Loop
    
    Dim i As Long
    
    ' 复制主记录
    For i = 0 To rsADOSrc.Fields.Count - 1
        If rsADOSrc.Fields(i).Name <> "编号" Then
            rsADODest.Fields(rsADOSrc.Fields(i).Name).Value = rsADOSrc.Fields(i).Value
        End If
    Next i
    
    rsADODest!LastUpdatedTime = CDate("1900-01-01")
    
    rsADODest.Update
    
    Dim rsADODetailSrc As New ADODB.Recordset
    
    ' 复制明细记录
    rsADODetailSrc.Open "select * from 工序单从表 where 主单编号 = " & rsADOSrc!编号, g_cnADO, adOpenStatic, adLockOptimistic
    
    Do Until rsADODetailSrc.EOF
        
        rsADODetailDest.AddNew
        
        rsADODetailDest!主单编号 = rsADODest!编号
        
        For i = 0 To rsADODetailSrc.Fields.Count - 1
            If rsADODetailSrc.Fields(i).Name <> "主单编号" Then
                rsADODetailDest.Fields(rsADODetailSrc.Fields(i).Name).Value = rsADODetailSrc.Fields(i).Value
            End If
        Next i
        
        rsADODetailDest.Update
        
        rsADODetailSrc.MoveNext
        
    Loop
    
    rsADODest!LastUpdatedTime = rsADOSrc!LastUpdatedTime
    
    rsADODest.Update
    
    CopyProcedureSheet = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "复制数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    rsADODetailDest.CancelUpdate
    rsADODest.CancelUpdate
    GoTo PROC_EXIT
    
End Function

' 复制所有销售记录
Public Function CopySaleSheets(rsADOSrc As ADODB.Recordset, rsADODest As ADODB.Recordset) As Boolean

    On Error GoTo PROC_ERR
    
    Dim blnSkip As Boolean
    
    Do Until rsADOSrc.EOF
        
        blnSkip = False
        
        If Not (rsADODest.BOF And rsADODest.EOF) Then
            rsADODest.MoveFirst
            rsADODest.Find "GUID = '" & rsADOSrc!GUID & "'"
        End If
        
        If rsADODest.EOF Then
            rsADODest.AddNew
        ElseIf rsADODest!LastUpdatedTime >= rsADOSrc!LastUpdatedTime Then
            blnSkip = True
        End If
        
        If Not blnSkip Then
            If Not CopySaleSheet(rsADOSrc, rsADODest) Then
                rsADODest.MoveFirst
            End If
        End If
        
        rsADOSrc.MoveNext
        
    Loop
    
    CopySaleSheets = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "复制数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 复制单张销售记录
Public Function CopySaleSheet(rsADOSrc As ADODB.Recordset, rsADODest As ADODB.Recordset) As Boolean

    On Error GoTo PROC_ERR
    
    Dim rsADODetailDest As New ADODB.Recordset
    
    rsADODetailDest.Open "select * from 发货明细记录 where 记录号 = " & rsADODest!记录号, rsADODest.ActiveConnection, adOpenStatic, adLockOptimistic, adCmdText
    
    Do Until rsADODetailDest.EOF
        rsADODetailDest.Delete
        rsADODetailDest.MoveNext
    Loop
    
    Dim i As Long
    
    ' 复制主记录
    For i = 0 To rsADOSrc.Fields.Count - 1
        If rsADOSrc.Fields(i).Name <> "记录号" Then
            rsADODest.Fields(rsADOSrc.Fields(i).Name).Value = rsADOSrc.Fields(i).Value
        End If
    Next i
    
    rsADODest!LastUpdatedTime = CDate("1900-01-01")
    
    rsADODest.Update
    
    Dim rsADODetailSrc As New ADODB.Recordset
    
    ' 复制明细记录
    rsADODetailSrc.Open "select * from 发货明细记录 where 记录号 = " & rsADOSrc!记录号, rsADOSrc.ActiveConnection, adOpenStatic, adLockOptimistic
    
    Do Until rsADODetailSrc.EOF
        
        rsADODetailDest.AddNew
        
        rsADODetailDest!记录号 = rsADODest!记录号
        
        For i = 0 To rsADODetailSrc.Fields.Count - 1
            If rsADODetailSrc.Fields(i).Name <> "记录号" Then
                rsADODetailDest.Fields(rsADODetailSrc.Fields(i).Name).Value = rsADODetailSrc.Fields(i).Value
            End If
        Next i
        
        rsADODetailDest.Update
        
        rsADODetailSrc.MoveNext
        
    Loop
    
    rsADODest!LastUpdatedTime = rsADOSrc!LastUpdatedTime
    
    rsADODest.Update
    
    CopySaleSheet = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "复制数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    rsADODetailDest.CancelUpdate
    rsADODest.CancelUpdate
    GoTo PROC_EXIT
    
End Function

' 复制整个数据表
Public Function CopyTable(rsADOSrc As ADODB.Recordset, rsADODest As ADODB.Recordset) As Boolean

    On Error GoTo PROC_ERR
    
    Dim blnSkip As Boolean
    
    Do Until rsADOSrc.EOF
        
        blnSkip = False
        
        If Not (rsADODest.BOF And rsADODest.EOF) Then
            rsADODest.MoveFirst
            rsADODest.Find "GUID = '" & rsADOSrc!GUID & "'"
        End If
        
        If rsADODest.EOF Then
            rsADODest.AddNew
        ElseIf rsADODest!LastUpdatedTime >= rsADOSrc!LastUpdatedTime Then
            blnSkip = True
        End If
        
        If Not blnSkip Then
            If Not CopyRow(rsADOSrc, rsADODest) Then
                rsADODest.MoveFirst
            End If
        End If
        
        rsADOSrc.MoveNext
        
    Loop
    
    CopyTable = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "复制数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Function

' 复制单条记录
Public Function CopyRow(rsADOSrc As ADODB.Recordset, rsADODest As ADODB.Recordset) As Boolean

    On Error GoTo PROC_ERR
    
    Dim i As Long
    
    For i = 0 To rsADOSrc.Fields.Count - 1
        rsADODest.Fields(rsADOSrc.Fields(i).Name).Value = rsADOSrc.Fields(i).Value
    Next i
    
    Call FillBlankRecordData(rsADODest)
    
    rsADODest.Update
    
    CopyRow = True
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "复制数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    If Err.Number = 3265 Then
        Resume Next
    End If
    rsADODest.CancelUpdate
    GoTo PROC_EXIT
    
End Function

' 从指定表的指定字段取得时间范围
Public Function GetDateRange(cnADO As ADODB.Connection, strTable As String, strDateField As String, dateStart As Date, dateEnd As Date) As Boolean

    On Error GoTo PROC_ERR
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select " & strDateField & " from " & strTable & " order by " & strDateField, cnADO, adOpenStatic, adLockReadOnly
    
        If Not (.BOF And .EOF) Then
        
            If dateStart > .Fields(strDateField).Value Then
                dateStart = .Fields(strDateField).Value
            End If
            
            .MoveLast
            
            If dateEnd < .Fields(strDateField).Value Then
                dateEnd = .Fields(strDateField).Value
            End If
            
            GetDateRange = True
            
        End If
        
    End With
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
'    MsgBox "查询数据的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT

End Function
