VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form mfrmStaff 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "职工管理"
   ClientHeight    =   7185
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8295
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7185
   ScaleWidth      =   8295
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkValid 
      Caption         =   "在职"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3480
      TabIndex        =   24
      Top             =   660
      Width           =   915
   End
   Begin VB.TextBox txtMemo 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   1200
      MultiLine       =   -1  'True
      TabIndex        =   17
      Top             =   2520
      Width           =   5475
   End
   Begin VB.TextBox txtAddress 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1200
      TabIndex        =   15
      Top             =   2040
      Width           =   5475
   End
   Begin MSComCtl2.DTPicker dtpJoinDate 
      Height          =   315
      Left            =   1200
      TabIndex        =   7
      Top             =   600
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   54132737
      CurrentDate     =   39529
   End
   Begin VB.ComboBox cboGender 
      Height          =   330
      ItemData        =   "mStaff.frx":0000
      Left            =   3720
      List            =   "mStaff.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   120
      Width           =   855
   End
   Begin VB.TextBox txtPhoneNumber 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1200
      TabIndex        =   13
      Top             =   1560
      Width           =   5475
   End
   Begin VB.TextBox txtSkills 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1200
      TabIndex        =   11
      Top             =   1080
      Width           =   5475
   End
   Begin VB.TextBox txtAge 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5400
      MaxLength       =   5
      TabIndex        =   5
      Top             =   120
      Width           =   1275
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "取消(&C)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6840
      TabIndex        =   22
      Top             =   2100
      Width           =   1335
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "保存(&S)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6840
      TabIndex        =   21
      Top             =   1620
      Width           =   1335
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "删除(&D)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6840
      TabIndex        =   20
      Top             =   1140
      Width           =   1335
   End
   Begin VB.CommandButton cmdModify 
      Caption         =   "修改(&M)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6840
      TabIndex        =   19
      Top             =   660
      Width           =   1335
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "添加(&A)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6840
      TabIndex        =   18
      Top             =   180
      Width           =   1335
   End
   Begin VB.TextBox txtID 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5400
      TabIndex        =   9
      Top             =   600
      Width           =   1275
   End
   Begin VB.TextBox txtName 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1200
      TabIndex        =   1
      Top             =   120
      Width           =   1635
   End
   Begin MSComctlLib.ListView lvw 
      Height          =   3735
      Left            =   60
      TabIndex        =   23
      Top             =   3360
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   6588
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   10
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "工号"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "姓名"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   2
         Text            =   "性别"
         Object.Width           =   1323
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Text            =   "年龄"
         Object.Width           =   1323
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   4
         Text            =   "入厂日期"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   5
         Text            =   "是否在职"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "技能特长"
         Object.Width           =   3175
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "联系电话"
         Object.Width           =   2999
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "住址"
         Object.Width           =   4762
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "备注"
         Object.Width           =   4762
      EndProperty
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "备注"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   8
      Left            =   600
      TabIndex        =   16
      Top             =   2580
      Width           =   480
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "住址"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   7
      Left            =   600
      TabIndex        =   14
      Top             =   2100
      Width           =   480
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "联系电话"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   6
      Left            =   180
      TabIndex        =   12
      Top             =   1620
      Width           =   960
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "入厂日期"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   5
      Left            =   180
      TabIndex        =   6
      Top             =   660
      Width           =   960
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "技能特长"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   4
      Left            =   180
      TabIndex        =   10
      Top             =   1140
      Width           =   960
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "年龄"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   3
      Left            =   4800
      TabIndex        =   4
      Top             =   180
      Width           =   480
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "性别"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   3120
      TabIndex        =   2
      Top             =   180
      Width           =   480
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "姓名"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   600
      TabIndex        =   0
      Top             =   180
      Width           =   480
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "工号"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   4800
      TabIndex        =   8
      Top             =   660
      Width           =   480
   End
End
Attribute VB_Name = "mfrmStaff"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim blnAdding As Boolean
Dim blnEditing As Boolean

' 放弃保存
Private Sub CancelSave()
    
    blnAdding = False
    blnEditing = False
    
    Call DoLock
    Call DisplayRecord
    
End Sub

' 清除控件内容以免对后来的显示造成影响
Private Sub ClearDisplay()
    
    txtID.Text = ""
    txtName.Text = ""
    cboGender.ListIndex = -1
    txtAge.Text = ""
    txtSkills.Text = ""
    dtpJoinDate.Value = CDate("1970-01-01")
    chkValid.Value = vbUnchecked
    txtPhoneNumber.Text = ""
    txtAddress.Text = ""
    txtMemo.Text = ""
    
End Sub

Private Sub cmdAdd_Click()

    Dim strID As String
    
    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    blnAdding = True
    
    Call DoLock
    Call ClearDisplay
    
    With g_cnADO.Execute("select top 1 工号 from 职工 order by 工号 desc", , adCmdText)
        If Not (.BOF And .EOF) Then
            txtID.Text = IncreaseString(.Fields(0).Value)
        Else
            txtID.Text = "1001"
        End If
    End With
    
    cboGender.ListIndex = 1
    txtAge.Text = "0"
    dtpJoinDate.Value = Date
    chkValid.Value = vbChecked
    
    txtName.SetFocus
    
End Sub

Private Sub cmdCancel_Click()

    Call CancelSave
    
End Sub

Private Sub cmdDelete_Click()

    Call DeleteRecord
    
End Sub

Private Sub cmdModify_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) <> "IListItem" Then
        Exit Sub
    End If
    
    blnEditing = True
    Call DoLock
    txtName.SetFocus
    
End Sub

' 删除记录
Private Sub DeleteRecord()

    If blnAdding Or blnEditing Then Exit Sub
    
    If TypeName(lvw.SelectedItem) <> "IListItem" Then
        Exit Sub
    End If
    
    If MsgBox("确实要删除该记录吗？", vbInformation + vbYesNo, msgPrompt) = vbNo Then
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    Dim lngRecordsAffected As Long
    
    Call g_cnADO.Execute("delete from 职工 where GUID = '" & lvw.SelectedItem.Key & "'", lngRecordsAffected, adCmdText)
    
    Call RemoveSelectedRow(lvw)
    
    lvw.SetFocus
    
    Call DisplayRecord
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox "删除记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 显示记录列表
Private Sub DisplayList()
    
    Dim itemTemp As ListItem
    
    lvw.ListItems.Clear
    
    On Error GoTo PROC_ERR
    
    With g_cnADO.Execute("select * from 职工 order by 工号", , adCmdText)
        Do Until .EOF
            Set itemTemp = lvw.ListItems.Add(, !GUID, !工号 & "")
            itemTemp.SubItems(1) = !姓名 & ""
            itemTemp.SubItems(2) = IIf(!性别 = 0, "女", "男")
            itemTemp.SubItems(3) = !年龄
            itemTemp.SubItems(4) = !入厂日期 & ""
            itemTemp.SubItems(5) = IIf(!是否在职, "是", "否")
            itemTemp.SubItems(6) = !技能 & ""
            itemTemp.SubItems(7) = !联系电话 & ""
            itemTemp.SubItems(8) = !住址 & ""
            itemTemp.SubItems(9) = !备注 & ""
            .MoveNext
        Loop
    End With
    
PROC_EXIT:
    Set itemTemp = Nothing
    Exit Sub
    
PROC_ERR:
    MsgBox "显示记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbInformation, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 显示记录
Private Sub DisplayRecord()
    
    Call ClearDisplay
    
    If TypeName(lvw.SelectedItem) <> "IListItem" Then
        Exit Sub
    End If
    
    With lvw.SelectedItem
        txtID.Text = .Text
        txtName.Text = .SubItems(1)
        cboGender.ListIndex = IIf(.SubItems(2) = "女", 1, 0)
        txtAge.Text = .SubItems(3)
        dtpJoinDate.Value = CDate(.SubItems(4))
        chkValid.Value = IIf(.SubItems(5) = "是", vbChecked, vbUnchecked)
        txtSkills.Text = .SubItems(6)
        txtPhoneNumber.Text = .SubItems(7)
        txtAddress.Text = .SubItems(8)
        txtMemo.Text = .SubItems(9)
    End With
    
End Sub

' 锁定/解锁控件
Private Sub DoLock()
    
    If blnAdding Or blnEditing Then
        txtID.Locked = False
        txtName.Locked = False
        cboGender.Locked = False
        txtAge.Locked = False
        txtAddress.Locked = False
        txtSkills.Locked = False
        dtpJoinDate.Enabled = True
        chkValid.Enabled = True
        txtPhoneNumber.Locked = False
        txtAddress.Locked = False
        txtMemo.Locked = False
        cmdAdd.Enabled = False
        cmdModify.Enabled = False
        cmdDelete.Enabled = False
        cmdSave.Enabled = True
        cmdCancel.Enabled = True
    Else
        txtID.Locked = True
        txtName.Locked = True
        cboGender.Locked = True
        txtAge.Locked = True
        txtAddress.Locked = True
        txtSkills.Locked = True
        dtpJoinDate.Enabled = False
        chkValid.Enabled = False
        txtPhoneNumber.Locked = True
        txtAddress.Locked = True
        txtMemo.Locked = True
        cmdAdd.Enabled = True
        cmdModify.Enabled = True
        cmdDelete.Enabled = True
        cmdSave.Enabled = False
        cmdCancel.Enabled = False
    End If
    
End Sub

Private Sub cmdSave_Click()

    If blnAdding Then
        Call SaveAdd
    ElseIf blnEditing Then
        Call SaveEdit
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
    
        If TypeName(ActiveControl) = "TextBox" _
            Or TypeName(ActiveControl) = "ComboBox" _
            Or TypeName(ActiveControl) = "CheckBox" _
            Or TypeName(ActiveControl) = "DTPicker" _
            Or TypeName(ActiveControl) = "lc" Then
            
            On Error Resume Next
            
            SendKeys "{TAB}"
            
        End If
        
    ElseIf KeyCode = vbKeyEscape Then
    
        Unload Me
        
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
    Call DisplayList
    Call DoLock
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Call CancelSave
    
End Sub

' 初始化必要数据
Private Sub InitData()

End Sub

' 保存添加的内容
Private Sub SaveAdd()
    
    txtID.Text = Trim(txtID.Text)
    If StrLen(txtID.Text) < 4 Or StrLen(txtID.Text) > 50 Then
        MsgBox "工号应该是 4 至 50 个字符，请重新输入。", vbInformation, msgPrompt
        txtID.SetFocus
        GoTo PROC_EXIT
    End If
    txtID.Tag = Replace(txtID.Text, "'", "''")
    
    txtName.Text = Trim(txtName.Text)
    If txtName.Text = "" Or StrLen(txtName.Text) > 50 Then
        MsgBox "姓名应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        txtName.SetFocus
        GoTo PROC_EXIT
    End If
    txtName.Tag = Replace(txtName.Text, "'", "''")
    
    txtSkills.Text = Trim(txtSkills.Text)
    If StrLen(txtSkills.Text) > 250 Then
        MsgBox "技能描述不能超过 125 个汉字，请重新输入。", vbInformation, msgPrompt
        txtSkills.SetFocus
        GoTo PROC_EXIT
    End If
    
    txtPhoneNumber.Text = Trim(txtPhoneNumber.Text)
    If StrLen(txtPhoneNumber.Text) > 250 Then
        MsgBox "联系电话不能超过 125 个汉字，请重新输入。", vbInformation, msgPrompt
        txtPhoneNumber.SetFocus
        GoTo PROC_EXIT
    End If
    
    txtAddress.Text = Trim(txtAddress.Text)
    If StrLen(txtAddress.Text) > 250 Then
        MsgBox "住址不能超过 125 个汉字，请重新输入。", vbInformation, msgPrompt
        txtAddress.SetFocus
        GoTo PROC_EXIT
    End If
    
    On Error GoTo PROC_ERR
    
    With g_cnADO.Execute("select * from 职工 where 工号 = '" & txtID.Tag & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            MsgBox "该工号已经存在，请输入另一个工号。", vbInformation, msgPrompt
            txtID.SetFocus
            GoTo PROC_EXIT
        End If
    End With
    
    With g_cnADO.Execute("select * from 职工 where 姓名 = '" & txtName.Tag & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            MsgBox "该姓名已经存在。", vbInformation, msgPrompt
            txtName.SetFocus
            GoTo PROC_EXIT
        End If
    End With
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "职工", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
        
        .AddNew
        
        !工号 = txtID.Text
        !姓名 = txtName.Text
        !性别 = IIf(cboGender.Text = "女", 0, 1)
        !年龄 = Val(txtAge.Text)
        !技能 = txtSkills.Text
        !入厂日期 = dtpJoinDate.Value
        !是否在职 = (chkValid.Value = vbChecked)
        !联系电话 = txtPhoneNumber.Text
        !住址 = txtAddress.Text
        !备注 = txtMemo.Text
        
        Call FillNewRecordData(rsADO)
        
        txtID.Tag = !GUID
        
        .Update
        
    End With
    
    blnAdding = False
    Call DoLock
    
    Dim itemTemp As ListItem
    
    Set itemTemp = lvw.ListItems.Add(, txtID.Tag, txtID.Text)
    itemTemp.SubItems(1) = txtName.Text
    itemTemp.SubItems(2) = cboGender.Text
    itemTemp.SubItems(3) = Val(txtAge.Text)
    itemTemp.SubItems(4) = Format(dtpJoinDate.Value, "yyyy-mm-dd")
    itemTemp.SubItems(5) = IIf(chkValid.Value = vbChecked, "是", "否")
    itemTemp.SubItems(6) = txtSkills.Text
    itemTemp.SubItems(7) = txtPhoneNumber.Text
    itemTemp.SubItems(8) = txtAddress.Text
    itemTemp.SubItems(9) = txtMemo.Text
    Set lvw.SelectedItem = itemTemp
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    MsgBox "添加记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 保存修改的内容
Private Sub SaveEdit()
    
    If TypeName(lvw.SelectedItem) <> "IListItem" Then
        MsgBox "发生意外错误，无法修改记录。", vbExclamation, msgErrRuntime
        Exit Sub
    End If
    
    txtID.Text = Trim(txtID.Text)
    If StrLen(txtID.Text) < 4 Or StrLen(txtID.Text) > 50 Then
        MsgBox "工号应该是 4 至 50 个字符，请重新输入。", vbInformation, msgPrompt
        txtID.SetFocus
        GoTo PROC_EXIT
    End If
    txtID.Tag = Replace(txtID.Text, "'", "''")
    
    txtName.Text = Trim(txtName.Text)
    If txtName.Text = "" Or StrLen(txtName.Text) > 50 Then
        MsgBox "姓名应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        txtName.SetFocus
        GoTo PROC_EXIT
    End If
    txtName.Tag = Replace(txtName.Text, "'", "''")
    
    txtSkills.Text = Trim(txtSkills.Text)
    If StrLen(txtSkills.Text) > 250 Then
        MsgBox "技能描述不能超过 125 个汉字，请重新输入。", vbInformation, msgPrompt
        txtSkills.SetFocus
        GoTo PROC_EXIT
    End If
    
    txtPhoneNumber.Text = Trim(txtPhoneNumber.Text)
    If StrLen(txtPhoneNumber.Text) > 250 Then
        MsgBox "联系电话不能超过 125 个汉字，请重新输入。", vbInformation, msgPrompt
        txtPhoneNumber.SetFocus
        GoTo PROC_EXIT
    End If
    
    txtAddress.Text = Trim(txtAddress.Text)
    If StrLen(txtAddress.Text) > 250 Then
        MsgBox "住址不能超过 125 个汉字，请重新输入。", vbInformation, msgPrompt
        txtAddress.SetFocus
        GoTo PROC_EXIT
    End If
    
    On Error GoTo PROC_ERR
    
    If txtID.Text <> lvw.SelectedItem.Text Then
        With g_cnADO.Execute("select * from 职工 where 工号 = '" & txtID.Tag & "'", , adCmdText)
            If Not (.BOF And .EOF) Then
                MsgBox "该工号已经存在，请输入另一个工号。", vbInformation, msgPrompt
                txtID.SetFocus
                GoTo PROC_EXIT
            End If
        End With
    End If
    
    If txtName.Text <> lvw.SelectedItem.SubItems(1) Then
        With g_cnADO.Execute("select * from 职工 where 姓名 = '" & txtName.Tag & "'", , adCmdText)
            If Not (.BOF And .EOF) Then
                MsgBox "该姓名已经存在。", vbInformation, msgPrompt
                txtName.SetFocus
                GoTo PROC_EXIT
            End If
        End With
    End If
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 职工 where GUID = '" & lvw.SelectedItem.Key & "'", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If .BOF And .EOF Then
            MsgBox "发生意外错误，无法修改记录。", vbExclamation, msgErrRuntime
            GoTo PROC_EXIT
        End If
        
        !工号 = txtID.Text
        !姓名 = txtName.Text
        !性别 = IIf(cboGender.Text = "女", 0, 1)
        !年龄 = Val(txtAge.Text)
        !技能 = txtSkills.Text
        !入厂日期 = dtpJoinDate.Value
        !是否在职 = (chkValid.Value = vbChecked)
        !联系电话 = txtPhoneNumber.Text
        !住址 = txtAddress.Text
        !备注 = txtMemo.Text
        
        Call FillUpdateRecordData(rsADO)
        
        .Update
        
    End With
    
    blnEditing = False
    Call DoLock
    
    With lvw.SelectedItem
        .Text = txtID.Text
        .SubItems(1) = txtName.Text
        .SubItems(2) = cboGender.Text
        .SubItems(3) = Val(txtAge.Text)
        .SubItems(4) = Format(dtpJoinDate.Value, "yyyy-mm-dd")
        .SubItems(5) = IIf(chkValid.Value = vbChecked, "是", "否")
        .SubItems(6) = txtSkills.Text
        .SubItems(7) = txtPhoneNumber.Text
        .SubItems(8) = txtAddress.Text
        .SubItems(9) = txtMemo.Text
    End With
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    If Err.Number = 3022 Then
        rsADO.CancelUpdate
        MsgBox "编号或姓名重复，无法修改。", vbCritical, msgErrRuntime
    Else
        MsgBox "修改记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    End If
    GoTo PROC_EXIT
    
End Sub

Private Sub lvw_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    lvw.SortKey = ColumnHeader.Index - 1
    
End Sub

Private Sub lvw_ItemClick(ByVal Item As MSComctlLib.ListItem)

    If blnAdding Or blnEditing Then
        Call CancelSave
    End If
    
    Call DisplayRecord
    
End Sub

Private Sub txtAddress_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtAge_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtID_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtMemo_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtName_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtPhoneNumber_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtSkills_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub
