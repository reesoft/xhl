VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{19A6DCA1-B1C0-11D5-9712-5254AB22AAF0}#2.0#0"; "lc.ocx"
Begin VB.Form frmSalaries 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "计件工资结算"
   ClientHeight    =   6630
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9600
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6630
   ScaleWidth      =   9600
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cboDate 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "Salary.frx":0000
      Left            =   1200
      List            =   "Salary.frx":0028
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   6202
      Width           =   1575
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "打印(&P)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   8220
      TabIndex        =   13
      Top             =   6180
      Width           =   1155
   End
   Begin TabDlg.SSTab sstab 
      Height          =   6015
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   10610
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   635
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   " 列表 "
      TabPicture(0)   =   "Salary.frx":008E
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lvw"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   " 明细 "
      TabPicture(1)   =   "Salary.frx":00AA
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lbl(0)"
      Tab(1).Control(1)=   "lvwDetail"
      Tab(1).Control(2)=   "lcOperator"
      Tab(1).Control(3)=   "cmdFont"
      Tab(1).Control(4)=   "txtPageFrom"
      Tab(1).Control(5)=   "txtPageTo"
      Tab(1).ControlCount=   6
      Begin VB.TextBox txtPageTo 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -66480
         TabIndex        =   6
         Text            =   "全部"
         Top             =   480
         Width           =   735
      End
      Begin VB.TextBox txtPageFrom 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -67620
         TabIndex        =   5
         Text            =   "1"
         Top             =   480
         Width           =   735
      End
      Begin VB.CommandButton cmdFont 
         Caption         =   "选择字体(&F)"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   -70560
         TabIndex        =   4
         Top             =   443
         Width           =   1755
      End
      Begin MSComctlLib.ListView lvw 
         Height          =   5355
         Left            =   180
         TabIndex        =   1
         Top             =   480
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   9446
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "打印"
            Object.Width           =   1323
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "职工"
            Object.Width           =   3704
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "计件工资"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "扣款"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "实发工资"
            Object.Width           =   3175
         EndProperty
      End
      Begin LabelComboBox.lc lcOperator 
         Height          =   360
         Left            =   -74820
         TabIndex        =   2
         Top             =   480
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   635
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "职工"
         ListIndex       =   -1
      End
      Begin MSComctlLib.ListView lvwDetail 
         Height          =   4995
         Left            =   -74820
         TabIndex        =   3
         Top             =   900
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   8811
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "日期"
            Object.Width           =   2328
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "工序单号"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "产品名称"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "工序名称"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "单价"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "数量"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "工资小计"
            Object.Width           =   2434
         EndProperty
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "页码从        到"
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   -68460
         TabIndex        =   14
         Top             =   540
         Width           =   1920
      End
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   3240
      TabIndex        =   10
      Top             =   6195
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   54067201
      CurrentDate     =   37302
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   5220
      TabIndex        =   12
      Top             =   6195
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   54067201
      CurrentDate     =   37302.9999884259
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "时间区间"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   11
      Left            =   180
      TabIndex        =   7
      Top             =   6255
      Width           =   960
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "到"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   10
      Left            =   4920
      TabIndex        =   11
      Top             =   6255
      Width           =   240
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "从"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   9
      Left            =   2940
      TabIndex        =   9
      Top             =   6255
      Width           =   240
   End
End
Attribute VB_Name = "frmSalaries"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' 显示工资明细
Private Sub DisplayDetail()
    
    Dim strSQL As String
    Dim dateFrom As Date, dateTo As Date
    Dim i As Long
    Dim ccyTotal As Currency
    Dim itemTemp As ListItem
    
    Screen.MousePointer = vbHourglass
    DoEvents
    
    i = 1
    ccyTotal = 0
    dateFrom = dtpFrom.Value
    dateTo = dtpTo.Value
    
    lvwDetail.Visible = False
    DoEvents
    
    lvwDetail.ListItems.Clear
    
    strSQL = "select 工序单主表.*, 工序单从表.*, 产品.* from 工序单从表, 工序单主表" & _
        " inner join 产品 on 产品.编号 = 工序单主表.产品编号" & _
        " where 工序单主表.编号 = 工序单从表.主单编号 and 操作者编号 = '" & lcOperator.Value & "'" & _
        " and 日期 between #" & Format(dateFrom, gc_DateFormat) & "# and #" & Format(dateTo, gc_DateFormat) & " 23:59:59#" & _
        " order by 日期, 单号"
        
    With g_cnADO.Execute(strSQL, , adCmdText)
    
        If .BOF And .EOF Then
            GoTo PROC_EXIT
        End If
        
        Do Until .EOF
            Set itemTemp = lvwDetail.ListItems.Add(, gc_Char & i, Format(!日期, gc_DateFormat))
            itemTemp.SubItems(1) = !单号
            itemTemp.SubItems(2) = !名称
            itemTemp.SubItems(3) = !工序名称
            itemTemp.SubItems(4) = FormatMoney(!单价)
            itemTemp.SubItems(5) = ![工序单从表.数量]
            itemTemp.SubItems(6) = FormatMoney(!单价 * ![工序单从表.数量])
            ccyTotal = ccyTotal + !单价 * ![工序单从表.数量]
            .MoveNext
            i = i + 1
        Loop
        
    End With
    
    Set itemTemp = lvwDetail.ListItems.Add(, gc_Char & "Total", "合计")
    itemTemp.SubItems(6) = FormatMoney(ccyTotal)
    
PROC_EXIT:
    Call GetPages
    Screen.MousePointer = vbNormal
    lvwDetail.Visible = True
    DoEvents
        
End Sub

' 显示所有职工工资列表
Private Sub DisplayList()
    
    Dim strSQL As String
    Dim dateFrom As Date, dateTo As Date
    Dim ccyTotal As Currency
    Dim itemTemp As ListItem
    
    Screen.MousePointer = vbHourglass
    DoEvents
    
    ccyTotal = 0
    dateFrom = dtpFrom.Value
    dateTo = dtpTo.Value
    
    lvw.Visible = False
    DoEvents
    
    lvw.ListItems.Clear
    
    strSQL = "select sum(单价*工序单从表.数量) as 计件工资, 操作者编号, 姓名 from 工序单主表, 工序单从表" & _
        " inner join 职工 on 职工.工号 = 工序单从表.操作者编号" & _
        " where 工序单主表.编号 = 工序单从表.主单编号 and 日期 between #" & Format(dateFrom, gc_DateFormat) & "# and #" & Format(dateTo, gc_DateFormat) & " 23:59:59#" & _
        " group by 操作者编号, 姓名 order by 姓名"
        
    With g_cnADO.Execute(strSQL, , adCmdText)
    
        If .BOF And .EOF Then
            GoTo PROC_EXIT
        End If
        
        Do Until .EOF
            Set itemTemp = lvw.ListItems.Add(, gc_Char & !操作者编号, "")
            itemTemp.SubItems(1) = !姓名 & ""
            itemTemp.SubItems(2) = FormatMoney(!计件工资)
            ccyTotal = ccyTotal + !计件工资
            .MoveNext
        Loop
        
    End With
    
    Set itemTemp = lvw.ListItems.Add(, gc_Char & "Total", "")
    itemTemp.SubItems(1) = "合计"
    itemTemp.SubItems(2) = FormatMoney(ccyTotal)
    
    Call lvw_ColumnClick(lvw.ColumnHeaders(1))
    
PROC_EXIT:
    lvw.Visible = True
    Screen.MousePointer = vbNormal
    DoEvents
        
End Sub

Private Sub cboDate_Click()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        dtpFrom.Enabled = True
        dtpTo.Enabled = True
    Else
        dtpFrom.Enabled = False
        dtpTo.Enabled = False
    End If
    
    SetDateRange cboDate.List(cboDate.ListIndex), dtpFrom, dtpTo
    
    If sstab.Tab = 0 Then
        Call DisplayList
    Else
        Call lcOperator_Click
    End If
    
End Sub

' 取得每页能打印的行数
Private Function GetLinesPerPage()
    
    Dim sngPageHeight As Single
    Dim sngCurY As Single
    Dim lngPrintedLines As Long
    Dim blnEmpty As Boolean
    
    If lvwDetail.ListItems.Count = 0 Then
        blnEmpty = True
        lvwDetail.ListItems.Add , gc_Char & "Dummy"
    End If
    
    ' 可打印高度为 A4 纸高度的 95%
    sngPageHeight = 297 * 56.7 / 100 * 95
    
    lngPrintedLines = 1
    
    ' 表头
    sngCurY = 0
    sngCurY = sngCurY + 60
    sngCurY = sngCurY + 390
    sngCurY = sngCurY + 30
    sngCurY = sngCurY + lvwDetail.ListItems(1).Height
    sngCurY = sngCurY + 15
    
    ' 表中内容
    Do While sngCurY < sngPageHeight - lvwDetail.ListItems(1).Height
        sngCurY = sngCurY + 45
        sngCurY = sngCurY + lvwDetail.ListItems(1).Height + 15
        lngPrintedLines = lngPrintedLines + 1 ' 已打印行数增加 1
    Loop
    
    GetLinesPerPage = lngPrintedLines
    
    If blnEmpty Then
        lvwDetail.ListItems.Remove (gc_Char & "Dummy")
    End If
    
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    GetLinesPerPage = 0
    GoTo PROC_EXIT
    
End Function

' 取得总页数
Private Function GetPages()
    
    Dim lngLines As Long, lngLinesPerPage As Long
    
    lngLines = lvwDetail.ListItems.Count
    lngLinesPerPage = GetLinesPerPage
    
    If lngLinesPerPage > 0 Then
        txtPageTo.Text = (lngLines + lngLinesPerPage - 1) \ lngLinesPerPage
    Else
        txtPageTo.Text = "0"
    End If
    
    txtPageTo.Tag = txtPageTo.Text
    
End Function

' 初始化必要数据
Private Sub InitData()
    
    Set lcOperator.DataSource = g_cnADO.Execute("select 工号, 姓名 from 职工 order by 姓名", , adCmdText)
    
    InitDateRange g_cnADO, "工序单主表", dtpFrom, dtpTo
    
End Sub

Private Sub cmdFont_Click()

    Dim strFontName As String
    Dim sngSize As Single
    
    strFontName = lvwDetail.Font.Name
    sngSize = lvwDetail.Font.Size
    
    If CommonDialogGetFont(Me.hwnd, strFontName, sngSize) <> 0 Then
        lvwDetail.Font.Size = sngSize
        Call GetPages ' 计算总页数
    End If
    
End Sub

Private Sub cmdPrint_Click()

    If sstab.Tab = 0 Then
        Call PrintMe
    Else
        Call PrintMeDetail
    End If
    
End Sub

Private Sub dtpFrom_Change()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        Call DisplayList
    End If
    
End Sub

Private Sub dtpTo_Change()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        Call DisplayList
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyEscape Then
        Unload Me
    End If

End Sub

Private Sub Form_Load()

    Call InitData
    cboDate.ListIndex = 5
    
End Sub

Private Sub lcOperator_Click()

    If lcOperator.Value <> "" Then
        Call DisplayDetail
    End If
    
End Sub

Private Sub lvw_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    Dim i As Long
    
    If ColumnHeader.Index = 1 Then
        For i = 1 To lvw.ListItems.Count
            lvw.ListItems(i).Checked = True
        Next i
    End If
    
End Sub

Private Sub lvw_DblClick()

    If TypeName(lvw.SelectedItem) = "IListItem" Then
        If lvw.SelectedItem.Key <> gc_Char & "Total" Then
            sstab.Tab = 1
        End If
    End If
    
End Sub

Private Sub lvw_ItemClick(ByVal Item As MSComctlLib.ListItem)

    If Item.Key <> gc_Char & "Total" Then
        lcOperator.Value = Mid(Item.Key, Len(gc_Char) + 1)
    End If
    
End Sub

Private Sub PrintMe()

    Dim dateFrom As Date, dateTo As Date
    Dim sngTotalWidth As Single, sngTotalHeight As Single
    Dim sngPageWidth As Single, sngPageHeight As Single
    Dim sngStartX As Single, sngStartY As Single
    Dim sngCurX As Single, sngCurY As Single
    Dim lngCurPage As Long, lngTotalPages As Long
    Dim i As Long, j As Long, lngLines As Long, lngPrintedLines As Long
    
    If lvw.ListItems.Count <= 1 Then Exit Sub
    
    ' 限定用 A4 纸打印
    Printer.Width = 210 * 56.7
    Printer.Height = 297 * 56.7
    
    sngPageWidth = Printer.Width / 100 * 90 ' 可打印范围在纸张范围的 90% 以内
    sngPageHeight = Printer.Height / 100 * 90
    
    sngTotalWidth = 0
    
    For i = 1 To lvw.ColumnHeaders.Count
        sngTotalWidth = sngTotalWidth + lvw.ColumnHeaders(i).Width
    Next i
    
    If sngTotalWidth > sngPageWidth Then
        MsgBox "欲打印列宽之和超出了纸张宽度 " & Format((sngTotalWidth - sngPageWidth) / 56.7, "0.0") & " 毫米，请减少列数或缩小列宽。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    If MsgBox("请准备好打印机和 A4 幅面的纸张，按确定键开始打印工资列表，按取消键放弃打印。", vbInformation + vbOKCancel, msgPrompt) = vbCancel Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    Me.Enabled = False
    lvw.ColumnHeaders(1).Width = 0
    DoEvents
    
    On Error GoTo PROC_ERR
    
'    For i = 1 To lvw.ListItems.Count
'        If lvw.ListItems(i).Checked Then lngLines = lngLines + 1
'    Next i
'    lngLines = lngLines + 1
    lngLines = lvw.ListItems.Count + 1
    dateFrom = dtpFrom.Value
    dateTo = dtpTo.Value
    
    ' 计算打印起始位置
    sngStartX = (Printer.Width - sngPageWidth) / 2
    sngStartY = (Printer.Height - sngPageHeight) / 2
    
    ' 分页打印
    lngPrintedLines = 1
    lngCurPage = 1
    
    Do While lngPrintedLines < lngLines
    
        ' 打印表头
        sngCurY = sngStartY
        Printer.DrawWidth = 2
        Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        sngCurY = sngCurY + 60
        If lngCurPage = 1 Then
            Call PrintText(sngStartX + 100, sngCurY, 15, FormatDateTime(dateFrom, vbLongDate) & "至" & FormatDateTime(dateTo, vbLongDate) & "工资表")
        Else
            Call PrintText(sngStartX + 100, sngCurY, 15, FormatDateTime(dateFrom, vbLongDate) & "至" & FormatDateTime(dateTo, vbLongDate) & "工资表（续）")
        End If
        sngCurY = sngCurY + 390
        Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        Printer.DrawWidth = 1
        
        sngCurY = sngCurY + 30
        For i = 2 To lvw.ColumnHeaders.Count
            If lvw.ColumnHeaders(i).Width > 60 Then Call PrintText(sngStartX + 60 + lvw.ColumnHeaders(i).Left, sngCurY, lvw.Font.Size, lvw.ColumnHeaders(i).Text)
        Next i
        
        sngCurY = sngCurY + lvw.ListItems(1).Height
        sngCurY = sngCurY + 15
        Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        
        ' 打印表中内容
        Do While sngCurY < sngPageHeight - lvw.ListItems(1).Height And lngPrintedLines < lngLines
            If lvw.ListItems(lngPrintedLines).Checked Then
                sngCurY = sngCurY + 45
                'Call PrintText(sngStartX + 120, sngCurY, lvw.Font.Size, lvw.ListItems(lngPrintedLines).Text)
                For j = 1 To lvw.ColumnHeaders.Count - 1
                    If lvw.ColumnHeaders(j + 1).Width > 60 Then Call PrintText(sngStartX + 60 + lvw.ColumnHeaders(j + 1).Left, sngCurY, lvw.Font.Size, lvw.ListItems(lngPrintedLines).SubItems(j))
                Next j
                sngCurY = sngCurY + lvw.ListItems(lngPrintedLines).Height + 15
                Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
            End If
            lngPrintedLines = lngPrintedLines + 1 ' 已打印行数增加 1
        Loop
        
        sngTotalHeight = sngCurY - 15
        
        If lngCurPage = 1 Then
            lngTotalPages = (lngLines - 1) \ lngPrintedLines + 1
        End If
        
        ' 打印页脚
        sngCurY = sngCurY + 30
        Call PrintText(sngStartX, sngCurY, 10.5, "  ※  第 " & lngCurPage & " 页  ※  共 " & lngTotalPages & " 页  ※  ")
        sngCurY = sngCurY + lvw.ListItems(1).Height + 30
        Printer.DrawWidth = 2
        Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        
        ' 打印分隔竖线
        Printer.Line (sngStartX, sngStartY)-(sngStartX, sngTotalHeight + lvw.ListItems(1).Height + 60)
        Printer.DrawWidth = 1
        For i = 2 To lvw.ColumnHeaders.Count
            If lvwDetail.ColumnHeaders(i - 1).Width > 60 Then Printer.Line (sngStartX + lvw.ColumnHeaders(i).Left, sngStartY + 435)-(sngStartX + lvw.ColumnHeaders(i).Left, sngTotalHeight + 15)
        Next i
        Printer.DrawWidth = 2
        Printer.Line (sngStartX + sngTotalWidth, sngStartY)-(sngStartX + sngTotalWidth, sngTotalHeight + lvw.ListItems(1).Height + 60)
        Printer.DrawWidth = 1
        
        ' 开始打印
        Printer.EndDoc
        
        lngCurPage = lngCurPage + 1
        
        If lngPrintedLines < lvw.ListItems.Count Then
'            Call Delay(gc_DelayBetweenMultiPages)
        End If
        
        ' 适当延时，避免因打印机内存不足而导致打印错乱
        Call Delay(gc_DelayBetweenMultiPages)
        
    Loop
    
PROC_EXIT:
    Screen.MousePointer = vbNormal
    Me.Enabled = True
    lvw.ColumnHeaders(1).Width = 600
    Exit Sub
    
PROC_ERR:
    GoTo PROC_EXIT
    
End Sub

Private Sub PrintMeDetail()

    Dim dateFrom As Date, dateTo As Date
    Dim strOperator As String
    Dim sngTotalWidth As Single, sngTotalHeight As Single
    Dim sngPageWidth As Single, sngPageHeight As Single
    Dim sngStartX As Single, sngStartY As Single
    Dim sngCurX As Single, sngCurY As Single
    Dim lngPageFrom As Long, lngPageTo As Long, lngCurPage As Long, lngTotalPages As Long
    Dim i As Long, j As Long, lngLines As Long, lngPrintedLines As Long
    
    If lvwDetail.ListItems.Count <= 1 Then Exit Sub
    
    ' 限定用 A4 纸打印
    Printer.Width = 210 * 56.7
    Printer.Height = 297 * 56.7
    
    sngPageWidth = Printer.Width / 100 * 90 ' 可打印范围在纸张范围的 90% 以内
    sngPageHeight = Printer.Height / 100 * 92
    
    sngTotalWidth = 0
    For i = 1 To lvwDetail.ColumnHeaders.Count
        sngTotalWidth = sngTotalWidth + lvwDetail.ColumnHeaders(i).Width
    Next i
    If sngTotalWidth > sngPageWidth Then
        MsgBox "欲打印列宽之和超出了纸张宽度 " & Format((sngTotalWidth - sngPageWidth) / 56.7, "0.0") & " 毫米，请减少列数或缩小列宽。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    If MsgBox("请准备好打印机和 A4 幅面的纸张，按确定键开始打印工资明细表，按取消键放弃打印。", vbInformation + vbOKCancel, msgPrompt) = vbCancel Then
        Exit Sub
    End If
    
    lngPageFrom = Int(Val(txtPageFrom.Text))
    lngPageTo = Int(Val(txtPageTo.Text))
    If lngPageFrom < 1 Then lngPageFrom = 1
    If lngPageTo < 0 Then lngPageTo = 0
    txtPageFrom.Text = lngPageFrom
    txtPageTo.Text = lngPageTo
    
    lngTotalPages = Val(txtPageTo.Tag)
    
    Screen.MousePointer = vbHourglass
    Me.Enabled = False
    DoEvents
    
    On Error GoTo PROC_ERR
    
    lngLines = lvwDetail.ListItems.Count + 1
    dateFrom = dtpFrom.Value
    dateTo = dtpTo.Value
    strOperator = lcOperator.Text
    
    ' 计算打印起始位置
    sngStartX = (Printer.Width - sngPageWidth) / 2
    sngStartY = (Printer.Height - sngPageHeight) / 4
    
    ' 分页打印
    lngPrintedLines = 1 + (lngPageFrom - 1) * GetLinesPerPage
    lngCurPage = lngPageFrom
    
    Do While lngPrintedLines < lngLines
    
        ' 打印表头
        sngCurY = sngStartY
        Printer.DrawWidth = 2
        Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        sngCurY = sngCurY + 60
        If lngCurPage = 1 Then
            Call PrintText(sngStartX + 100, sngCurY, lvwDetail.Font.Size + 3, strOperator & " " & FormatDateTime(dateFrom, vbLongDate) & "至" & FormatDateTime(dateTo, vbLongDate) & "工资明细表")
        Else
            Call PrintText(sngStartX + 100, sngCurY, lvwDetail.Font.Size + 3, strOperator & " " & FormatDateTime(dateFrom, vbLongDate) & "至" & FormatDateTime(dateTo, vbLongDate) & "工资明细表（续）")
        End If
        sngCurY = sngCurY + 390
        Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        Printer.DrawWidth = 1
        
        sngCurY = sngCurY + 30
        For i = 1 To lvwDetail.ColumnHeaders.Count
            If lvwDetail.ColumnHeaders(i).Width > 60 Then Call PrintText(sngStartX + 60 + lvwDetail.ColumnHeaders(i).Left, sngCurY, lvwDetail.Font.Size, lvwDetail.ColumnHeaders(i).Text)
        Next i
        
        sngCurY = sngCurY + lvwDetail.ListItems(1).Height
        sngCurY = sngCurY + 15
        Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        
        ' 打印表中内容
        Do While sngCurY < sngPageHeight - lvwDetail.ListItems(1).Height And lngPrintedLines < lngLines
            sngCurY = sngCurY + 45
            Call PrintText(sngStartX + 120, sngCurY, lvwDetail.Font.Size, lvwDetail.ListItems(lngPrintedLines).Text)
            For j = 1 To lvwDetail.ColumnHeaders.Count - 1
                If lvwDetail.ColumnHeaders(j + 1).Width > 60 Then Call PrintText(sngStartX + 60 + lvwDetail.ColumnHeaders(j + 1).Left, sngCurY, lvwDetail.Font.Size, lvwDetail.ListItems(lngPrintedLines).SubItems(j))
            Next j
            sngCurY = sngCurY + lvwDetail.ListItems(lngPrintedLines).Height + 15
            Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
            lngPrintedLines = lngPrintedLines + 1 ' 已打印行数增加 1
        Loop
        
        sngTotalHeight = sngCurY - 15
        
        ' 取得总页数
        If lngCurPage = 1 Then
            lngTotalPages = (lngLines - 1) \ lngPrintedLines + 1
        End If
        
        ' 打印页脚
        sngCurY = sngCurY + 30
        Call PrintText(sngStartX, sngCurY, lvwDetail.Font.Size, "  ※  第 " & lngCurPage & " 页  ※  共 " & lngTotalPages & " 页  ※  ")
        sngCurY = sngCurY + lvwDetail.ListItems(1).Height + 30
        Printer.DrawWidth = 2
        Printer.Line (sngStartX, sngCurY)-(sngStartX + sngTotalWidth, sngCurY)
        
        ' 打印分隔竖线
        Printer.Line (sngStartX, sngStartY)-(sngStartX, sngTotalHeight + lvwDetail.ListItems(1).Height + 60)
        Printer.DrawWidth = 1
        For i = 2 To lvwDetail.ColumnHeaders.Count
            If lvwDetail.ColumnHeaders(i - 1).Width > 60 Then Printer.Line (sngStartX + lvwDetail.ColumnHeaders(i).Left, sngStartY + 435)-(sngStartX + lvwDetail.ColumnHeaders(i).Left, sngTotalHeight + 15)
        Next i
        Printer.DrawWidth = 2
        Printer.Line (sngStartX + sngTotalWidth, sngStartY)-(sngStartX + sngTotalWidth, sngTotalHeight + lvwDetail.ListItems(1).Height + 60)
        Printer.DrawWidth = 1
        
        ' 开始打印
        Printer.EndDoc
        
        lngCurPage = lngCurPage + 1
        
        If lngPrintedLines < lvwDetail.ListItems.Count Then
            Call Delay(15)
        End If
        
    Loop
    
PROC_EXIT:
    Screen.MousePointer = vbNormal
    Me.Enabled = True
    Exit Sub
    
PROC_ERR:
    GoTo PROC_EXIT
    
End Sub

