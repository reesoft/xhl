VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form mfrmShortcuts 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "定制快捷按钮"
   ClientHeight    =   3675
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   4995
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3675
   ScaleWidth      =   4995
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Caption         =   "取消(&C)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3660
      TabIndex        =   4
      Top             =   1620
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "确定(&O)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3660
      TabIndex        =   3
      Top             =   1140
      Width           =   1215
   End
   Begin VB.CommandButton cmdMoveDown 
      Caption         =   "下移(&D)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3660
      TabIndex        =   2
      Top             =   660
      Width           =   1215
   End
   Begin VB.CommandButton cmdMoveUp 
      Caption         =   "上移(&U)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3660
      TabIndex        =   1
      Top             =   180
      Width           =   1215
   End
   Begin MSComctlLib.ListView lvw 
      Height          =   3555
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   6271
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "快捷按钮名称"
         Object.Width           =   4233
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "命令"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "启用"
         Object.Width           =   1587
      EndProperty
   End
End
Attribute VB_Name = "mfrmShortcuts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()

    Unload Me
    
End Sub

Private Sub cmdMoveDown_Click()

    If TypeName(lvw.SelectedItem) = "Nothing" Or lvw.ListItems.Count < 2 Then
        Exit Sub
    End If
    
    If lvw.SelectedItem.Index = lvw.ListItems.Count Then
        Exit Sub
    End If
    
    Call ExangeItems(lvw.SelectedItem.Index, lvw.SelectedItem.Index + 1)
    
    Set lvw.SelectedItem = lvw.ListItems.Item(lvw.SelectedItem.Index + 1)
    
End Sub

Private Sub cmdMoveUp_Click()

    If TypeName(lvw.SelectedItem) = "Nothing" Or lvw.ListItems.Count < 2 Then
        Exit Sub
    End If
    
    If lvw.SelectedItem.Index = 1 Then
        Exit Sub
    End If
    
    Call ExangeItems(lvw.SelectedItem.Index - 1, lvw.SelectedItem.Index)
    
    Set lvw.SelectedItem = lvw.ListItems.Item(lvw.SelectedItem.Index - 1)
    
End Sub

Private Sub cmdOK_Click()

    Dim blnChanged As Boolean
    Dim i As Long
    
    For i = 1 To lvw.ListItems.Count
    
        If lvw.ListItems.Item(i).Text <> g_Shortcuts.Item(i).Name _
            Or lvw.ListItems.Item(i).SubItems(1) <> g_Shortcuts.Item(i).Command Then
            
            g_Shortcuts.Item(i).Name = lvw.ListItems.Item(i).Text
            g_Shortcuts.Item(i).Command = lvw.ListItems.Item(i).SubItems(1)
            blnChanged = True
            
        End If
        
    Next i
    
    If blnChanged Then
        Call SaveShortcuts
    End If
    
    Unload Me
    
End Sub

Public Sub DisplayList()

    Dim i As Long
    Dim itemTemp As ListItem
    
    For i = 1 To g_Shortcuts.Count
    
        Set itemTemp = lvw.ListItems.Add(, g_Shortcuts.Item(i).Name, g_Shortcuts.Item(i).Name)
        itemTemp.SubItems(1) = g_Shortcuts.Item(i).Command
        
        If i <= 6 Then
            itemTemp.SubItems(2) = "√"
        End If
        
    Next i
    
End Sub

Private Sub ExangeItems(iItem1 As Long, iItem2 As Long)

    Dim sc As New CShortcut
    
    sc.Name = lvw.ListItems.Item(iItem1).Text
    sc.Command = lvw.ListItems.Item(iItem1).SubItems(1)
    
    lvw.ListItems.Item(iItem1).Text = lvw.ListItems.Item(iItem2).Text
    lvw.ListItems.Item(iItem1).SubItems(1) = lvw.ListItems.Item(iItem2).SubItems(1)
    lvw.ListItems.Item(iItem2).Text = sc.Name
    lvw.ListItems.Item(iItem2).SubItems(1) = sc.Command
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
    Call DisplayList
    
End Sub

Public Sub InitData()

End Sub
