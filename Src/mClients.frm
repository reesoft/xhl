VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{19A6DCA1-B1C0-11D5-9712-5254AB22AAF0}#2.0#0"; "lc.ocx"
Begin VB.Form mfrmClients 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "维护客户"
   ClientHeight    =   7185
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11505
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7185
   ScaleWidth      =   11505
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkInvalid 
      Caption         =   "停用"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   10080
      TabIndex        =   5
      Top             =   660
      Width           =   915
   End
   Begin LabelComboBox.lc lcRegion 
      Height          =   360
      Left            =   6660
      TabIndex        =   2
      Top             =   120
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   635
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "所属地区"
      ListIndex       =   -1
      Spacing         =   90
   End
   Begin VB.TextBox txtAddress 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1200
      TabIndex        =   3
      Top             =   600
      Width           =   5295
   End
   Begin VB.TextBox txtPhone 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   7740
      TabIndex        =   4
      Top             =   600
      Width           =   2175
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "取消(&C)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   9
      Top             =   6660
      Width           =   1335
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "保存(&S)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   8
      Top             =   6180
      Width           =   1335
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "删除(&D)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   10
      Top             =   5700
      Width           =   1335
   End
   Begin VB.CommandButton cmdModify 
      Caption         =   "修改(&M)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   7
      Top             =   5220
      Width           =   1335
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "添加(&A)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   10080
      TabIndex        =   6
      Top             =   4740
      Width           =   1335
   End
   Begin VB.TextBox txtName 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1200
      TabIndex        =   1
      Top             =   120
      Width           =   5295
   End
   Begin MSComctlLib.ListView lvw 
      Height          =   6015
      Left            =   60
      TabIndex        =   0
      Top             =   1080
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   10610
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "客户名称"
         Object.Width           =   2328
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "所属地区"
         Object.Width           =   2328
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "地址"
         Object.Width           =   7408
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "电话"
         Object.Width           =   3175
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "停用"
         Object.Width           =   1323
      EndProperty
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "电话"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   4
      Left            =   7140
      TabIndex        =   13
      Top             =   660
      Width           =   480
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "地址"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   600
      TabIndex        =   12
      Top             =   660
      Width           =   480
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "客户名称"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   120
      TabIndex        =   11
      Top             =   180
      Width           =   960
   End
End
Attribute VB_Name = "mfrmClients"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public blnAutoAdd As Boolean
Public strNewItemID As String

Dim blnAdding As Boolean
Dim blnEditing As Boolean

' 放弃保存
Private Sub CancelSave()
    
    blnAdding = False
    blnEditing = False
    
    Call DoLock
    Call DisplayRecord
    
End Sub

' 清除控件内容以免对后来的显示造成影响
Private Sub ClearDisplay()
    
    On Error Resume Next
    
    txtName.Text = ""
    lcRegion.Value = ""
    txtAddress.Text = ""
    txtPhone.Text = ""
    chkInvalid.Value = vbUnchecked
    
End Sub

Private Sub cmdAdd_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    blnAdding = True
    Call DoLock
    Call ClearDisplay
    
    txtName.SetFocus
    
End Sub

Private Sub cmdCancel_Click()

    Call CancelSave
    
End Sub

Private Sub cmdDelete_Click()

    Call DeleteRecord
    
End Sub

Private Sub cmdModify_Click()

    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    blnEditing = True
    Call DoLock
    txtName.SetFocus
    
End Sub

' 删除记录
Private Sub DeleteRecord()
    
    If blnAdding Or blnEditing Then
        Exit Sub
    End If
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    If MsgBox("确实要删除该记录吗？", vbInformation + vbYesNo, msgPrompt) = vbNo Then
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    g_cnADO.Execute "delete from 客户 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText
    
    If strNewItemID = lvw.SelectedItem.Text Then
        strNewItemID = ""
    End If
    
    Call RemoveSelectedRow(lvw)
    
    Call DisplayRecord
    
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox "删除记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 显示列表
Private Sub DisplayList()
    
    Dim itemTemp As ListItem
    
    lvw.ListItems.Clear
    
    With g_cnADO.Execute("select 客户.*, 地区.名称 from 客户 left join 地区 on 客户.地区 = 地区.编号 order by 客户.名称", , adCmdText)
        Do Until .EOF
            Set itemTemp = lvw.ListItems.Add(, !GUID, .Fields("客户.名称") & "")
            itemTemp.SubItems(1) = .Fields("地区.名称") & ""
            itemTemp.SubItems(2) = !地址 & ""
            itemTemp.SubItems(3) = !电话 & ""
            itemTemp.SubItems(4) = IIf(!停用, "是", "否")
            .MoveNext
        Loop
    End With
    
    If lvw.ListItems.Count > 0 Then
        Set lvw.SelectedItem = lvw.ListItems.Item(1)
        Call DisplayRecord
    End If
    
End Sub

' 显示记录
Private Sub DisplayRecord()
    
    Call ClearDisplay
    
    If TypeName(lvw.SelectedItem) = "Nothing" Then
        Exit Sub
    End If
    
    With g_cnADO.Execute("select * from 客户 where GUID = '" & lvw.SelectedItem.Key & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            txtName.Text = !名称 & ""
            lcRegion.Value = !地区 & ""
            txtAddress.Text = !地址 & ""
            txtPhone.Text = !电话 & ""
            chkInvalid.Value = IIf(!停用, vbChecked, vbUnchecked)
        End If
    End With
    
End Sub

' 锁定/解锁控件
Private Sub DoLock()
    
    If blnAdding Or blnEditing Then
        txtName.Locked = False
        lcRegion.Locked = False
        txtAddress.Locked = False
        txtPhone.Locked = False
        chkInvalid.Enabled = True
        cmdAdd.Enabled = False
        cmdModify.Enabled = False
        cmdDelete.Enabled = False
        cmdSave.Enabled = True
        cmdCancel.Enabled = True
    Else
        txtName.Locked = True
        lcRegion.Locked = True
        txtAddress.Locked = True
        txtPhone.Locked = True
        chkInvalid.Enabled = False
        cmdAdd.Enabled = True
        cmdModify.Enabled = True
        cmdDelete.Enabled = True
        cmdSave.Enabled = False
        cmdCancel.Enabled = False
    End If
    
End Sub

Private Sub cmdSave_Click()

    If blnAdding Then
        Call SaveAdd
    ElseIf blnEditing Then
        Call SaveEdit
    End If
    
End Sub

Private Sub Form_Activate()
    
    If blnAutoAdd Then
        Call cmdAdd_Click
        blnAutoAdd = False
    End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
    
        If TypeName(ActiveControl) = "TextBox" _
            Or TypeName(ActiveControl) = "ComboBox" _
            Or TypeName(ActiveControl) = "CheckBox" _
            Or TypeName(ActiveControl) = "DTPicker" _
            Or TypeName(ActiveControl) = "lc" Then
            
            On Error Resume Next
            
            SendKeys "{TAB}"
            
        End If
        
    ElseIf KeyCode = vbKeyEscape Then
    
        Unload Me
        
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
    Call DisplayList
    Call DoLock
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Call CancelSave
    
End Sub

' 初始化必要数据
Private Sub InitData()
    
    Set lcRegion.DataSource = g_cnADO.Execute("select 编号, 名称 from 地区 order by 名称", , adCmdText)
    Call lcRegion.AddItem("<添加新地区>", "<AddNew>")
    
End Sub

' 保存添加的内容
Private Sub SaveAdd()
    
    txtName.Text = Trim(txtName.Text)
    If txtName.Text = "" Or StrLen(txtName.Text) > 50 Then
        MsgBox "名称应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        txtName.SetFocus
        Exit Sub
    End If
    txtName.Tag = Replace(txtName.Text, "'", "''")
    
    If lcRegion.Value = "<AddNew>" Or lcRegion.Value = "" Then
        MsgBox "请选择地区。", vbInformation, msgPrompt
        lcRegion.SetFocus
        Exit Sub
    End If
    
    txtAddress.Text = Trim(txtAddress.Text)
    If StrLen(txtAddress.Text) > 50 Then
        MsgBox "地址应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        txtAddress.SetFocus
        Exit Sub
    End If
    
    txtPhone.Text = Trim(txtPhone.Text)
    If StrLen(txtPhone.Text) > 50 Then
        MsgBox "电话应该是 1 至 50 个字符，请重新输入。", vbInformation, msgPrompt
        txtPhone.SetFocus
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    With g_cnADO.Execute("select * from 客户 where 名称 = '" & txtName.Tag & "'", , adCmdText)
        If Not (.BOF And .EOF) Then
            MsgBox "该名称已经存在，请输入另一个名称。", vbInformation, msgPrompt
            txtName.SetFocus
            GoTo PROC_EXIT
        End If
    End With
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "客户", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdTable
        
        .AddNew
        
        !编号 = txtName.Text
        !名称 = txtName.Text
        !地区 = lcRegion.Value
        !地址 = txtAddress.Text
        !电话 = txtPhone.Text
        !停用 = (chkInvalid.Value = vbChecked)
        
        Call FillNewRecordData(rsADO)
        
        txtName.Tag = !GUID
        
        .Update
        
    End With
    
    strNewItemID = txtName.Text
    
    blnAdding = False
    Call DoLock
    
    Dim itemTemp As ListItem
    
    Set itemTemp = lvw.ListItems.Add(, txtName.Tag, txtName.Text)
    itemTemp.SubItems(1) = lcRegion.Text
    itemTemp.SubItems(2) = txtAddress.Text
    itemTemp.SubItems(3) = txtPhone.Text
    itemTemp.SubItems(4) = IIf(chkInvalid.Value = vbChecked, "是", "否")
    
    Set lvw.SelectedItem = itemTemp
    Call lvw_ItemClick(itemTemp)
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.CancelUpdate
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    MsgBox "添加记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    GoTo PROC_EXIT
    
End Sub

' 保存修改的内容
Private Sub SaveEdit()
    
    txtName.Text = Trim(txtName.Text)
    If txtName.Text = "" Or StrLen(txtName.Text) > 50 Then
        MsgBox "名称应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        txtName.SetFocus
        Exit Sub
    End If
    txtName.Tag = Replace(txtName.Text, "'", "''")
    
    If lcRegion.Value = "<AddNew>" Or lcRegion.Value = "" Then
        MsgBox "请选择地区。", vbInformation, msgPrompt
        lcRegion.SetFocus
        Exit Sub
    End If
    
    txtAddress.Text = Trim(txtAddress.Text)
    If StrLen(txtAddress.Text) > 50 Then
        MsgBox "地址应该是 1 至 25 个汉字，请重新输入。", vbInformation, msgPrompt
        txtAddress.SetFocus
        Exit Sub
    End If
    
    txtPhone.Text = Trim(txtPhone.Text)
    If StrLen(txtPhone.Text) > 50 Then
        MsgBox "电话应该是 1 至 50 个字符，请重新输入。", vbInformation, msgPrompt
        txtPhone.SetFocus
        Exit Sub
    End If
    
    On Error GoTo PROC_ERR
    
    If txtName.Text <> lvw.SelectedItem.SubItems(1) Then
        With g_cnADO.Execute("select * from 地区 where 名称 = '" & txtName.Tag & "'", , adCmdText)
            If Not (.BOF And .EOF) Then
                MsgBox "该名称已经存在，请输入另一个名称。", vbInformation, msgPrompt
                txtName.SetFocus
                GoTo PROC_EXIT
            End If
        End With
    End If
    
    Dim rsADO As New ADODB.Recordset
    
    With rsADO
    
        .Open "select * from 客户 where GUID = '" & lvw.SelectedItem.Key & "'", g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        If .BOF Or .EOF Then
            GoTo PROC_EXIT
        End If
        
        !编号 = txtName.Text
        !名称 = txtName.Text
        !地区 = lcRegion.Value
        !地址 = txtAddress.Text
        !电话 = txtPhone.Text
        !停用 = (chkInvalid.Value = vbChecked)
        
        Call FillUpdateRecordData(rsADO)
        
        .Update
        
    End With
    
    blnEditing = False
    Call DoLock
    
    With lvw.SelectedItem
        .Text = txtName.Text
        .SubItems(1) = lcRegion.Text
        .SubItems(2) = txtAddress.Text
        .SubItems(3) = txtPhone.Text
        .SubItems(4) = IIf(chkInvalid.Value = vbChecked, "是", "否")
    End With
    
PROC_EXIT:
    If rsADO.State <> adStateClosed Then
        rsADO.Close
    End If
    Exit Sub
    
PROC_ERR:
    If Err.Number = 3022 Then
        rsADO.CancelUpdate
        MsgBox "编号或名称重复，无法修改。", vbCritical, msgErrRuntime
    Else
        MsgBox "修改记录的过程中发生错误：" & Err.Number & vbCrLf & Err.Description, vbCritical, msgErrRuntime
    End If
    GoTo PROC_EXIT
    
End Sub

Private Sub lcRegion_Click()

    If lcRegion.Value = "<AddNew>" Then
    
        mfrmRegions.blnAutoAdd = True
        
        mfrmRegions.Show vbModal, Me
        
        Call InitData
        
        If mfrmRegions.strNewItemID <> "" Then
            lcRegion.Value = mfrmRegions.strNewItemID
            mfrmRegions.strNewItemID = ""
        End If
        
    End If
    
End Sub

Private Sub lvw_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    lvw.SortKey = ColumnHeader.Index - 1
    
End Sub

Private Sub lvw_ItemClick(ByVal Item As MSComctlLib.ListItem)

    If blnAdding Or blnEditing Then
        Call CancelSave
    End If
    
    Call DisplayRecord
    
End Sub

Private Sub txtAddress_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtID_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtName_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub

Private Sub txtPhone_GotFocus()

    Call SelectMe(ActiveControl)
    
End Sub
