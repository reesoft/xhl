VERSION 5.00
Begin VB.Form frmRegister 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "软件注册"
   ClientHeight    =   3675
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5625
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   -1  'True
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Register.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3675
   ScaleWidth      =   5625
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdPrint 
      Caption         =   "打印(&P)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   2910
      TabIndex        =   6
      Top             =   3060
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "确定(&O)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   1260
      TabIndex        =   5
      Top             =   3060
      Width           =   1455
   End
   Begin VB.Frame fra 
      Caption         =   "注册信息"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   10.5
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Index           =   1
      Left            =   180
      TabIndex        =   7
      Top             =   180
      Width           =   5235
      Begin VB.TextBox txtRegisterCode 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1980
         TabIndex        =   4
         Top             =   2055
         Width           =   2835
      End
      Begin VB.TextBox txtProductSN 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1980
         TabIndex        =   2
         Top             =   1208
         Width           =   2835
      End
      Begin VB.TextBox txtSN 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1980
         TabIndex        =   3
         Top             =   1635
         Width           =   2835
      End
      Begin VB.TextBox txtUserName 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1980
         TabIndex        =   1
         Top             =   788
         Width           =   2835
      End
      Begin VB.TextBox txtCompany 
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1980
         TabIndex        =   0
         Top             =   368
         Width           =   2835
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "产品序号："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   930
         TabIndex        =   12
         Top             =   1260
         Width           =   1050
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "注 册 码："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   10
         Left            =   930
         TabIndex        =   11
         Top             =   2107
         Width           =   1050
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "注册序号："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   9
         Left            =   930
         TabIndex        =   10
         Top             =   1687
         Width           =   1050
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "用户名称："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   7
         Left            =   930
         TabIndex        =   9
         Top             =   840
         Width           =   1050
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "公司(单位)名称："
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   10.5
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   6
         Left            =   300
         TabIndex        =   8
         Top             =   420
         Width           =   1680
      End
   End
End
Attribute VB_Name = "frmRegister"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strProgramVersion As String
Dim strCompany As String, strUserName As String, strProductSN As String
Dim dateStartDate As Date
Dim lngTrialDays As Long, lngRunDays As Long
Dim strCodeA As String, strCodeD As String

Private Sub cmdOK_Click()

    ' 检验注册码是否有效
    Dim B64 As New CBase64
    
    If ValidRegisterCode(txtUserName.Text, txtCompany.Text, txtProductSN.Text, g_DiskID, txtRegisterCode.Text) Then
    
        If g_UserName = txtUserName.Text And g_Company = txtCompany.Text And g_ProductSN = txtProductSN.Text And g_RegisterCode = txtRegisterCode.Text Then
            MsgBox "感谢您选择了小花篮管理系统！您已经成功注册。", vbExclamation, "谢谢"
            Exit Sub
        End If
        
        g_UserName = txtUserName.Text
        g_Company = txtCompany.Text
        g_ProductSN = txtProductSN.Text
        g_RegisterCode = txtRegisterCode.Text
        
        If InStr(g_Code, " == " & g_UserName & " == ") = 0 Then
            g_Code = g_Code & g_UserName & " == "
        End If
        
        If InStr(g_Code, " == " & g_Company & " == ") = 0 Then
            g_Code = g_Code & g_Company & " == "
        End If
        
        If InStr(g_Code, " == " & g_ProductSN & " == ") = 0 Then
            g_Code = g_Code & g_ProductSN & " == "
        End If
        
        If InStr(g_Code, " == " & g_DiskID & " == ") = 0 Then
            g_Code = g_Code & g_DiskID & " == "
        End If
        
        g_Code = g_Code & "Register Date == " & Format(Date, "yyyy-mm-dd") & " == "
        
        If Left(g_Code, 4) <> " == " Then
            g_Code = " == " & g_Code
        End If
        
        B64.EncryptKey = gc_B64EncryptKey
        B64.FillCharacter = gc_B64FillCharacter
        
        Call SaveSet("UserName", B64.Encrypt(g_UserName))
        Call SaveSet("Company", B64.Encrypt(g_Company))
        Call SaveSet("ProductSN", g_ProductSN)
        Call SaveSet("RegisterCode", g_RegisterCode)
        Call SaveSet("Code", B64.Encrypt(g_Code))
        
        If MsgBox("感谢您选择了小花篮管理系统！您已经成功注册。" & vbCrLf & vbCrLf & "要打印注册信息以保存备忘吗？", vbExclamation + vbYesNo, "谢谢") = vbNo Then
            Unload Me
        Else
            Call cmdPrint_Click
        End If
        
    Else
    
        MsgBox "您输入的注册码无效，请检查用户名称、公司名称、注册序号、注册码是否与软件开发商提供的内容一致。", vbInformation, msgOperationFail
        
    End If
    
    Me.Caption = "软件注册 （已成功注册）"
    
End Sub

' 打印注册单
Private Sub cmdPrint_Click()

    If MsgBox("请将一张空白 A4 纸放入打印机，按“确认”键开始打印注册单，按“取消”键放弃打印。", vbInformation + vbOKCancel, "提示") = vbCancel Then
        Exit Sub
    End If
    
    Printer.FontSize = 15
    Printer.FontBold = True
    Printer.FontName = "宋体"
    
    Call PrintText(vbCrLf)
    Call PrintText("    ============================================================" & vbCrLf)
    
    If ValidRegisterCode(txtUserName.Text, txtCompany.Text, txtProductSN.Text, g_DiskID, txtRegisterCode.Text) = False Then
        Call PrintText("      请将如下信息发送给软件开发商——" & vbCrLf)
        'Call PrintText("          电话：(020) 87544162" & vbCrLf)
        'Call PrintText("          传真：(020) 87543636" & vbCrLf)
        'Call PrintText("          网址：http://www.ducen.com/" & vbCrLf)
        'Call PrintText("          邮址：ducen@ducen.com" & vbCrLf)
        'Call PrintText("    ============================================================" & vbCrLf)
    End If
    
    Call PrintText("      小花篮管理系统注册信息：" & vbCrLf)
    Call PrintText("          产品序号：" & txtProductSN.Text & vbCrLf)
    Call PrintText("          用户名称：" & txtUserName.Text & vbCrLf)
    Call PrintText("          公司名称：" & txtCompany.Text & vbCrLf)
    Call PrintText("          注册序号：" & txtSN.Text & "(" & EncodeN(txtSN.Text) & ")" & vbCrLf)
    
    If ValidRegisterCode(txtUserName.Text, txtCompany.Text, txtProductSN.Text, g_DiskID, txtRegisterCode.Text) Then
        Call PrintText("          注 册 码：" & txtRegisterCode.Text & "(" & EncodeN(txtRegisterCode.Text) & ")" & vbCrLf)
    End If
    
    Call PrintText("          " & FormatDateTime(Date, vbLongDate) & vbCrLf)
    Call PrintText("    ============================================================" & vbCrLf)
    
    Printer.EndDoc
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
    
        If TypeName(ActiveControl) = "TextBox" Then
        
            KeyCode = 0
            
            On Error Resume Next
            
            SendKeys "{TAB}"
            
        End If
        
    End If
    
End Sub

Private Sub Form_Load()

    txtCompany.Text = g_Company
    txtUserName.Text = g_UserName
    txtProductSN.Text = g_ProductSN
    txtSN.Text = EncodeN(EncryptIt(g_DiskID))
    txtRegisterCode.Text = g_RegisterCode
    
    If ValidRegisterCode(txtUserName.Text, txtCompany.Text, txtProductSN.Text, g_DiskID, txtRegisterCode.Text) Then
        'txtUserName.Enabled = False
        'txtCompany.Enabled = False
        'txtRegisterCode.Enabled = False
        'cmdOK.Enabled = False
        Me.Caption = "软件注册 （已成功注册）"
    Else
        Me.Caption = "软件注册 （未注册）"
    End If
    
End Sub

' 打印文本
Private Sub PrintText(Text As String)

    Printer.Print Text
    
End Sub

