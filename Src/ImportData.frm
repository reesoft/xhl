VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form mfrmImportData 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "导入数据"
   ClientHeight    =   2745
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6975
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2745
   ScaleWidth      =   6975
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkCombineProducts 
      Caption         =   "合并名称相同、编号不同的产品"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   13
      Top             =   1980
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   3675
   End
   Begin VB.TextBox txtDatabase 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1560
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   240
      Width           =   4695
   End
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "..."
      Height          =   375
      Left            =   6360
      TabIndex        =   2
      Top             =   240
      Width           =   375
   End
   Begin VB.CheckBox chkProcedureSheet 
      Caption         =   "生产单记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1800
      TabIndex        =   4
      Top             =   840
      Value           =   1  'Checked
      Width           =   1515
   End
   Begin VB.CheckBox chkIncome 
      Caption         =   "收款记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4920
      TabIndex        =   6
      Top             =   840
      Value           =   1  'Checked
      Width           =   1275
   End
   Begin VB.CheckBox chkSales 
      Caption         =   "发货记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3480
      TabIndex        =   5
      Top             =   840
      Value           =   1  'Checked
      Width           =   1275
   End
   Begin VB.ComboBox cboDate 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "ImportData.frx":0000
      Left            =   1320
      List            =   "ImportData.frx":002B
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   1380
      Width           =   1575
   End
   Begin VB.CommandButton cmdImport 
      Caption         =   "开始导入(&I)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   2520
      TabIndex        =   14
      Top             =   2100
      Width           =   1935
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   3240
      TabIndex        =   10
      Top             =   1380
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   78970881
      CurrentDate     =   37302
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   5220
      TabIndex        =   12
      Top             =   1380
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   78970881
      CurrentDate     =   37302.9999884259
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "导入数据源"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   300
      Width           =   1200
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "要导入的内容"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Width           =   1440
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "从"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   9
      Left            =   2940
      TabIndex        =   9
      Top             =   1440
      Width           =   240
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "到"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   10
      Left            =   4920
      TabIndex        =   11
      Top             =   1440
      Width           =   240
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "时间区间"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   11
      Left            =   240
      TabIndex        =   7
      Top             =   1440
      Width           =   960
   End
End
Attribute VB_Name = "mfrmImportData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_cnADO As New ADODB.Connection

Private Sub cboDate_Click()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        dtpFrom.Enabled = True
        dtpTo.Enabled = True
    Else
        dtpFrom.Enabled = False
        dtpTo.Enabled = False
    End If
    
    SetDateRange cboDate.List(cboDate.ListIndex), dtpFrom, dtpTo
    
End Sub

Private Sub cmdBrowse_Click()

    Dim strDatabase As String
    
    strDatabase = CommonDialogFileOpen(Me.hWnd, txtDatabase.Text, "选择导入数据源", , "数据库文件|*.mdb|所有文件|*.*|")
    
    If strDatabase = "" Then
        Exit Sub
    End If
    
    If strDatabase = g_DBName Then
        MsgBox "不能从这个文件导入数据。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    txtDatabase.Text = strDatabase
    
    If Dir(txtDatabase.Text) <> "" Then
        If Not DatabaseUtilities.CheckDatabase(txtDatabase.Text) Then
            cmdImport.Enabled = False
            MsgBox "文件 " & txtDatabase.Text & " 不是可导入的数据库格式。", vbInformation, msgPrompt
        Else
            cmdImport.Enabled = True
            Call InitData
        End If
    End If
    
End Sub

Private Sub cmdImport_Click()
    
    If chkProcedureSheet.Value = vbUnchecked And chkSales.Value = vbUnchecked And chkIncome.Value = vbUnchecked Then
        MsgBox "请选择需要导入的数据。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    DoEvents
    
    Call ImportData(dtpFrom.Value, dtpTo.Value, m_cnADO, chkProcedureSheet.Value = vbChecked, chkSales.Value = vbChecked, chkIncome.Value = vbChecked)
    
    If chkCombineProducts.Value = vbChecked Then
        Call CombineProductsWithSameName
    End If

    Call ChangeProductsIDToName
    
    Screen.MousePointer = vbNormal
    
    MsgBox "数据已成功导入！", vbInformation, msgOperationSuccess
    
End Sub

' 初始化必要数据
Private Sub InitData()
    
    If m_cnADO.State <> adStateClosed Then
        m_cnADO.Close
    End If
    
    m_cnADO.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source= " & txtDatabase.Text & ";Persist Security Info=False"
    
    InitDateRange m_cnADO, "工序单主表,发货记录,收款记录", dtpFrom, dtpTo
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
    
End Sub

Private Sub Form_Load()

    Call cmdBrowse_Click
    
    cboDate.ListIndex = 11
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    If m_cnADO.State <> adStateClosed Then
        m_cnADO.Close
    End If
    
End Sub
