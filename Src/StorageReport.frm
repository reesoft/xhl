VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmStorageReport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "库存统计"
   ClientHeight    =   5430
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7890
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5430
   ScaleWidth      =   7890
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ListView lvw 
      Height          =   5295
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   7755
      _ExtentX        =   13679
      _ExtentY        =   9340
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "产品名称"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   1
         Text            =   "总产量"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Text            =   "总销量"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Text            =   "当前库存"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "frmStorageReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' 显示所有产品产量、销量、库存列表
Private Sub DisplayList()
    
    Dim strSQL As String
    Dim ccyTotal As Currency
    Dim itemTemp As ListItem
    
    Screen.MousePointer = vbHourglass
    DoEvents
    
    lvw.ListItems.Clear
    
    ccyTotal = 0
    
    strSQL = "SELECT 产品.名称, Sum(工序单主表.数量) AS 生产数量, Sum(发货明细记录.数量) AS 销售数量 " & _
        "FROM (产品 LEFT OUTER JOIN 发货明细记录 ON 产品.编号 = 发货明细记录.产品编号) LEFT OUTER JOIN 工序单主表 ON 产品.编号 = 工序单主表.产品编号 " & _
        "WHERE NOT 停产 " & _
        "GROUP BY 产品.名称 " & _
        "ORDER BY 产品.名称"
    
    With g_cnADO.Execute(strSQL, , adCmdText)
        Do Until .EOF
            Set itemTemp = lvw.ListItems.Add(, , !名称)
            itemTemp.SubItems(1) = FormatNumber(IIf(IsNull(!生产数量), 0, !生产数量), 0)
            itemTemp.SubItems(2) = FormatNumber(IIf(IsNull(!销售数量), 0, !销售数量), 0)
            itemTemp.SubItems(3) = FormatNumber(IIf(IsNull(!生产数量), 0, !生产数量) - IIf(IsNull(!销售数量), 0, !销售数量), 0)
            .MoveNext
        Loop
        .Close
    End With
    
PROC_EXIT:
    Screen.MousePointer = vbNormal
        
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
    Call DisplayList
    
End Sub

' 初始化必要数据
Private Sub InitData()
    
End Sub
