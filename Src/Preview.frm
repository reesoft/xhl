VERSION 5.00
Begin VB.Form frmPreview 
   Caption         =   "打印预览"
   ClientHeight    =   7125
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11910
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   9
      Charset         =   134
      Weight          =   400
      Underline       =   -1  'True
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Preview.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7125
   ScaleWidth      =   11910
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "刷新(&R)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1800
      TabIndex        =   1
      Top             =   0
      Width           =   1800
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "打印(&P)"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   1800
   End
   Begin VB.PictureBox pic 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   6735
      Left            =   0
      ScaleHeight     =   6705
      ScaleWidth      =   11865
      TabIndex        =   2
      Top             =   360
      Width           =   11895
      Begin VB.VScrollBar vscr 
         Height          =   6435
         LargeChange     =   3000
         Left            =   11610
         SmallChange     =   240
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   0
         Width           =   255
      End
      Begin VB.HScrollBar hscr 
         Height          =   255
         LargeChange     =   3000
         Left            =   0
         SmallChange     =   240
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   6450
         Width           =   11595
      End
      Begin VB.PictureBox picPreview 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BeginProperty Font 
            Name            =   "宋体"
            Size            =   9
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   6435
         Left            =   0
         ScaleHeight     =   6405
         ScaleWidth      =   11565
         TabIndex        =   3
         Top             =   0
         Width           =   11595
      End
   End
End
Attribute VB_Name = "frmPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' *************************************************************************
' 打印预览
' *************************************************************************

Option Explicit

Public Sub InitPreview()
    ' *************************************************************************
    ' 初始化预览信息（票据幅面大小等）
    ' *************************************************************************
'    Call LoadPrnSet(ActiveForm.lcScheme.Text)
'    Call SetPrinter
    
    'picPreview.Width = Printer.Width
    'picPreview.Height = Printer.Height
'    picPreview.Width = (g_CurPrnSet.Width - g_PT.ExcursionX) * 56.7
'    picPreview.Height = (g_CurPrnSet.Height - g_PT.ExcursionY) * 56.7
    
    If picPreview.Width > pic.Width - vscr.Width Then
        hscr.Enabled = True
        picPreview.Left = 0
        hscr.Max = picPreview.Width - pic.Width + vscr.Width + 75
        hscr.Min = 0
        hscr.Value = 0
    Else
        hscr.Enabled = False
        picPreview.Left = (pic.Width - vscr.Width - 90 - picPreview.Width) / 2
    End If
    If picPreview.Height > pic.Height - hscr.Height Then
        vscr.Enabled = True
        picPreview.Top = 0
        vscr.Max = picPreview.Height - pic.Height + hscr.Height + 75
        vscr.Min = 0
        vscr.Value = 0
    Else
        vscr.Enabled = False
        picPreview.Top = (pic.Height - hscr.Height - 90 - picPreview.Height) / 2
    End If
End Sub

Public Sub DoPreview()
    ' *************************************************************************
    ' 开始预览，调用票据窗体中定义的 PrintMe 方法将结果输出到 PictureBox 控件中
    ' *************************************************************************
    picPreview.Cls
    g_Preview = True
End Sub

Private Sub cmdRefresh_Click()
    Call DoPreview
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyEscape Then
        Unload Me
    End If

End Sub

Private Sub Form_Load()
    If Screen.Width > 800 Then
        Me.Width = Screen.Width
        Me.Height = Screen.Height - 1020
    End If
End Sub

Private Sub Form_Resize()
    pic.Width = Me.ScaleWidth
    pic.Height = Me.ScaleHeight - pic.Top
    hscr.Width = pic.Width - vscr.Width
    vscr.Height = pic.Height - hscr.Height
    hscr.Top = pic.Height - hscr.Height - 30
    vscr.Left = pic.Width - vscr.Width - 30
End Sub

Private Sub hscr_Change()
    ' *************************************************************************
    ' 水平滚动
    ' *************************************************************************
    picPreview.Left = -hscr.Value
End Sub

Private Sub vscr_Change()
    ' *************************************************************************
    ' 垂直滚动
    ' *************************************************************************
    picPreview.Top = -vscr.Value
End Sub
