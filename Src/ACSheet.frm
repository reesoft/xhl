VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{19A6DCA1-B1C0-11D5-9712-5254AB22AAF0}#2.0#0"; "lc.ocx"
Begin VB.Form frmACSheet 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "输出对帐单"
   ClientHeight    =   6570
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9630
   BeginProperty Font 
      Name            =   "宋体"
      Size            =   10.5
      Charset         =   134
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6570
   ScaleWidth      =   9630
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cboDate 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "ACSheet.frx":0000
      Left            =   1200
      List            =   "ACSheet.frx":0028
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   600
      Width           =   1575
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "打印(&P)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   7500
      TabIndex        =   3
      Top             =   105
      Width           =   1875
   End
   Begin VB.CommandButton cmdList 
      Caption         =   "显示记录(&L)"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5160
      TabIndex        =   2
      Top             =   105
      Width           =   2175
   End
   Begin MSComctlLib.ListView lvw1 
      Height          =   4995
      Left            =   120
      TabIndex        =   10
      Top             =   1440
      Width           =   4635
      _ExtentX        =   8176
      _ExtentY        =   8811
      SortKey         =   1
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "序号"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "日期"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "金额"
         Object.Width           =   3175
      EndProperty
   End
   Begin MSComctlLib.ListView lvw2 
      Height          =   4995
      Left            =   4860
      TabIndex        =   0
      Top             =   1440
      Width           =   4635
      _ExtentX        =   8176
      _ExtentY        =   8811
      SortKey         =   1
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "序号"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "日期"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "金额"
         Object.Width           =   3175
      EndProperty
   End
   Begin LabelComboBox.lc lcClients 
      Height          =   360
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   4875
      _ExtentX        =   8599
      _ExtentY        =   635
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "客户名称"
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   5460
      TabIndex        =   9
      Top             =   615
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   54132737
      CurrentDate     =   37302.9999884259
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   3300
      TabIndex        =   7
      Top             =   615
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   661
      _Version        =   393216
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   54132737
      CurrentDate     =   37302
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "到"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   10
      Left            =   5100
      TabIndex        =   8
      Top             =   660
      Width           =   240
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "从"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   9
      Left            =   2940
      TabIndex        =   6
      Top             =   660
      Width           =   240
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "时间区间"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   11
      Left            =   120
      TabIndex        =   4
      Top             =   660
      Width           =   960
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "付款记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   2
      Left            =   4920
      TabIndex        =   12
      Top             =   1140
      Width           =   960
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "发货记录"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   180
      TabIndex        =   11
      Top             =   1140
      Width           =   960
   End
End
Attribute VB_Name = "frmACSheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboDate_Click()

    If cboDate.List(cboDate.ListIndex) = "任选时间段" Then
        dtpFrom.Enabled = True
        dtpTo.Enabled = True
    Else
        dtpFrom.Enabled = False
        dtpTo.Enabled = False
    End If
    
    SetDateRange cboDate.List(cboDate.ListIndex), dtpFrom, dtpTo
    
    If lcClients.Value <> "" Then
        Call DoQuery
    End If
    
End Sub

Private Sub cmdList_Click()

    Call DoQuery
    
End Sub

Private Sub cmdPrint_Click()

    Call PrintMe
    
End Sub

Public Sub DisplayList()

    Dim strSQL As String
    
    If lcClients.Text = "" Then
        MsgBox "请选择一个客户名称。", vbInformation, msgPrompt
        Exit Sub
    End If
    
    strSQL = " where 名称='" & lcClients.Text & "'"
    
    If Trim(strSQL) <> "" Then
        strSQL = strSQL & " and "
    End If
    
    strSQL = strSQL & "日期 between #" & Format(dtpFrom.Value, gc_DateFormat) & "# and #" & Format(dtpTo.Value, gc_DateFormat) & " 23:59:59#"
    
    Dim rsADO1 As New ADODB.Recordset
    Dim rsADO2 As New ADODB.Recordset
    Dim itemTemp As ListItem
    Dim i As Long
    Dim ccyTotal1 As Currency, ccyTotal2 As Currency
    
    ccyTotal1 = 0
    ccyTotal2 = 0
    
    lvw1.Visible = False
    lvw2.Visible = False
    DoEvents
    
    lvw1.ListItems.Clear
    lvw2.ListItems.Clear
    
    i = 1
    
    With rsADO1
    
        .Open "select * from 发货记录列表" & strSQL, g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        Do Until .EOF
        
            Set itemTemp = lvw1.ListItems.Add(, !GUID, i)
            itemTemp.SubItems(1) = Format(!日期, gc_DateFormat)
            itemTemp.SubItems(2) = FormatMoney(!金额)
            ccyTotal1 = ccyTotal1 + !金额
            
            .MoveNext
            i = i + 1
            
        Loop
        
        Set itemTemp = lvw1.ListItems.Add(, gc_Char, "")
        itemTemp.SubItems(1) = "合计"
        itemTemp.SubItems(2) = FormatMoney(ccyTotal1)
        
    End With
    
    i = 1
    
    With rsADO2
    
        .Open "select * from 收款记录列表" & strSQL, g_cnADO, adOpenDynamic, adLockOptimistic, adCmdText
        
        Do Until .EOF
        
            Set itemTemp = lvw2.ListItems.Add(, !GUID, i)
            itemTemp.SubItems(1) = Format(!日期, gc_DateFormat)
            itemTemp.SubItems(2) = FormatMoney(!金额)
            ccyTotal2 = ccyTotal2 + !金额
            
            .MoveNext
            i = i + 1
            
        Loop
        
        Set itemTemp = lvw2.ListItems.Add(, gc_Char, "")
        itemTemp.SubItems(1) = "合计"
        itemTemp.SubItems(2) = FormatMoney(ccyTotal2)
        
    End With
    
PROC_EXIT:
    lvw1.Visible = True
    lvw2.Visible = True
    DoEvents
    
End Sub

Private Sub DoQuery()

    Call DisplayList
    
End Sub

Private Sub dtpFrom_Change()

    Call DoQuery
    
End Sub

Private Sub dtpTo_Change()

    Call DoQuery
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
    
End Sub

Private Sub Form_Load()

    Call InitData
    cboDate.ListIndex = 9
    
End Sub

Private Sub InitData()

    Set lcClients.DataSource = g_cnADO.Execute("select 编号, 名称 from 客户 order by 地区, 名称", , adCmdText)
    
    InitDateRange g_cnADO, "发货记录,收款记录", dtpFrom, dtpTo
    
End Sub

Private Sub lcClients_Click()

    Call DoQuery
    
End Sub

Private Sub lcClients_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyReturn Then
        Call cmdList_Click
    End If
    
End Sub

Private Sub PrintMe()

    Const TableWidth = 5100
    Const LineHeight = 225
    Dim sngTotalWidth As Single, sngTotalHeight As Single
    Dim sngPageWidth As Single, sngPageHeight As Single
    Dim sngStartX1 As Single, sngStartX2 As Single, sngStartY As Single
    Dim sngCurX As Single, sngCurY As Single, sngCurY2 As Single
    Dim lngCurPage As Long, lngTotalPages1 As Long, lngTotalPages2 As Long, lngLines1 As Long, lngLines2 As Long
    Dim i As Long, j As Long, lngPrintedLines1 As Long, lngPrintedLines2 As Long
    Dim FieldX(3) As Single, FieldW(3) As Single
    
    ' 限定用 A4 纸打印
    Printer.Width = 210 * 56.7
    Printer.Height = 297 * 56.7
    
    sngPageWidth = Printer.Width / 100 * 90 ' 可打印范围在纸张范围的 90% 以内
    sngPageHeight = Printer.Height / 100 * 90
    
    'If lvw1.ListItems.Count <= 1 Then Exit Sub
    
'    sngTotalWidth = 0
'    For i = 1 To 2
'        sngTotalWidth = sngTotalWidth + lvw1.ColumnHeaders(i).Width
'    Next i
'    If sngTotalWidth > sngPageWidth Then
'        MsgBox "欲打印列宽之和超出了纸张宽度 " & Format((sngTotalWidth - sngPageWidth) / 56.7, "0.0") & " 毫米，请减少列数或缩小列宽。", vbInformation, msgPrompt
'        Exit Sub
'    End If
    
    If MsgBox("请准备好打印机和 A4 幅面的纸张，按确定键开始打印发货记录，按取消键放弃打印。", vbInformation + vbOKCancel, msgPrompt) = vbCancel Then
        Exit Sub
    End If
    
    FieldX(1) = 0
    FieldX(2) = 900
    FieldX(3) = 3000
    FieldW(1) = 900
    FieldW(2) = 1500
    FieldW(3) = 2100
    
    lngLines1 = lvw1.ListItems.Count + 1
    lngLines2 = lvw2.ListItems.Count + 1
    
    ' 计算打印起始位置
    sngStartX1 = (Printer.Width - sngPageWidth) / 2
    sngStartX2 = sngStartX1 + sngPageWidth / 2 + 60
    sngStartY = (Printer.Height - sngPageHeight) / 2
    
    ' 分页打印
    lngPrintedLines1 = 1
    lngPrintedLines2 = 1
    lngCurPage = 1
    
    Do While lngPrintedLines1 < lngLines1 Or lngPrintedLines2 < lngLines2
    
        ' 打印表头
        sngCurY = sngStartY
        Printer.DrawWidth = 2
        If lngPrintedLines1 < lngLines1 Then Printer.Line (sngStartX1, sngCurY)-(sngStartX1 + TableWidth, sngCurY)
        If lngPrintedLines2 < lngLines2 Then Printer.Line (sngStartX2, sngCurY)-(sngStartX2 + TableWidth, sngCurY)
        sngCurY = sngCurY + 60
        If lngCurPage = 1 Then
            If lngPrintedLines1 < lngLines1 Then Call PrintText(sngStartX1 + 100, sngCurY, 15, "发货记录表 - 客户：" & lcClients.Text)
            If lngPrintedLines2 < lngLines2 Then Call PrintText(sngStartX2 + 100, sngCurY, 15, "付款记录表 - 客户：" & lcClients.Text)
        Else
            If lngPrintedLines1 < lngLines1 Then Call PrintText(sngStartX1 + 100, sngCurY, 15, "发货记录表（续） - 客户：" & lcClients.Text)
            If lngPrintedLines2 < lngLines2 Then Call PrintText(sngStartX2 + 100, sngCurY, 15, "付款记录表（续） - 客户：" & lcClients.Text)
        End If
        sngCurY = sngCurY + 390
        If lngPrintedLines1 < lngLines1 Then Printer.Line (sngStartX1, sngCurY)-(sngStartX1 + TableWidth, sngCurY)
        If lngPrintedLines2 < lngLines2 Then Printer.Line (sngStartX2, sngCurY)-(sngStartX2 + TableWidth, sngCurY)
        Printer.DrawWidth = 1
        
        sngCurY = sngCurY + 30
        For i = 1 To 3
            If lngPrintedLines1 < lngLines1 Then Call PrintText(sngStartX1 + 60 + FieldX(i), sngCurY, 10.5, lvw1.ColumnHeaders(i))
            If lngPrintedLines2 < lngLines2 Then Call PrintText(sngStartX2 + 60 + FieldX(i), sngCurY, 10.5, lvw2.ColumnHeaders(i))
        Next i
        
        sngCurY = sngCurY + LineHeight
        sngCurY = sngCurY + 15
        If lngPrintedLines1 < lngLines1 Then Printer.Line (sngStartX1, sngCurY)-(sngStartX1 + TableWidth, sngCurY)
        If lngPrintedLines2 < lngLines2 Then Printer.Line (sngStartX2, sngCurY)-(sngStartX2 + TableWidth, sngCurY)
        
        sngCurY2 = sngCurY
        
        ' ************************** 表 1 ******************************
        If lngPrintedLines1 < lngLines1 Then
            ' 打印表中内容
            Do While sngCurY < sngPageHeight - LineHeight And lngPrintedLines1 < lngLines1
                sngCurY = sngCurY + 45
                Call PrintText(sngStartX1 + 120 + FieldX(1), sngCurY, lvw1.Font.Size, lvw1.ListItems(lngPrintedLines1))
                For j = 1 To 2
                    Call PrintText(sngStartX1 + 120 + FieldX(j + 1), sngCurY, lvw1.Font.Size, lvw1.ListItems(lngPrintedLines1).SubItems(j))
                Next j
                sngCurY = sngCurY + lvw1.ListItems(lngPrintedLines1).Height + 15
                Printer.Line (sngStartX1, sngCurY)-(sngStartX1 + TableWidth, sngCurY)
                lngPrintedLines1 = lngPrintedLines1 + 1 ' 已打印行数增加 1
            Loop
            
            sngTotalHeight = sngCurY - 15
            
            If lngCurPage = 1 Then
                lngTotalPages1 = (lngLines1 - 1) \ lngPrintedLines1 + 1
            End If
            
            ' 打印页脚
            sngCurY = sngCurY + 30
            Call PrintText(sngStartX1, sngCurY, 10.5, "  ※  第 " & lngCurPage & " 页  ※  共 " & lngTotalPages1 & " 页  ※  ")
            sngCurY = sngCurY + LineHeight
            sngCurY = sngCurY + 30
            Printer.DrawWidth = 2
            Printer.Line (sngStartX1, sngCurY)-(sngStartX1 + TableWidth, sngCurY)
            
            ' 打印分隔竖线
            Printer.Line (sngStartX1, sngStartY)-(sngStartX1, sngTotalHeight + LineHeight + 60)
            Printer.DrawWidth = 1
            Printer.Line (sngStartX1 + FieldX(2), sngStartY + 435)-(sngStartX1 + FieldX(2), sngTotalHeight + 15)
            Printer.Line (sngStartX1 + FieldX(3), sngStartY + 435)-(sngStartX1 + FieldX(3), sngTotalHeight + 15)
            Printer.DrawWidth = 2
            Printer.Line (sngStartX1 + TableWidth, sngStartY)-(sngStartX1 + TableWidth, sngTotalHeight + LineHeight + 60)
            Printer.DrawWidth = 1
        End If
        
        sngCurY = sngCurY2
        
        ' ************************** 表 2 ******************************
        If lngPrintedLines2 < lngLines2 Then
            ' 打印表中内容
            Do While sngCurY < sngPageHeight - lvw2.ListItems(1).Height And lngPrintedLines2 < lngLines2
                sngCurY = sngCurY + 45
                Call PrintText(sngStartX2 + 120 + FieldX(1), sngCurY, lvw2.Font.Size, lvw2.ListItems(lngPrintedLines2))
                For j = 1 To 2
                    Call PrintText(sngStartX2 + 120 + FieldX(j + 1), sngCurY, lvw2.Font.Size, lvw2.ListItems(lngPrintedLines2).SubItems(j))
                Next j
                sngCurY = sngCurY + lvw2.ListItems(lngPrintedLines2).Height + 15
                Printer.Line (sngStartX2, sngCurY)-(sngStartX2 + TableWidth, sngCurY)
                lngPrintedLines2 = lngPrintedLines2 + 1 ' 已打印行数增加 1
            Loop
            
            sngTotalHeight = sngCurY - 15
            
            If lngCurPage = 1 Then
                lngTotalPages2 = (lngLines2 - 1) \ lngPrintedLines2 + 1
            End If
            
            ' 打印页脚
            sngCurY = sngCurY + 30
            Call PrintText(sngStartX2, sngCurY, 10.5, "  ※  第 " & lngCurPage & " 页  ※  共 " & lngTotalPages2 & " 页  ※  ")
            sngCurY = sngCurY + lvw2.ListItems(1).Height
            sngCurY = sngCurY + 30
            Printer.DrawWidth = 2
            Printer.Line (sngStartX2, sngCurY)-(sngStartX2 + TableWidth, sngCurY)
            
            ' 打印分隔竖线
            Printer.Line (sngStartX2, sngStartY)-(sngStartX2, sngTotalHeight + lvw2.ListItems(1).Height + 60)
            Printer.DrawWidth = 1
            Printer.Line (sngStartX2 + FieldX(2), sngStartY + 435)-(sngStartX2 + FieldX(2), sngTotalHeight + 15)
            Printer.Line (sngStartX2 + FieldX(3), sngStartY + 435)-(sngStartX2 + FieldX(3), sngTotalHeight + 15)
            Printer.DrawWidth = 2
            Printer.Line (sngStartX2 + TableWidth, sngStartY)-(sngStartX2 + TableWidth, sngTotalHeight + lvw2.ListItems(1).Height + 60)
            Printer.DrawWidth = 1
        End If
        
        ' 开始打印
        Printer.EndDoc
        
        lngCurPage = lngCurPage + 1
        
        If lngPrintedLines1 < lvw1.ListItems.Count Then
            'If MsgBox("请稍候。" & vbCrLf & vbCrLf & "待当前页打印完毕后，按确认键继续打印下一页，按取消键放弃打印后面的页。", vbInformation + vbOKCancel, msgprompt) = vbCancel Then Exit Sub
            'Printer.NewPage
        End If
        
        ' 适当延时，避免因打印机内存不足而导致打印错乱
        Call Delay(gc_DelayBetweenMultiPages)
    Loop
    
End Sub

